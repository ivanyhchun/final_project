# Frontend folder:
Perform following actions at ./frontend directory:
* yarn add bootstrap
* yarn add reactstrap @types/reactstrap
* yarn add @testing-library/react @testing-library/jest-dom
* yarn add redux react-redux @types/react-redux
* yarn add redux-logger @types/redux-logger
* yarn add react-router-dom @types/react-router-dom
* yarn add history@^4.10.1 connected-react-router@^6.8.0
* yarn add use-react-router 
* yarn add redux-thunk
* yarn add jwt-decode


# Server folder:
Perform following actions at ./server directory:
* yarn add ts-node @types/node typescript express @types/express cors @types/cors ts-node-dev
* yarn add ts-jest @types/jest
* yarn add dotenv @types/dotenv knex @types/knex pg @types/pg
* yarn add jwt-simple @types/jwt-simple permit @types/permit
* yarn add --dev playwright 
* yarn ts-jest config:init
* yarn knex init -x ts
* yarn add add multer @types/multer multer-s3 @types/multer-s3