import {createStore,combineReducers,compose, applyMiddleware} from 'redux'
import {
    RouterState,
    connectRouter,
    routerMiddleware, 
    CallHistoryMethodAction
} from 'connected-react-router'
import { createBrowserHistory } from 'history'
import { profileReducer, ProfileState } from './redux/profile/reducer'
import {namecardsListReducer, 
        NamecardsListState, 
        oneNamecardReducer, 
        OneNamecardState, 
} from './redux/namecards/reducer'

import { owncardsReducer, OwncardsState } from './redux/owncards/reducer'
import thunk from 'redux-thunk'
import { authReducer, AuthState } from './redux/auth/reducer'
import { searchReducer, SearchState } from './redux/search/reducer'
import { CategoryListState, categoryReducer } from './redux/category/reducer'
import { messageReducer, MessageState } from './redux/message/reducer'
import { createdCardsReducer, CreatedCardsState } from './redux/createdCards.ts/reducer'
import { QRCodeReducer, QRCodeState } from './redux/qrcode/reducer'
import { dataReducer, DataState } from './redux/data/reducer'

export const history = createBrowserHistory()

export interface RootState{
    auth:AuthState,
    owncards: OwncardsState,
    data:DataState,
    namecardsList:NamecardsListState,
    categoryList:CategoryListState,
    search:SearchState,
    oneNamecard:OneNamecardState,
    profile: ProfileState,
    message: MessageState,
    router:RouterState,
    cardObj:CreatedCardsState,
    QRCode:QRCodeState
}

const reducer = combineReducers<RootState>({
    auth:authReducer,
    owncards: owncardsReducer,
    data:dataReducer,
    namecardsList:namecardsListReducer,
    oneNamecard:oneNamecardReducer,
    categoryList:categoryReducer,
    search:searchReducer,
    profile: profileReducer,
    message: messageReducer,
    router:connectRouter(history),
    cardObj:createdCardsReducer,
    QRCode:QRCodeReducer
})

declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(reducer,composeEnhancers(
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history)),

))