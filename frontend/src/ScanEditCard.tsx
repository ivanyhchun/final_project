import React from 'react'; 
import Background from './Background';
import CardForm from './CardForm'; 
import styles from './ScanEditCard.module.scss'; 

function ScanEditCard() {
    return (
        <Background name="bkgd-main">
            <div className={styles["card-container"]}>
                <div className={styles["card-body"]}>
                    CARD IMAGE HERE
                </div>
            </div>
            <div className={styles["form-body"]}>
                <CardForm name="CardForm"/>
            </div>
        </Background>
    )
}

export default ScanEditCard; 

