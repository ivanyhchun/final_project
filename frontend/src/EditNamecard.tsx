import { push } from 'connected-react-router';
import React, { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { AiOutlineMail } from 'react-icons/ai';
import { CgProfile } from 'react-icons/cg';
import { FaCamera } from 'react-icons/fa';
import { FiPhone } from 'react-icons/fi';
import { HiOutlineBriefcase } from 'react-icons/hi';
import { IoIosArrowBack, IoMdDesktop } from 'react-icons/io';
import { IoLocationOutline, IoTrashOutline } from 'react-icons/io5';
import { MdSmartphone } from 'react-icons/md';
import { RiToolsFill } from 'react-icons/ri';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import useRouter from 'use-react-router';
import Background from './Background';
import styles from './EditNamecard.module.scss';
import { deleteNamecard, editNonMemberCard, fetchOneCard } from './redux/namecards/action';
import { RootState } from './store';
import CreatableSelect from 'react-select/creatable';

function EditNamecard() {
    const existingCategoryArray = useSelector((state: RootState) => state.categoryList.categoryList)
    const selectedNamecard = useSelector((state: RootState) => state.oneNamecard.oneNamecard);
    const router = useRouter<{ id?: string }>();
    let selectionArray:  Object[] = [];
    selectionArray = existingCategoryArray.map(data=>(
        {value:data.categoryId,
        label:data.categoryName}
    ))

    const { register, handleSubmit, control } = useForm();
    const onSubmit = handleSubmit(data => {
        dispatch(editNonMemberCard(
            selectedNamecard.externalId,
            data.firstName,
            data.lastName,
            data.position,
            data.companyName,
            data.address,
            data.workContact,
            data.mobile,
            data.email,
            data.namecardImg,
            data.categoryIDorName.value
        )); 
        }
    );

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchOneCard(router.match.params.id + ''))
    }, []);

    const [firstName, setFirstName] = useState(selectedNamecard.firstName);
    const [lastName, setLastName] = useState(selectedNamecard.lastName);
    const [companyName, setCompanyName] = useState(selectedNamecard.companyName);
    const [address, setAddress] = useState(selectedNamecard.address);
    const [position, setPosition] = useState(selectedNamecard.position);
    const [workContact, setWorkContact] = useState(selectedNamecard.workContact);
    const [mobile, setMobile] = useState(selectedNamecard.mobile);
    const [email, setEmail] = useState(selectedNamecard.email);

    return (
        <Background name="bkgd-namecard">
            <div className={styles["page-header"]}>
                <div className={styles["ioiosArrowBack"]} onClick={() => {
                    dispatch(push('/homepage/' + selectedNamecard.externalId));
                }}>
                    <IoIosArrowBack />
                </div>
                <h4>Edit {selectedNamecard.firstName}'s Profile</h4>
                <div className={styles["vscGear"]} onClick={() => {
                    if (!selectedNamecard.memberId) {
                        const confirmBox = window.confirm(`Are you sure you want to delete ${selectedNamecard.firstName}'s card?`); 
                        if(confirmBox) {
                            dispatch(deleteNamecard(null, selectedNamecard.id)); 
                        }
                    }
                }}>
                    <IoTrashOutline />
                </div>
            </div>
            <div className={styles["card-main-container"]}>
                <form onSubmit={onSubmit} className={styles["create-form"]} autoComplete="off">
                <div className={styles["form-container"]}>
                        <RiToolsFill className={styles["form-icon"]} />
                        <Controller
                            name="categoryIDorName"
                            render={({ field }) => (
                                <CreatableSelect
                                    {...field}
                                    className={styles["selector-bar"]}
                                    options={selectionArray}
                                    isClearable={false}
                                    placeholder={""}
                                    defaultValue={{key:selectedNamecard.categoryId, value:selectedNamecard.categoryName}}
                                />
                            )}
                            control={control}
                            defaultValue={{key:selectedNamecard.categoryId, value:selectedNamecard.categoryName}}
                        />
                    </div>
                    <div className={styles["form-container"]}>
                        <CgProfile className={styles["form-icon"]} />
                        <input {...register("firstName", { required: true, maxLength: 20 })} placeholder="First Name" value={firstName} className={styles["form-group"]} onChange={(event) => {
                            setFirstName(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <CgProfile className={styles["form-icon"]} />
                        <input {...register("lastName", { required: true, maxLength: 20 })} placeholder="Last Name" value={lastName} className={styles["form-group"]} onChange={(event) => {
                            setLastName(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <HiOutlineBriefcase className={styles["form-icon"]} />
                        <input {...register("companyName", { required: true })} placeholder="Company Name" value={companyName} className={styles["form-group"]} onChange={(event) => {
                            setCompanyName(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <IoLocationOutline className={styles["form-icon"]} />
                        <input {...register("address", { required: true })} placeholder="Company Address" value={address} className={styles["form-group"]} onChange={(event) => {
                            setAddress(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <IoMdDesktop className={styles["form-icon"]} />
                        <input {...register("position", { required: true })} placeholder="Job Position" value={position} className={styles["form-group"]} onChange={(event) => {
                            setPosition(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <FiPhone className={styles["form-icon"]} />
                        <input {...register("workContact")} placeholder="Work Phone" value={workContact} className={styles["form-group"]} onChange={(event) => {
                            setWorkContact(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <MdSmartphone className={styles["form-icon"]} />
                        <input {...register("mobile")} placeholder="Mobile Phone" value={mobile} className={styles["form-group"]} onChange={(event) => {
                            setMobile(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <AiOutlineMail className={styles["form-icon"]} />
                        <input {...register("email")} placeholder="Work Email" value={email} className={styles["form-group"]} onChange={(event) => {
                            setEmail(event.currentTarget.value);
                        }} />
                    </div>
                    <input type="submit" className={styles["submit-btn"]} value="SAVE CHANGES" />

                </form>
            </div>
        </Background>
    )
}

export default EditNamecard;