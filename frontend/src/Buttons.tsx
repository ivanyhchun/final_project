import React from 'react';
import { FaCamera, FaQrcode } from 'react-icons/fa';
import styles from './Buttons.module.scss';
import { VscChromeClose } from 'react-icons/vsc';
import { GoPlus } from 'react-icons/go';
import { NavLink } from 'react-router-dom';

function Buttons() {
    return (
        <div className={styles["add"]}>
            <NavLink to="/CameraScan" className={styles["a-link"]}>
                <FaCamera className={styles["icons"]} />
            </NavLink>
            <NavLink to="/QRScan" className={styles["a-link"]}>
                <FaQrcode className={styles["icons"]} />
            </NavLink>
            <NavLink to="/CreateManualNamecard" className={styles["a-link"]}>
                <GoPlus className={styles["icons"]} />
            </NavLink>
            <a href="" className={styles["a-link"]}>
                <VscChromeClose className={styles["icons"]} />
            </a>
        </div>
    )
}

export default Buttons;