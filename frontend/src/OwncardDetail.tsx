import { push } from 'connected-react-router';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { AiOutlineMail } from 'react-icons/ai';
import { CgProfile } from 'react-icons/cg';
import { FiPhone } from 'react-icons/fi';
import { HiOutlineBriefcase } from 'react-icons/hi';
import { IoIosArrowBack, IoMdDesktop } from 'react-icons/io';
import { IoLocationOutline, IoTrashOutline } from 'react-icons/io5';
import { MdSmartphone } from 'react-icons/md';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import useRouter from 'use-react-router';
import Background from './Background';
import styles from './OwncardDetail.module.scss';
import { deleteOwncard, editOwncard, fetchOneOwncard, getNameCardImg } from './redux/owncards/action';
import { RootState } from './store';

interface IOwncardDetail {
    setMenuState: () => void
}

function OwncardDetail(props: IOwncardDetail) {
    const owncards = useSelector((state: RootState) => state.owncards.owncards);
    const router = useRouter<{ id?: string }>();
    const selectedOwncard = owncards.find(owncard => owncard.id == (router.match.params.id + ''));
    const data = useSelector((state: RootState) => state.data.data.namecardImg);
    const dispatch = useDispatch();
    const { register, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = handleSubmit(data => {

        if (selectedOwncard) {
            dispatch(editOwncard(
                parseInt(String(selectedOwncard.id)),
                data.firstName,
                data.lastName,
                data.position,
                data.companyName,
                data.address,
                data.workContact,
                data.mobile,
                data.email,
                data.namecardImg
            ));
            dispatch(push('/DND/' + selectedOwncard?.id))
        }
    });

    useEffect(() => {
        dispatch(fetchOneOwncard(router.match.params.id + ''));
        dispatch(getNameCardImg(selectedOwncard?.namecardImg + ""))
    }, []);

    const [firstName, setFirstName] = useState(selectedOwncard?.firstName);
    const [lastName, setLastName] = useState(selectedOwncard?.lastName);
    const [companyName, setCompanyName] = useState(selectedOwncard?.companyName);
    const [address, setAddress] = useState(selectedOwncard?.address);
    const [position, setPosition] = useState(selectedOwncard?.position);
    const [workContact, setWorkContact] = useState(selectedOwncard?.workContact);
    const [mobile, setMobile] = useState(selectedOwncard?.mobile);
    const [email, setEmail] = useState(selectedOwncard?.email);

    return (
        <Background name="bkgd-profile">
            <div className={styles["page-header"]}>
                <NavLink to="/profile" className={styles["ioiosArrowBack"]}>
                    <IoIosArrowBack />
                </NavLink>
                <h4>Edit Card</h4>
                <div className={styles["vscGear"]} onClick={() => {
                    if (selectedOwncard) {
                        const confirmBox = window.confirm("Are you sure you want to delete this card?");
                        if (confirmBox) {
                            dispatch(deleteOwncard(selectedOwncard.id + ''));
                        }
                    }
                }}>
                    <IoTrashOutline />
                </div>
            </div>
            <div className={styles["card-img-container"]}>
                <div className={styles["card-img-body"]} onClick={() => {
                    dispatch(push('/DND/' + selectedOwncard?.id))
                }}>
                    <img className={styles["img-style"]} src={selectedOwncard?.namecardImg ? selectedOwncard.imageObjectUrl : ""}
                        alt="profile image" />
                </div>
            </div>
            {selectedOwncard &&
                <div className={styles["card-main-container"]}>
                    <form onSubmit={onSubmit} className={styles["create-form"]} autoComplete="off">
                        <div className={styles["form-container"]}>
                            <CgProfile className={styles["form-icon"]} />
                            <input {...register("firstName", { required: true, maxLength: 20 })} placeholder="First Name" value={firstName} className={styles["form-group"]} onChange={(event) => {
                                setFirstName(event.currentTarget.value);
                            }} />
                            {errors.firstName && <span className={styles["error-text"]}>*This field is required.</span>}
                        </div>
                        <div className={styles["form-container"]}>
                            <CgProfile className={styles["form-icon"]} />
                            <input {...register("lastName", { required: true, maxLength: 20 })} placeholder="Last Name" value={lastName} className={styles["form-group"]} onChange={(event) => {
                                setLastName(event.currentTarget.value);
                            }} />
                            {errors.lastName && <span className={styles["error-text"]}>*This field is required.</span>}
                        </div>
                        <div className={styles["form-container"]}>
                            <HiOutlineBriefcase className={styles["form-icon"]} />
                            <input {...register("companyName", { required: true })} placeholder="Company Name" value={companyName} className={styles["form-group"]} onChange={(event) => {
                                setCompanyName(event.currentTarget.value);
                            }} />
                            {errors.companyName && <span className={styles["error-text"]}>*This field is required.</span>}
                        </div>
                        <div className={styles["form-container"]}>
                            <IoLocationOutline className={styles["form-icon"]} />
                            <input {...register("address", { required: true })} placeholder="Company Address" value={address} className={styles["form-group"]} onChange={(event) => {
                                setAddress(event.currentTarget.value);
                            }} />
                            {errors.address && <span className={styles["error-text"]}>*This field is required.</span>}
                        </div>
                        <div className={styles["form-container"]}>
                            <IoMdDesktop className={styles["form-icon"]} />
                            <input {...register("position", { required: true })} placeholder="Job Position" value={position} className={styles["form-group"]} onChange={(event) => {
                                setPosition(event.currentTarget.value);
                            }} /> {errors.position && <span className={styles["error-text"]}>*This field is required.</span>}
                        </div>
                        <div className={styles["form-container"]}>
                            <FiPhone className={styles["form-icon"]} />
                            <input {...register("workContact")} placeholder="Work Phone" value={workContact} className={styles["form-group"]} onChange={(event) => {
                                setWorkContact(event.currentTarget.value);
                            }} />
                        </div>
                        <div className={styles["form-container"]}>
                            <MdSmartphone className={styles["form-icon"]} />
                            <input {...register("mobile")} placeholder="Mobile Phone" value={mobile} className={styles["form-group"]} onChange={(event) => {
                                setMobile(event.currentTarget.value);
                            }} />
                        </div>
                        <div className={styles["form-container"]}>
                            <AiOutlineMail className={styles["form-icon"]} />
                            <input {...register("email")} placeholder="Work Email" value={email} className={styles["form-group"]} onChange={(event) => {
                                setEmail(event.currentTarget.value);
                            }} />
                        </div>
                        <input type="submit" className={styles["submit-btn"]} value="SAVE CHANGES" />
                    </form>
                </div>
            }
        </Background>
    )
}
export default OwncardDetail;

