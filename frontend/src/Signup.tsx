import React from 'react';
import { useForm } from 'react-hook-form';
import styles from './Signup.module.scss';
import Background from './Background';
import LoginHeader from './LoginHeader';
import LoginFooter from './LoginFooter'; 
import { NavLink } from 'react-router-dom';
import { signup } from './redux/auth/action';
import { useDispatch } from 'react-redux';

interface ISignupForm{
    firstName:string,
    lastName:string,
    email:string,
    password:string,
    confirmPassword:string
}


function Signup() {
    const dispatch = useDispatch();
    const { register, handleSubmit, formState: { errors },reset} = useForm<ISignupForm>();
    const onSubmit = handleSubmit(data => {
        if (data.password !== data.confirmPassword){
            alert("Password does not match, please type again.")
            reset({
                firstName:data.firstName,
                lastName:data.lastName,
                email:data.email
            })
        } else {
            dispatch(signup(data.firstName,data.lastName,data.email,data.password));
            reset();
        }

    });

    return (
        <Background name="bkgd-main">
            <LoginHeader>
                Create Account
            </LoginHeader>
            <div className={styles["login-body"]}>
                <form onSubmit={onSubmit} className={styles["login-form"]} autoComplete="off">
                    <input {...register("firstName", { required: true, maxLength: 20 })} placeholder=" First Name" className={errors.firstName ? styles["form-group-error"] : styles["form-group"]} />
                    {errors.firstName && <span className={styles["error-text"]}>*This field is required.</span>}
                    <input {...register("lastName", { required: true, maxLength: 20 })} placeholder=" Last Name" className={errors.lastName ? styles["form-group-error"] : styles["form-group"]} />
                    {errors.lastName && <span className={styles["error-text"]}>*This field is required.</span>}
                    <input {...register("email", { required: true, maxLength: 30 })} placeholder=" Email" className={errors.email ? styles["form-group-error"] : styles["form-group"]} />
                    {errors.email && <span className={styles["error-text"]}>*This field is required.</span>}
                    <input type="password" {...register("password", { required: true, maxLength: 20 })} placeholder=" Password" className={errors.password ? styles["form-group-error"] : styles["form-group"]} />
                    {errors.password && <span className={styles["error-text"]}>*This field is required.</span>}
                    <input type="password" {...register("confirmPassword", { required: true, maxLength: 20 })} placeholder=" Confirm Password" className={errors.confirmPassword ? styles["form-group-error"] : styles["form-group"]} />
                    {errors.confirmPassword && <span className={styles["error-text"]}>*This field is required.</span>}

                    <input type="submit" className={styles["submit-btn"]} value="SIGN UP" />
                </form>
                <LoginFooter>
                    Already have an account? <NavLink to="/login">Sign In</NavLink>
                </LoginFooter>
            </div>
        </Background>
    )

}

export default Signup;
