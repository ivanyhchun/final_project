import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import styles from './EditProfileForm.module.scss';
import { ICardForm } from './CardForm';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from './store';
import { editProfileName, fetchProfile } from './redux/profile/action';

interface IEditProfileForm {
    name: string
    setMenuState: () => void
}

function EditProfileForm(props: IEditProfileForm) {

    const dispatch = useDispatch(); 
    const data = useSelector((state: RootState) => state.profile.profile);
    const { register, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = handleSubmit(data => {
          dispatch(editProfileName(data.firstName, data.lastName)); 
    });
    const [firstName, setFirstName] = useState(data.firstName); 
    const [lastName, setLastName] = useState(data.lastName); 

    return (
        <>
            <form onSubmit={onSubmit} className={styles["login-form"]} autoComplete="off">
                <div className={styles["form-title"]}>First Name</div>
                <input {...register("firstName", { required: true, maxLength: 20 })} value={firstName} onChange={(event) => {
                    setFirstName(event.currentTarget.value); 

                }} className={styles["form-group"]} />

                <div className={styles["form-title"]}>Last Name</div>
                <input {...register("lastName", { required: true, maxLength: 20 })} value={lastName} onChange={(event) => {
                    setLastName(event.currentTarget.value); 

                }} className={styles["form-group"]} />

                <input type="submit" className={styles["submit-btn"]} value="SAVE CHANGES" onClick={props.setMenuState} />
            </form>
        </>
    )
}

export default EditProfileForm;