import React from 'react';
import { useDispatch } from 'react-redux';
import { searchByTextImplement } from './redux/search/action';
import styles from './SearchBar.module.scss';

function SearchBar() {
    const dispatch = useDispatch();
    return (
        <>
            <div className={styles["search-bar-icon"]}>
                <input
                    onInput={e => {
                        e.preventDefault();
                        dispatch(searchByTextImplement(e.currentTarget.value));
                    }}
                    type="text"
                    placeholder="Search..."
                    className={styles["search-bar"]}
                    name='search' />
            </div>
            <div className={styles["search-bar-block"]}></div>
        </>
    )
}

export default SearchBar;