import React from 'react'
import Background from './Background'
import CardForm from './CardForm'
import styles from './InputForm.module.scss'


export default function InputForm() {
    return (
        <Background name="bkgd-main">
            <div className={styles["form-page"]}>
            <CardForm name="" />
            </div>
        </Background>
    )
}