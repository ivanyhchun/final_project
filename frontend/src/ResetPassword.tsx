import React from 'react';
import { useForm } from 'react-hook-form';
import { IoIosArrowBack } from 'react-icons/io';
import { IoClose } from 'react-icons/io5';
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import Background from './Background';
import { chgPassword } from './redux/profile/action';
import styles from './ResetPassword.module.scss';

interface IResetPassword {
    setMenuState: () => void
}

function ResetPassword(props: IResetPassword) {
    const dispatch = useDispatch(); 
    const { register, handleSubmit, formState: { errors },reset } = useForm();
    const onSubmit = handleSubmit(data => {
        if (data.newPassword !== data.confirmPassword){
            alert("Confirm password invalid, please type again.");
            reset({
                oldPassword: data.oldPassword
            });
            return
        }
        dispatch(chgPassword(data.oldPassword, data.newPassword)); 
        reset({})
    });

    return (
        <Background name="bkgd-profile">
            <div className={styles["page-header"]}>
                <NavLink to="/profile" className={styles["ioiosArrowBack"]}>
                    <IoIosArrowBack />
                </NavLink>
                <h4>Account Setting</h4>
                <NavLink onClick={props.setMenuState} to="/profile" className={styles["ioClose"]}>
                    <IoClose />
                </NavLink>
            </div>
            <div className={styles["profile-container"]}>
                <h3>Change Password</h3>   
            </div>
            <div className={styles["reset-password-body"]}>
                <form onSubmit={onSubmit} className={styles["login-form"]} autoComplete="off">
                    <input type="password" {...register("oldPassword", { required: true })} placeholder=" Old Password" className={errors.oldPassword ? styles["form-group-error"] : styles["form-group"]} />
                    {errors.oldPassword && <span className={styles["error-text"]}>*This field is required.</span>}

                    <input type="password" {...register("newPassword", { required: true })} placeholder=" New Password" className={errors.newPassword ? styles["form-group-error"] : styles["form-group"]} />
                    {errors.newPassword && <span className={styles["error-text"]}>*This field is required.</span>}

                    <input type="password" {...register("confirmPassword", { required: true })} placeholder=" Confirm Password" className={errors.confirmPassword ? styles["form-group-error"] : styles["form-group"]} />
                    {errors.confirmPassword && <span className={styles["error-text"]}>*This field is required.</span>}

                    <input type="submit" className={styles["submit-btn"]} value="SUBMIT" onClick={props.setMenuState} />
                </form>
            </div>
        </Background>
    )
}

export default ResetPassword;
