
import React from 'react';
import { GoThreeBars } from 'react-icons/go';
import {  IoMdArrowDropright } from 'react-icons/io';
import { IoClose } from 'react-icons/io5';
import {  useDispatch, useSelector } from 'react-redux';
import styles from './CategoryMenu.module.scss';
import { searchByCategory } from './redux/search/action';
import { RootState } from './store';

interface ICategoryMenu {
    menuState: boolean
    setMenuState: () => void
}

function CategoryMenu(props:ICategoryMenu){
    const dispatch = useDispatch()
    const categoryList = useSelector((state: RootState) => state.categoryList.categoryList);

    return (
        <>
            <div className={props.menuState ? styles["menuClose"] : styles["menuOpen"]}>
                <div className={styles["page-header"]}>
                    <div className={styles["menu-icon"]}>
                        <IoClose className={styles["faBars"]} onClick={props.setMenuState}/>
                    </div>
                    <h4>Select Category</h4>
                    <div className={styles["placeholder"]}>
                        <GoThreeBars/>
                    </div>        
                </div>
                <div className={styles["catList"]}>
                    {categoryList.map(((item)=>(
                        <div className={styles["catButton"]} id={item.categoryId} onClick={()=>{
                            dispatch(searchByCategory(item.categoryId));
                            props.setMenuState()
                        }}>
                            <span><IoMdArrowDropright className={styles["catButton-icon"]} /></span>
                            <span>{item.categoryName}</span>
                        </div>
                    )))}                   
                </div>
                <div></div>
                <div className={styles["catList"]}>
                    <div className={styles["catButton"]} onClick={ ()=>{
                        dispatch(searchByCategory(""))
                        props.setMenuState()
                    }}>
                        Show all Category
                    </div>
                </div>
            </div>
        </>
    )
}

export default CategoryMenu


