import React from 'react';
import { useForm } from 'react-hook-form';
import styles from './Login.module.scss';
import Background from './Background';
import LoginHeader from './LoginHeader';
import LoginFooter from './LoginFooter';
import { NavLink } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { login } from './redux/auth/action';

function Login() {
    const dispatch = useDispatch();
    const { register, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = handleSubmit(data => {
        dispatch(login(data.email, data.password))
    }) ;

    return (
        <Background name="bkgd-main">
            <div>
                
                <LoginHeader>
                    Welcome Back
                </LoginHeader>
                <div className={styles["login-body"]}>
                    <form onSubmit={onSubmit} className={styles["login-form"]} autoComplete="off">
                        <input type="email" {...register("email", { required: true, maxLength: 30 })} placeholder=" Email" className={errors.email ? styles["form-group-error"] : styles["form-group"]} />
                        {errors.email && <span className={styles["error-text"]}>*This field is required.</span>}
                        <input type="password" {...register("password", { required: true, maxLength: 20 })} placeholder=" Password" className={errors.password ? styles["form-group-error"] : styles["form-group"]} />
                        {errors.password && <span className={styles["error-text"]}>*This field is required.</span>}
                        <input type="submit" className={styles["submit-btn"]} value="SIGN IN" />
                    </form>
                    <LoginFooter>
                        Don't have an account? <NavLink to="/signup">Sign Up</NavLink>
                    </LoginFooter>
                </div>
            </div>
        </Background>
    )

}

export default Login;