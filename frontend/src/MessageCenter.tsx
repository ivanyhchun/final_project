import { push } from 'connected-react-router';
import React, { useEffect, useState } from 'react';
import { FiEdit } from 'react-icons/fi';
import { IoIosArrowBack } from 'react-icons/io';
import { IoTrashOutline } from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import Background from './Background';
import styles from './MessageCenter.module.scss';
import { deleteMessage, fetchMessage, messageIsRead } from './redux/message/action';
import { RootState } from './store';

function MessageCenter() {

    const dispatch = useDispatch();
    const [checkbox, setCheckbox] = useState(false);
    const [checkItem, setCheckItem] = useState<number[]>([]);
    const messages = useSelector((state: RootState) => state.message.messages);
    messages.sort((a, b) => a.updatedAt > b.updatedAt ? -1 : 1);

    useEffect(() => {
        dispatch(fetchMessage());
    }, [])

    return (
        <Background name="bkgd-homepage">
            <div className={styles["page-header"]}>
                <div className={styles["ioiosArrowBack"]} onClick={() => {
                    dispatch(push('/homepage'));
                }}>
                    <IoIosArrowBack />
                </div>
                <h4>Message Center</h4>
                <div className={styles["vscGear"]} onClick={() => {
                    setCheckbox(!checkbox);
                    dispatch(deleteMessage(checkItem));
                    setCheckItem([]);
                }}>
                    {checkbox ? <IoTrashOutline /> : <FiEdit />}
                </div>
            </div>
            <div className={styles["page-header-block"]}></div>
            <div className={styles["message-container"]}>
                {messages.length > 0 ?
                    messages.map((message) => {
                        return (
                            <div className={styles["message-card"]}>
                                <div className={styles["alert-box"]}>
                                    <div className={styles["alert-icon-container"]}>
                                        <div className={message.read ? styles["alert-icon-hidden"] : styles["alert-icon"]}></div>
                                    </div>
                                    <div className={styles["checkbox-container"]}>
                                        <input type="checkbox" className={checkbox ? styles["checkbox"] : styles["checkbox-hidden"]} onChange={(event) => {
                                            if (event.currentTarget.checked === true) {
                                                setCheckItem(checkItem.concat(parseInt(String(message.id))));
                                            } else {
                                                setCheckItem(checkItem.filter((item) => {
                                                    return item !== parseInt(String(message.id));
                                                }))
                                            };
                                        }}
                                            checked={checkItem.includes(parseInt(String(message.id)))}
                                        />
                                    </div>
                                </div>
                                <div className={styles["message-body"]} onClick={() => {
                                    dispatch(push('/homepage/' + message.externalId));
                                    dispatch(messageIsRead(parseInt(String(message.id))));
                                }}>
                                    <div className={styles["message-title"]}>
                                        New Message from {message.firstName}
                                    </div>
                                    <div className={styles["content"]}>
                                        {message.content}
                                    </div>
                                    <div className={styles["message-time"]}>
                                        Received at: {(message.updatedAt).split('T')[0]}
                                    </div>
                                </div>
                            </div>
                        )
                    }) : <div className={styles["nomsg-text"]}>You have no new message at the moment.</div>
                }
            </div>
        </Background>
    )
}

export default MessageCenter;

