import React, { useEffect } from 'react';
import styles from './NavBar.module.scss';
import { GoThreeBars } from 'react-icons/go';
import { IoIosMail } from 'react-icons/io';
import { CgProfile } from 'react-icons/cg';
import { NavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { searchByTextImplement } from './redux/search/action';
import CategoryMenu from './CategoryMenu';
import { RootState } from './store';
import { fetchMessage, loadMessage } from './redux/message/action';

interface INavBar {
    menuState: boolean
    setMenuState: () => void
}

function NavBar(props: INavBar) {
    const dispatch = useDispatch();
    const messages = useSelector((state: RootState) => state.message.messages);
    let unreadMsg = 0;
    messages.map((message) => {
        if (message.read === false) {
            unreadMsg += 1;
        }
        return unreadMsg;
    })

    useEffect(() => {
        dispatch(fetchMessage());
    }, [])

    return (
        <div>
            <nav className={styles["navbar-container"]}>
                <CategoryMenu menuState={props.menuState} setMenuState={props.setMenuState} />
                <div className={styles["menu-icon"]}>
                    <GoThreeBars className={styles["faBars"]} onClick={props.setMenuState} />
                </div>
                <div className={styles["search-bar-icon"]}>
                    <input
                        onInput={e => {
                            e.preventDefault();
                            dispatch(searchByTextImplement(e.currentTarget.value));
                        }}
                        type="text"
                        placeholder="Search"
                        className={styles["search-bar"]}
                        name='search' />
                </div>
                <NavLink to="/message" className={styles["msg-icon"]}>
                    <IoIosMail className={styles["faEnvelope"]} />
                    <div className={unreadMsg !== 0 ? styles["alert-icon"] : styles["alert-icon-hidden"]}>{unreadMsg}</div>
                </NavLink>
                <NavLink to="/profile" className={styles["profile-icon"]}>
                    <CgProfile className={styles["faCog"]} />
                </NavLink>
            </nav>
            <div className={styles["navbar-block"]}></div>
        </div>

    )
}

export default NavBar;