import React from 'react';
import styles from './LoginHeader.module.scss';

interface ILoginHeader {
    children: React.ReactNode
}

function LoginHeader(props: ILoginHeader): React.ReactElement {
    return (
        <div className={styles["header"]}>
            <div className={styles["header-text"]}>
                {props.children}
            </div>
        </div>
    )
}

export default LoginHeader;