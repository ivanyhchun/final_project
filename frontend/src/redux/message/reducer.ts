import { MessageAction, messageIsRead } from './action'; 

export interface MessageState {
    messages: {
        id: string, 
        userId: string, 
        namecardId: string,
        content: string,
        firstName: string,
        externalId: string,
        read: boolean, 
        updatedAt: string
    }[]
}

const initialState: MessageState = {
    messages: []
}

export const messageReducer = (state: MessageState = initialState, action: MessageAction): MessageState => {
    if (action.type === "@@message/LOAD_MESSAGE") {
        return {
            ...state, 
            messages: action.messages
        }
    } else if (action.type === "@@message/UPDATE_IS_READ") {
        const newList = [...state.messages]; 
        for (let message of newList){
            message.read = true; 
        }
        return {
            messages: newList
        }
    } else if (action.type === "@@message/DELETE_MESSAGE") {
        let messages = state.messages; 
        let ids = action.messageToDelete; 
        messages = messages.filter(message => !ids.includes(parseInt(String(message.id)))); 
        return {
            ...state,
            messages: messages
        }
    }
    return state; 
}