import { push } from 'connected-react-router';
import { Dispatch } from 'redux';
import { RootState } from '../../store'; 

export function loadMessage(messages: any) {
    return {
        type: "@@message/LOAD_MESSAGE" as const, 
        messages: messages
    }
}

export function updateIsRead() {
    return {
        type: "@@message/UPDATE_IS_READ" as const, 
        read: true
    }
}

export function deleteMsg(msgIds: number[]) {
    return {
        type: "@@message/DELETE_MESSAGE" as const, 
        messageToDelete:msgIds
    }
}

export function fetchMessage() {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/message`, {
            headers: {
                Authorization: 'Bearer ' + getState().auth.token
            }
        }); 
        const result = await res.json(); 
        dispatch(loadMessage(result)); 
    }
}

export function messageIsRead(msgId: number) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/message/?msgId=${msgId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + getState().auth.token
            },
            body: JSON.stringify({
                read: true
            })
        }); 
        await res.json(); 
        dispatch(updateIsRead()); 
    }
}

export function deleteMessage(msgIds: number[]) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/message`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + getState().auth.token
            },
            body: JSON.stringify({
                msgIds: msgIds
            })
        }); 
        if(res.status === 200) {
            const result = res.json(); 
            dispatch(deleteMsg(msgIds)); 
        } else {
            await res.json(); 
        }
    }
}

export type MessageAction = ReturnType<typeof loadMessage> |
                            ReturnType<typeof updateIsRead>| 
                            ReturnType<typeof deleteMsg>; 
