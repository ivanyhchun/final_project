import { Dispatch } from "redux";

export function searchByText(searchText:any) {
    return {
        type: '@@search/BY_TEXT' as const,
        searchText: searchText
    }
}

export function searchByCategory(searchCategoryId:string) {
    return {
        type: '@@search/BY_CATEGORY' as const,
        categoryId: searchCategoryId
    }
}


export function searchByTextImplement(searchText:string){
    return (dispatch: Dispatch)=>{
        dispatch(searchByText(searchText))
    }
}

export function searchByCategoryImplement(categoryId:string){
    return (dispatch: Dispatch)=>{
        dispatch(searchByCategory(categoryId))
    }
}

export type SearchActions =
    ReturnType<typeof searchByText> |
    ReturnType<typeof searchByCategory>;