import { SearchActions } from "./action"

export interface SearchState{
    textSearch?:string,
    categoryId?:string | undefined
}

const initialTextSearchState:SearchState = {
    textSearch:"",
    categoryId:undefined
}

export const searchReducer = (state:SearchState = initialTextSearchState, action:SearchActions): SearchState =>{
    if (action.type === '@@search/BY_TEXT'){
        return {
            ...state,
            textSearch:action.searchText
        }
    } else if (action.type === '@@search/BY_CATEGORY'){
        return {
            ...state,
            categoryId:action.categoryId
        }
    }
    return state
}