import { QRCodeActions } from "./action"

export interface QRCodeState{
    QRCode:{
        id?:string;
        firstName?:string;
        lastName?:string;
        email?:string;
        companyName?:string;
        address?:string;
        position?:string;
        workContact?:string;
        mobile?:string;
        externalId?:string
    },
    QRCodeIsOpen:boolean
}

const initialQRCodeState: QRCodeState = {
    QRCode:{
        id:"",
        firstName:"",
        lastName:"",
        email:"",
        companyName:"",
        address:"",
        position:"",
        workContact:"",
        mobile:"",
        externalId:"",
    },
    QRCodeIsOpen:false
}

export const QRCodeReducer = (state:QRCodeState = initialQRCodeState, action:QRCodeActions): QRCodeState=>{
    if (action.type ==="@@QRCode/GENERATE_QRCODE"){
        return{
            ...state,
            QRCode:action.QRCode
        }
    }else if (action.type ==="@@QRCode/TOGGLE_QRCODE_DISPLAY" ){
        return {
            ...state,
            QRCodeIsOpen:action.openStatus
        }
    }
    return state
}