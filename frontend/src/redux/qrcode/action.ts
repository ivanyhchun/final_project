
export function generateQRCode(input:any)
    {
    return{
        type:"@@QRCode/GENERATE_QRCODE" as const,
        QRCode:input
    }
}

export function toggleQRCodeDisplay(openStatus:boolean){
    return{
        type:"@@QRCode/TOGGLE_QRCODE_DISPLAY" as const,
        openStatus:openStatus
    }
}

export type QRCodeActions =
    ReturnType<typeof generateQRCode> | 
    ReturnType<typeof toggleQRCodeDisplay>;