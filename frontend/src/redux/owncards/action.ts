import { push } from "connected-react-router";
import { Dispatch } from "redux";
import { RootState } from "../../store";

export function loadOwncards(owncards: any) {
    return {
        type: "@@owncards/LOAD_OWNCARDS" as const,
        owncards: owncards
    }
}

export function loadNameCardImg(imageObjectUrl: string | null) {
    return {
        type: "@@owncards/LOAD_NAMECARD_IMAGE" as const,
        imageObjectUrl:imageObjectUrl
    }
}

export function fetchOwncards() {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/owncard/list`, {
            headers: {
                Authorization: 'Bearer ' + getState().auth.token
            }
        });
        const json = await res.json();
        dispatch(loadOwncards(json))
    }
}

export function fetchOneOwncard(id: string) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/owncard/?id=${id}`, {
            headers: {
                Authorization: 'Bearer ' + getState().auth.token
            }
        });
        const json = await res.json();
        dispatch(loadOwncards(json));
    }
}

export function editOwncard(
    id: number,
    firstName: string,
    lastName: string,
    position: string,
    companyName: string,
    address: string,
    workContact: string,
    mobile: string,
    email: string,
    namecardImg: string
) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/owncard`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + getState().auth.token
            },
            body: JSON.stringify({
                id: id,
                firstName: firstName,
                lastName: lastName,
                position: position,
                companyName: companyName,
                address: address,
                workContact: workContact,
                mobile: mobile,
                email: email,
                namecardImg: namecardImg
            })
        });
        if (res.status === 200) {
            const result = await res.json();
        }
    }

}

export function deleteOwncard(cardId: string) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/owncard?id=${cardId}`, {
            method: 'DELETE',
            headers: {
                Authorization: 'Bearer ' + getState().auth.token
            }
        });
        if (res.status === 200) {
            dispatch(push('/profile'));
        } else {
            await res.json();
        }
    }
}

export function getNameCardImg(filename:string){
    return async (dispatch: Dispatch, getState:()=> RootState) =>{
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/owncard/image/${filename}`,{
            headers:{
                Authorization: 'Bearer ' + getState().auth.token
            }
        })
        const resBlob = await res.blob()
        const imageObjectUrl = URL.createObjectURL(resBlob);
        dispatch(loadNameCardImg(imageObjectUrl+''))
    }
}





export type OwncardsAction = ReturnType<typeof loadOwncards>|
                             ReturnType<typeof loadNameCardImg>;


