import {  OwncardsAction } from "./action";

export interface OwncardsState {
    owncards: {
        id: string,
        firstName: string,
        lastName: string,
        email: string,
        companyName: string,
        address: string,
        position: string,
        workContact: string,
        mobile: string,
        namecardImg: string,
        imageObjectUrl:string
    }[]
}

const initialState: OwncardsState = {
    owncards: []
}

export const owncardsReducer = (state: OwncardsState = initialState, action: OwncardsAction): OwncardsState => {
    if (action.type === '@@owncards/LOAD_OWNCARDS') {
        return {
            ...state,
            owncards: action.owncards
        }
    } else if (action.type === '@@owncards/LOAD_NAMECARD_IMAGE') {
        const newList = [...state.owncards]
        newList[0] = {
            ...newList[0],
            imageObjectUrl:action.imageObjectUrl+""
        }
        return {
            owncards: newList
        }
    } 
    return state;
}

