import { push } from "connected-react-router";
import { Dispatch } from "redux";
import { RootState } from "../../store";

export function loadNamecardsList(namecardsList: any) {
    return {
        type: '@@namecards/LOAD_NAMECARDS_LIST' as const,
        namecardsList: namecardsList
    }
}

export function loadNamecardProfileImages(index: number, profileImgURL: string) {
    return {
        type: '@@namecards/LOAD_NAMECARDS_PROFILE_IMG' as const,
        index: index,
        profileImgURL: profileImgURL
    }
}

export function loadOneNamecard(oneNamecard: any) { 
    return {
        type: '@@namecards/LOAD_ONE_NAMECARD' as const,
        oneNamecard: oneNamecard
    }
}

export function loadOneNamecardImage(namecardImgURL:string){
    return {
        type: '@@namecards/LOAD_ONE_NAMECARD_IMG' as const,
        namecardImgURL: namecardImgURL
    }
}
export function loadOneNamecardProfileImage(profileImgURL:string){
    return {
        type: '@@namecards/LOAD_ONE_NAMECARD_PROFILE_IMG' as const,
        profileImgURL: profileImgURL
    }
}

export function addOneNamecard(oneNamecard: any) {
    return {
        type: '@@namecards/ADD_ONE_NAMECARD' as const,
        oneNamecard: oneNamecard
    }
}

export function updateOneNamecard(oneNamecard: any) {
    return {
        type: '@@namecards/UPDATE_ONE_NAMECARD' as const,
        oneNamecard: oneNamecard
    }
}

export function updateOneNamecardCategory(categoryName: string) {
    return {
        type: '@@namecards/UPDATE_ONE_NAMECARD_CATEGORY' as const,
        categoryName: categoryName
    }
}

export function deleteOneNamecard(oneNamecard: any) {
    return {
        type: '@@namecards/DELETE_ONE_NAMECARD' as const,
        oneNamecard: oneNamecard
    }
}

export function updateNamecardFav(favorite: any) {
    return {
        type: '@@namecards/UPDATE_NAMECARD_FAV' as const,
        favorite: favorite
    }
}

export function fetchNamecards() {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        const namecardListResponse = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/namecard/list`, {
            headers: {
                Authorization: 'Bearer ' + getState().auth.token
            }
        });
        const namecardListArray = await namecardListResponse.json();
        dispatch(loadNamecardsList(namecardListArray))
        // obtain URL for default profile pic
        const defaultProfileImageResponse = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/userprofile/image/default-profile.jpeg`, {
            headers: {
                Authorization: 'Bearer ' + getState().auth.token
            }
        })
        const defaultProfileImageBlob = await defaultProfileImageResponse.blob();
        const defaultProfileImageURL = URL.createObjectURL(defaultProfileImageBlob);
        // obtain URL for individual profile pic
        for (let i = 0; i < namecardListArray.length; i++) {
            if (namecardListArray[i].profileImg) {
                const namecardProfileImageResponse = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/userprofile/image/${namecardListArray[i].profileImg}`, {
                    headers: {
                        Authorization: 'Bearer ' + getState().auth.token
                    }
                })
                const namecardProfileImageBlob = await namecardProfileImageResponse.blob();
                const namecardProfileImageURL = URL.createObjectURL(namecardProfileImageBlob);
                dispatch(loadNamecardProfileImages(i, namecardProfileImageURL))
            } else {
                dispatch(loadNamecardProfileImages(i, defaultProfileImageURL))
            }
        }
    }
}

export function fetchOneCard(externalId: string) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/namecard/?externalId=${externalId}`, {
            headers: {
                Authorization: 'Bearer ' + getState().auth.token
            }
        });
        const oneNameCardArray = await res.json();
        dispatch(loadOneNamecard(oneNameCardArray[0]))
        let oneNamecardProfileImageResponse
        if (oneNameCardArray[0].profileImg) {
            oneNamecardProfileImageResponse = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/userprofile/image/${oneNameCardArray[0].profileImg}`, {
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token
                }
            });
        } else {
            oneNamecardProfileImageResponse = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/userprofile/image/default-profile.jpeg`, {
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token
                }
            });
        }
        const namecardProfileImageBlob = await oneNamecardProfileImageResponse.blob();
        const namecardProfileImageURL = URL.createObjectURL(namecardProfileImageBlob);
        dispatch(loadOneNamecardProfileImage(namecardProfileImageURL))
        let oneNamecardImageResponse
        // non-member-card with no memberId
        if (!oneNameCardArray[0].memberId){
            oneNamecardImageResponse = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/namecard/camera/image/${oneNameCardArray[0].namecardImg}`, {
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token
                }
            });
        } else {
            oneNamecardImageResponse = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/owncard/image/${oneNameCardArray[0].namecardImg}`, {
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token
                }
            });
        }
        const namecardImageBlob = await oneNamecardImageResponse.blob();
        const namecardImageURL = URL.createObjectURL(namecardImageBlob);
        dispatch(loadOneNamecardImage(namecardImageURL))
    }
}

export function createNewCard(
    firstName:string,
    lastName:string,
    companyName:string,
    address:string,
    position:string,
    workContact:string,
    mobile:string,
    email:string,
    method:string,
    categoryIDorName?:string,
    namecardImg?:string,){
    return async (dispatch: Dispatch, getState: () => RootState) =>{
        const res = await fetch (`${process.env.REACT_APP_BACKEND_HOST}/namecard?method=${method}`,{
            method:'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + getState().auth.token
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                position: position,
                companyName: companyName,
                address: address,
                workContact: workContact,
                mobile: mobile,
                email: email,
                namecardImg: namecardImg,
                categoryIDorName:categoryIDorName
            })
        })
        if (res.status === 200) {
            dispatch(push('/homepage' ));
        } else {
            alert("Upload failed due to server error. Please try again later.")
        }
    }
}

export function editNonMemberCard(
    cardId: string,
    firstName: string,
    lastName: string,
    position: string,
    companyName: string,
    address: string,
    workContact: string,
    mobile: string,
    email: string,
    namecardImg: string,
    categoryIDorName:string
) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/namecard?externalId=${cardId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + getState().auth.token
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                position: position,
                companyName: companyName,
                address: address,
                workContact: workContact,
                mobile: mobile,
                email: email,
                namecardImg: namecardImg,
                categoryIDorName:categoryIDorName
            })
        });
        if (res.status === 200) {
            const result = await res.json();
            dispatch(push('/homepage/' + cardId));
        }
    }
}


export function deleteNamecard(memberId: number | null, cardId: string) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        let isMember = false;
        if (memberId !== null) {
            isMember = true;
        }
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/namecard/?id=${cardId}&isMember=${isMember}`, {
            method: 'DELETE',
            headers: {
                Authorization: 'Bearer ' + getState().auth.token
            }
        });
        if (res.status === 200) {
            dispatch(push('/homepage'));
        } else {
            await res.json();
        }
    }
}

export function addFavorite(externalId: string, favorite: boolean) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/namecard/favorite?externalId=${externalId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + getState().auth.token
            },
            body: JSON.stringify({
                favorite: favorite
            })
        });
        await res.json();
        dispatch(updateNamecardFav(favorite));
    }
}





export type NamecardsListActions =
    ReturnType<typeof loadNamecardsList> |
    ReturnType<typeof loadNamecardProfileImages>;

export type OneNamecardActions =
    ReturnType<typeof loadOneNamecard> |
    ReturnType<typeof loadOneNamecardImage> |
    ReturnType<typeof loadOneNamecardProfileImage> |
    ReturnType<typeof addOneNamecard> |
    ReturnType<typeof updateOneNamecard> |
    ReturnType<typeof deleteOneNamecard> |
    ReturnType<typeof updateNamecardFav> |
    ReturnType<typeof updateOneNamecardCategory>


