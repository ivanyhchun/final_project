import { NamecardsListActions, OneNamecardActions } from "./action"

export interface NamecardsListState{
    namecardsList:{
        id:string;
        firstName:string;
        lastName:string;
        position:string;
        companyName:string;
        externalId:string
        categoryId:string;
        categoryName:string;
        memberId:number | null;
        profileImg?:string; 
        profileImgURL?:string | null;
        favorite: boolean; 
        updatedAt: string; 
    }[]
}

export interface OneNamecardState{
    oneNamecard:{
        id:string;
        firstName:string;
        lastName:string;
        email:string;
        companyName:string;
        address:string;
        position:string;
        workContact:string;
        mobile:string;
        namecardImg:string;
        namecardImgURL:string | null
        externalId:string
        categoryId:string;
        categoryName:string;
        memberId:number | null;
        profileImg?:string |null;
        profileImgURL?:string | null;
        favorite: boolean; 
    }
}

const initialNamecardListState: NamecardsListState = {
    namecardsList:[]
}

const initialOneNamecardState: OneNamecardState = {
    oneNamecard:{
        id:"",
        firstName:"",
        lastName:"",
        email:"",
        companyName:"",
        address:"",
        position:"",
        workContact:"",
        mobile:"",
        namecardImg:"",
        namecardImgURL:"",
        externalId:"",
        categoryId:"",
        categoryName:"",
        memberId:null,
        profileImg:"",
        profileImgURL:"",
        favorite:false
    }
}

export const namecardsListReducer = (state:NamecardsListState = initialNamecardListState, action:NamecardsListActions): NamecardsListState =>{
    if (action.type === '@@namecards/LOAD_NAMECARDS_LIST'){
        return {
            ...state,
            namecardsList:action.namecardsList
        }
    } else if (action.type === '@@namecards/LOAD_NAMECARDS_PROFILE_IMG'){
        const newList = [...state.namecardsList]
        newList[action.index] = {
            ...newList[action.index],
            profileImgURL: action.profileImgURL
        }
        return {
            namecardsList: newList
        }
    }
    return state
}

export const oneNamecardReducer = (state:OneNamecardState = initialOneNamecardState,action:OneNamecardActions):OneNamecardState=>{
    if(action.type === "@@namecards/LOAD_ONE_NAMECARD"){
        return {
            ...state,
            oneNamecard:action.oneNamecard
        }
    } else if (action.type === "@@namecards/LOAD_ONE_NAMECARD_IMG"){
        return {
            oneNamecard:{
                ...state.oneNamecard,
                namecardImgURL:action.namecardImgURL               
            }
        }
    }else if (action.type === "@@namecards/LOAD_ONE_NAMECARD_PROFILE_IMG"){
        return {
            oneNamecard:{
                ...state.oneNamecard,
                profileImgURL:action.profileImgURL               
            }
        }
    } else if (action.type === '@@namecards/ADD_ONE_NAMECARD') {
        return {
            ...state,
            oneNamecard:action.oneNamecard
        }
    } else if (action.type === '@@namecards/UPDATE_ONE_NAMECARD') {
        return {
            ...state,
            oneNamecard:action.oneNamecard
        }
    } else if (action.type === '@@namecards/DELETE_ONE_NAMECARD') {
        return {
            ...state,
            oneNamecard:action.oneNamecard
        }
    } else if (action.type === '@@namecards/UPDATE_NAMECARD_FAV') {
        return {
            oneNamecard: {
                ...state.oneNamecard,
                favorite: !action.favorite
            }
        }
    }else if (action.type === '@@namecards/UPDATE_ONE_NAMECARD_CATEGORY') {
        return {
            oneNamecard: {
                ...state.oneNamecard,
                categoryName: action.categoryName
            }
        }
    }
    return state
}