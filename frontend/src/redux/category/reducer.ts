import { CategoryListActions } from "./action"

export interface CategoryListState{
    categoryList:{
        categoryId:string;
        categoryName:string;
    }[]
}

const initialCategoryListState: CategoryListState = {
    categoryList:[]
}

export const categoryReducer = (state:CategoryListState = initialCategoryListState, action:CategoryListActions): CategoryListState =>{
    if (action.type === '@@category/LOAD_CATEGORY_LIST'){
        return {
            ...state,
            categoryList:action.categoryList
        }
    } 
    return state
}
