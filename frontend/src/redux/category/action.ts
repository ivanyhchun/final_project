import { Dispatch } from "redux"
import { RootState } from "../../store"
import { updateOneNamecardCategory } from "../namecards/action"

export function loadCategoryList(categoryList:[]) {
    return {
        type: '@@category/LOAD_CATEGORY_LIST' as const,
        categoryList: categoryList
    }
}

export function fetchCategoryList(){
    return async (dispatch: Dispatch, getState: () => RootState)=>{
        const categoryListResponse = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/category`,{
            headers: {
                Authorization: 'Bearer ' + getState().auth.token
            }
        })
        const categoryListArray = await categoryListResponse.json()
        dispatch(loadCategoryList(categoryListArray))
    }
}

export function updateNamecardCategory(namecardId:string,namecardOwner:number|null, categoryId:number|string|null,categoryName:string|null){
    return async (dispatch: Dispatch, getState: () => RootState)=>{
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/namecard/catgory`,{
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + getState().auth.token
            },
            body:JSON.stringify({
                categoryId: categoryId + '',
                categoryName: categoryName + '',
                namecardId:namecardId,
                namecardOwner:namecardOwner+''
            })
        })
        if (res.status == 200){
            fetchCategoryList()
            dispatch(updateOneNamecardCategory(categoryName+''))
        } else {
            alert("Update category failed")
        }
    }
}


export type CategoryListActions =
ReturnType<typeof loadCategoryList> ;
