import { DataAction } from "./action"

export interface DataState {
    data: {
        id: string,
        firstName: string,
        lastName: string,
        email: string,
        companyName: string,
        address: string,
        position: string,
        workContact: string,
        mobile: string,
        namecardImg: string
        bgImg:string,
        bgImgUrl:string|null
    }
}

const initialDataState: DataState = {
    data: {
        id: "",
        firstName: "",
        lastName: "",
        email: "",
        companyName: "",
        address: "",
        position: "",
        workContact: "",
        mobile: "",
        namecardImg: "",
        bgImg: "",
        bgImgUrl:""

    }
}

export const dataReducer = (state: DataState = initialDataState, action: DataAction): DataState => {
    if (action.type === '@@owncards/LOAD_INPUTDATA') {
        return {
            ...state,
            data: action.data
        }
    } else if (action.type === '@@owncards/INPUTDATAID') {
        return {
            data: {
                ...state.data,
                id: action.dataId
            }
        }
    }else if (action.type === '@@owncards/LOAD_BGIMAGEURL'){
        return {
            data: {
                ...state.data,
                bgImgUrl:action.bgImgUrl
            }
        }
    }
    return state
}

