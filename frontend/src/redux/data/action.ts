import { push } from "connected-react-router";
import { Dispatch } from "redux";
import { RootState } from "../../store";

export function loadInputData(data: any) {
    return {
        type: "@@owncards/LOAD_INPUTDATA" as const,
        data: data
    }
}


export function InputDataId(dataId: any) {
    return {
        type: "@@owncards/INPUTDATAID" as const,
        dataId: dataId
    }
}



export function loadbgUrl(bgImgUrl:string|null){
    return{
        type: "@@owncards/LOAD_BGIMAGEURL" as const,
        bgImgUrl:bgImgUrl
    }

}

export function createOwncard(data:{}) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/owncard`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + getState().auth.token
            },
            body: JSON.stringify({
                data:data
            })
        });
        if (res.status === 200) {
            const json = await res.json();
            dispatch(InputDataId(json.cardId + ""));
            dispatch(push('/DND/'+json.cardId)); 
        }
    }
}

export function fetchCardData(id:string){
    return async (dispatch: Dispatch, getState:()=> RootState)=>{
        const cardData = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/owncard/?id=${id}`, {
            headers: {
                Authorization: 'Bearer ' + getState().auth.token
            }
        });;
        const data = await cardData.json();
        dispatch(loadInputData(data[0]))
        const bgImageResponse = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/owncard/backgroundimage/${data[0].bgImg}`, {
            headers: {
                Authorization: 'Bearer ' + getState().auth.token
            }
        })
        const bgImageBlob = await bgImageResponse.blob();
        const bgImageURL = URL.createObjectURL(bgImageBlob);
        dispatch(loadbgUrl(bgImageURL))
    }
}

export type DataAction = ReturnType<typeof loadInputData> |
                         ReturnType<typeof InputDataId>|
                         ReturnType<typeof loadbgUrl>;

