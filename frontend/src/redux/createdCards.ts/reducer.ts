
import { CreatedCardsAction } from "./action";

export interface CreatedCardsState {
    cardObj:ICardObj
}

export interface ICardObj{
    id: string,
    user_id: string,
    namecard_id: string,
    content: ICardData,
    logoUrl: string | null
}

const initialState: CreatedCardsState = {
    cardObj: {
        id: "",
        user_id: "",
        namecard_id: "",
        content: {
            firstName: { top: 0, left: 0, title: ""},
            companyName: { top: 0, left: 0, title: ""},
            lastName: { top: 0, left: 0, title: ""},
            address: { top: 0, left: 0, title: ""},
            position:{ top: 0, left: 0, title: ""} ,
        },
        logoUrl: ""
    }
}

export interface Box {
    top: number; left: number; title: string
}

export interface ICardData {
    firstName: Box,
    companyName: Box,
    lastName: Box,
    email?: Box,
    address: Box,
    position: Box,
    workContact?: Box,
    mobile?: Box,
    logo?: Box
}


export const createdCardsReducer = (state: CreatedCardsState = initialState, action: CreatedCardsAction): CreatedCardsState => {
    if (action.type === '@@createdCards/LOAD_CARDOBJ') {
        return {
            ...state,
            cardObj: {
                ...action.cardObj,
                content: action.cardObj.content
            }
        }
    } else if (action.type === '@@owncards/LOAD_LOGOURL') {
        return {
            ...state,
            cardObj: {
                ...state.cardObj,
                content: {
                    ...state.cardObj.content,
                    logo: {
                        top: 0,
                        left: 0,
                        title: action.logoUrl+""
                    }
                }
            }
        }
    } else if (action.type === '@@owncards/DEL_LOGOURL') {
        const newData = {
            cardObj: {
                ...state.cardObj,
                content: {
                    ...state.cardObj.content,
                }
            }
        }
        delete newData.cardObj.content.logo
        return newData
    }

    return state;
}