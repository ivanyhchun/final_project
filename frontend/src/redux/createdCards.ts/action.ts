import { Dispatch } from "redux";
import { RootState } from "../../store";
import { ICardData, ICardObj } from "./reducer";

export function loadcardObj(cardObj:ICardObj) {
    return {
        type: "@@createdCards/LOAD_CARDOBJ" as const,
        cardObj: cardObj
    }
}

export function loadlogoUrl(logoUrl:string|null){
    return{
        type: "@@owncards/LOAD_LOGOURL" as const,
        logoUrl:logoUrl
    }
}

export function delLogoStore(){
    return{
        type: "@@owncards/DEL_LOGOURL" as const,
    }
}
export function saveCard(id:number,boxes:ICardData){
    let afterBoxes:ICardData = {
        firstName: { top: boxes.firstName.top/(window.innerWidth*0.4), left: boxes.firstName.left/(window.innerWidth*0.7), title: boxes.firstName.title },
        lastName: { top: boxes.lastName.top/(window.innerWidth*0.4), left: boxes.lastName.left/(window.innerWidth*0.7), title: boxes.lastName.title },
        companyName: { top: boxes.companyName.top/(window.innerWidth*0.4), left: boxes.companyName.left/(window.innerWidth*0.7), title: boxes.companyName.title },
        address: { top: boxes.address.top/(window.innerWidth*0.4), left: boxes.address.left/(window.innerWidth*0.7), title: boxes.address.title },
        position: { top: boxes.position.top/(window.innerWidth*0.4), left: boxes.position.left/(window.innerWidth*0.7), title: boxes.position.title },
    }
    if(boxes.email){
        afterBoxes = {
            ...afterBoxes,
            email: { top: boxes.email.top/(window.innerWidth*0.4), left: boxes.email.left/(window.innerWidth*0.7), title: boxes.email.title }
        }
    }
    if (boxes.mobile) {
        afterBoxes = {
          ...afterBoxes,
          mobile: { top: boxes.mobile.top / (window.innerWidth*0.4), left: boxes.mobile.left / (window.innerWidth*0.7), title: boxes.mobile.title }
        }
      }
    if(boxes.workContact){
        afterBoxes = {
            ...afterBoxes,
            workContact: { top: boxes.workContact.top/(window.innerWidth*0.4), left: boxes.workContact.left/(window.innerWidth*0.7), title: boxes.workContact.title }
        }
    }
    if(boxes.logo){
        afterBoxes = {
            ...afterBoxes,
            logo: { top: boxes.logo.top/(window.innerWidth*0.4), left: boxes.logo.left/(window.innerWidth*0.7), title: boxes.logo.title }
        }
    }

    return async (dispatch: Dispatch, getState:()=> RootState) => {
        let res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/cardElements/?nameCardId=${id ? id : ""}`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + getState().auth.token
            },
            body: JSON.stringify(afterBoxes)
          });
    }
}

export function postBackgroundImg(image:File|string,id:number){
    return async (dispatch: Dispatch, getState:()=> RootState) => {
        let formData = new FormData();
        formData.append("image",image)
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/owncard/backgroundimage?id=${id}`, {
            method:"PUT",
            headers: {
                Authorization: 'Bearer ' + getState().auth.token
            },
            body: formData,
        });
    }
}

export function PostLogoImg(newURL:string,id:number){
    return async (dispatch: Dispatch, getState:()=> RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/owncard/logoimage?id=${id}`, {
            method:"PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + getState().auth.token
            },
            body: JSON.stringify({
                imagedata64:newURL
            })
        });
      dispatch(loadlogoUrl(newURL));
    }
}

export function delLogo(id:string){
    return async (dispatch: Dispatch, getState:()=> RootState) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/owncard/logoimage?id=${id}`, {
        method:'DELETE',
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + getState().auth.token
        }
      })
      dispatch(delLogoStore());
    };
}

export function fetchBoxObj(id:string){
    return async (dispatch: Dispatch, getState:()=> RootState)=>{
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/cardObj?id=${id}`, {
            headers: {
                Authorization: 'Bearer ' + getState().auth.token
            }
        });
        if (res.status == 200){
            const cardObjresult = await res.json();
            if(cardObjresult.length>0){
                let cardObj = cardObjresult[0]
                cardObj.content = JSON.parse(cardObj.content)
                dispatch(loadcardObj(cardObj))
            }
        }else {
            return
        }
    }
}
export type CreatedCardsAction = ReturnType<typeof loadcardObj>|
                                 ReturnType<typeof loadlogoUrl>|
                                 ReturnType<typeof delLogoStore>;