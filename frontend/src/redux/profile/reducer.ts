import { ProfileAction } from "./action";

export interface ProfileState {
    profile: {
        id?: string,
        firstName?: string,
        lastName?: string,
        email?: string,
        profileImg?: string | null,
        imageObjectUrl?:string | null
    }
}

const initialState: ProfileState = {
    profile: {
        id:'',
        firstName:'',
        lastName:'',
        email:'',
        profileImg:null,
        imageObjectUrl:null
    }
}

export const profileReducer = (state: ProfileState = initialState, action: ProfileAction): ProfileState => {
    if (action.type === "@@profile/LOAD_PROFILE") {
        return {
            ...state,
            profile: action.profile
        }
    }else if (action.type === "@@profile/LOAD_PROFILE_IMAGE"){
        return {
            profile:{
                ...state.profile,
                imageObjectUrl:action.imageObjectUrl
            }
        }
    }else if (action.type === "@@profile/UPDATE_PROFILE_IMAGE") {
        return {
            profile:{
                ...state.profile,
                profileImg:action.profileImg
            }
        }
    }
    return state;
}