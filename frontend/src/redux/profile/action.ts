import { push } from 'connected-react-router';
import { Dispatch } from 'redux';
import { RootState } from '../../store';

export function loadProfile(profile: any) {
    return {
        type: "@@profile/LOAD_PROFILE" as const,
        profile: profile
    }
}

export function loadProfileImage(imageObjectUrl: string | null) {
    return {
        type: "@@profile/LOAD_PROFILE_IMAGE" as const,
        imageObjectUrl:imageObjectUrl
    }
}

export function updateProfileImage(profileImg: string) {
    return {
        type: "@@profile/UPDATE_PROFILE_IMAGE" as const,
        profileImg:profileImg
    }
}


export function fetchProfile() {
    return async (dispatch: Dispatch, getState:()=> RootState) => {
        const textResponse = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/userprofile`, {
            headers: {
                Authorization: 'Bearer ' + getState().auth.token
            }
        });
        const profile = await textResponse.json();
        dispatch(loadProfile(profile[0])); 
        let imageResponseBlob,imageObjectUrl,imageResponse;
        if (profile[0].profileImg){
            imageResponse = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/userprofile/image/${profile[0].profileImg}`,{
                headers:{
                    Authorization: 'Bearer ' + getState().auth.token
                }
            })
        } else {
            imageResponse = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/userprofile/image/default-profile.jpeg`,{
                headers:{
                    Authorization: 'Bearer ' + getState().auth.token
                }
            })
        }
        imageResponseBlob = await imageResponse.blob();
        imageObjectUrl = URL.createObjectURL(imageResponseBlob);
        dispatch(loadProfileImage(imageObjectUrl)); 
    }
}

export function editProfileName(firstName: string, lastName: string) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/userprofile`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + getState().auth.token
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName
            })
        })
        if (res.status === 200) {
            const result = await res.json();
            dispatch(push('/profile'));
        }
    }
}

export function getProfileImage(filename:string){
    return async (dispatch: Dispatch, getState:()=> RootState) =>{
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/userprofile/image/${filename}`,{
            headers:{
                Authorization: 'Bearer ' + getState().auth.token
            }
        })
        const resBlob = await res.blob()
        const imageObjectUrl = URL.createObjectURL(resBlob);
        dispatch(loadProfileImage(imageObjectUrl+''))
    }
}

export function PostProfileImage(image:File) {
    return async (dispatch: Dispatch, getState:()=> RootState) => {
        let formData = new FormData();
        formData.append("image",image)
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/userprofile/image`, {
            method:"PUT",
            headers: {
                Authorization: 'Bearer ' + getState().auth.token
            },
            body: formData,
        });
        const updatedProfileImgPath = await res.json();
        if (res.status === 200){
            dispatch(updateProfileImage(updatedProfileImgPath)); 
        } else if (res.status === 401) {
            alert('Upload profile image failed. Please try again')
        }
    }
}

export function chgPassword(oldPassword: string, newPassword: string) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/password`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + getState().auth.token
            },
            body: JSON.stringify({
                oldPassword: oldPassword,
                newPassword: newPassword
            })
        })
        const result = await res.json();
        if(res.status === 201) {
            dispatch(push('/profile')); 
        } else {
            alert(result.message);   
        }
    }
}

export type ProfileAction = ReturnType<typeof loadProfile> | 
                            ReturnType<typeof loadProfileImage> | 
                            ReturnType<typeof updateProfileImage>
