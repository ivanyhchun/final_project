import { push } from 'connected-react-router'
import { Dispatch } from "redux";
import vCard from 'vcf'
import { RootState } from '../../store';

export function processQRCode(result:string){
    return async (dispatch: Dispatch, getState: () => RootState)=>{
        let cardData = new vCard().parse(result)
        if (!cardData.data.note.valueOf()){
            alert("Only member's QR code is supported.")
        } else {
            const externalId = cardData.data.note.valueOf()
            const res = await fetch (`${process.env.REACT_APP_BACKEND_HOST}/namecard/qrcode/${externalId}`,{
                method: 'POST',
                headers: {
                    Authorization: 'Bearer ' + getState().auth.token
                }
            })
            if (res.status == 200) {
                dispatch(push('/homepage/' + externalId))
            } else if (res.status == 202) {
                alert("This card has been added previously. Redirect to card page.")
                dispatch(push('/homepage/' + externalId))
            }
            if (res.status == 404){
                alert("Error: this QR code is not valid. The owner may have removed this card.")
            } 
        }
    }
}
