
import { push } from "connected-react-router"
import { Dispatch } from "redux"

export function loginSuccess(userId: number, token: string) {
    return {
        type: '@@auth/LOGIN_SUCCESS' as const,
        userId,
        token
    }
}
export function loginFailure() {
    return {
        type: '@@auth/LOGIN_FAILURE' as const
    }
}
export function logoutSuccess() {
    return {
        type: '@@auth/LOGOUT_SUCCESS' as const
    }
}

export type AuthActions =
    ReturnType<typeof loginSuccess> |
    ReturnType<typeof loginFailure> |
    ReturnType<typeof logoutSuccess>

export function login(email: string, password: string) {
    return async (dispatch: Dispatch) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        const json = await res.json();
        if (json.token) {
            localStorage.setItem('token', json.token)
            dispatch(loginSuccess(json.userId, json.token))
            dispatch(push('/homepage'))
        } else {
            dispatch(loginFailure())
            alert(`Wrong Username/Password!`)
        }
    }
}

export function checkLogin(){
    return async ( dispatch:Dispatch) =>{
        const token = localStorage.getItem('token')
        if(token == null ){
            dispatch(logoutSuccess())
            return;
        }
        const res= await fetch (`${process.env.REACT_APP_BACKEND_HOST}/current-user`,{
            headers:{
                'Authorization': `Bearer ${token}`
            }
        })
        const json = await res.json();
        if(json.id){
            dispatch(loginSuccess(json.id, token))
        }else {
            dispatch(logoutSuccess())
        }
    }
}

export function logout(){
    return ( dispatch:Dispatch)=>{
        localStorage.removeItem('token')
        dispatch(logoutSuccess())
    }
}

export function signup(firstName:string,lastName:string,email:string,password:string){
    return async (dispatch:Dispatch)=>{
        const res = await fetch (`${process.env.REACT_APP_BACKEND_HOST}/signup`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email:email,
                password:password
            })
        });
        if (res.status === 406){
            alert("User with same email already exist. Please try with another email");
            dispatch(push('/signup'));
        }
        const json = await res.json();
        if (json.token) {
            localStorage.setItem('token', json.token)
            dispatch(loginSuccess(json.userId, json.token))
            dispatch(push('/profile'))
        }
    }
}