import { AuthActions } from "./action"

export interface AuthState {
    isAuthenticated: boolean|null,
    userId: number | null,
    token: string
}

const initialState: AuthState = {
    isAuthenticated: null,
    userId: null,
    token: ''
}

export const authReducer = (state: AuthState = initialState, action: AuthActions): AuthState => {
    if (action.type === '@@auth/LOGIN_SUCCESS') {
        return {
            ...state,
            isAuthenticated: true,
            userId: action.userId,
            token: action.token
        }
    } else if (action.type === '@@auth/LOGIN_FAILURE') {
        return {
            ...state,
            isAuthenticated: false,
            userId: null,
            token: ''
        }
    } else if (action.type === '@@auth/LOGOUT_SUCCESS') {
        return {
            ...state,
            isAuthenticated: false,
            userId: null,
            token: ''
        }
    }
    return state;
}