import { push } from "connected-react-router"
import { Dispatch } from "redux"

export function drawCardData(data:[]) {
    return {
        type: '@@drawcard/DRAWCARD_DATA' as const,
        dataList:data
    }
}


export type DrawActions =
    ReturnType<typeof drawCardData>

export function drawCard() {
    return async (dispatch: Dispatch) => {
    }
}
