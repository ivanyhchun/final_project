import { DrawActions } from "./action"

export interface DataState {
    
}

const initialState: DataState = {

}

export const authReducer = (state: DataState = initialState, action: DrawActions): DataState => {
    if (action.type === '@@drawcard/DRAWCARD_DATA') {
        return {
            ...state,
        }
    }
    return state;
}