import React, { useState, useEffect } from 'react';
import './App.css';
import Login from './Login';
import Signup from './Signup';
import CreateCard from './CreateCard';
import ScanEditCard from './ScanEditCard';
import Homepage from './Homepage';
import ProfilePage from './ProfilePage';
import EditProfile from './EditProfile';
import UploadQr from './scannings/UploadQr';
import CardDetail from './CardDetail';
import { Route, Switch } from 'react-router-dom';
import QRScan from './scannings/QRScan';
import { useDispatch, useSelector } from 'react-redux';
import { checkLogin } from './redux/auth/action';
import { RootState } from './store';
import ResetPassword from './ResetPassword';
import OwncardDetail from './OwncardDetail';
import CameraScan from './scannings/CameraScan'
import InputForm from './InputForm';
import DND from './DND/DND';
import CreateOwncard from './CreateOwncard';
import EditNamecard from './EditNamecard';
import ReviewScannedCardForm from './scannings/ReviewScannedCardForm';
import CreateManualNamecard from './CreateManualNamecard';
import MessageCenter from './MessageCenter';
import IndexPage from './IndexPage';

function App() {
  const dispatch = useDispatch()
  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated)
  useEffect(() => {
    dispatch(checkLogin())
  }, [dispatch])
  const [openMenu, setOpenMenu] = useState(true);

  if (isAuthenticated === null) {
    return (
      <div>
        Loading...
        Loading...
      </div>
    )
  }
  return (
    <div className="App">
      {
        !isAuthenticated &&
        <div>
          <Switch>
            <Route path='/login'><Login /></Route>
            <Route path='/signup'><Signup /></Route>
            <Route path='/'><IndexPage /></Route>
          </Switch>
        </div>
      }
      {
        isAuthenticated &&
        <div>

          <Route path='/create-card' exact><CreateCard /> </Route>
          <Route path='/scan-edit-card' exact><ScanEditCard /></Route>
          <Route path='/homepage' exact><Homepage menuState={openMenu} setMenuState={() => {
            setOpenMenu(openMenu === false)
          }}/></Route>
          <Route path='/homepage/:id' exact><CardDetail /></Route>

          <Route path='/login'><Login /></Route>
          <Route path='/CameraScan'><CameraScan /></Route>
          <Route path='/QRScan' exact><QRScan /></Route>
          <Route path='/uploadQR' exact><UploadQr /></Route>
          <Route path='/CreateManualNamecard'><CreateManualNamecard /></Route>
          <Route path='/InputForm' exact><InputForm /></Route>
          <Route path='/DND/:id'><DND /></Route>
          <Route path='/scan/review'><ReviewScannedCardForm /></Route>
          <Route path='/message' exact><MessageCenter /></Route>
          <Route path='/profile' exact><ProfilePage menuState={openMenu} setMenuState={() => {
            setOpenMenu(openMenu === false)
          }} /></Route>
          <Route path='/profile/:id' exact><OwncardDetail setMenuState={() => {
            setOpenMenu(openMenu === false)
          }} /></Route>
          <Route path='/editProfile' exact><EditProfile setMenuState={() => {
            setOpenMenu(openMenu === false)
          }} /></Route>
          <Route path='/resetPassword' exact><ResetPassword setMenuState={() => {
            setOpenMenu(openMenu === false)
          }} /></Route>
          <Route path='/create-owncard' exact><CreateOwncard /></Route>
          <Route path='/' exact><Homepage menuState={openMenu} setMenuState={() => {
            setOpenMenu(openMenu === false)
          }}/></Route>
          <Route path='/homepage/:id/editNamecard' exact><EditNamecard /></Route>
        </div>
      }
    </div>
  );
}

export default App;
