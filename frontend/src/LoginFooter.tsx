import React from 'react';
import styles from './LoginFooter.module.scss';

interface ILoginFooter {
    children: React.ReactNode
}

function LoginFooter(props: ILoginFooter): React.ReactElement {
    
    return (
        <div className={styles["footer"]}>
            <div className={styles["footer-text"]}>
                {props.children}
            </div>
        </div>
    )

}

export default LoginFooter;