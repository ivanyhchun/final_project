import { CSSProperties, FC, useEffect } from 'react'
import { useDrag } from 'react-dnd'
import { ItemTypes } from './ItemTypes'

export const style: CSSProperties = {
  position: 'absolute',
  // border: '1px dashed gray',
  padding: '0.5rem 1rem',
  cursor: 'move',
  color: 'black',
  fontSize:'3vw'
  
}

export interface BoxProps {
  id: any
  left: number
  top: number
  hideSourceOnDrag?: boolean
  onDrag?:()=>void
}

export const Box: FC<BoxProps> = ({
  id,
  left,
  top,
  hideSourceOnDrag,
  onDrag,
  children,
}) => {
  const [{ isDragging }, drag] = useDrag(
    () => ({
      type: ItemTypes.BOX,
      item: { id, left, top },
      collect: (monitor) => ({
        isDragging: monitor.isDragging(),
      }),
    }),
    [id, left, top],
  )

  useEffect(()=>{
    if(onDrag){
      onDrag()
    }
  },[isDragging])

  if (id == 'firstName') {
    return (
      <div ref={drag} style={{ ...style, left, top , fontSize:'5vw' }} role="Box">
        {children}
      </div>
    )
  }
  if (id == 'lastName') {
    return (
      <div ref={drag} style={{ ...style, left, top , fontSize:'5vw' }} role="Box">
        {children}
      </div>
    )
  }
  return (
    <div ref={drag} style={{ ...style, left, top }} role="Box">
      {children}
    </div>
  )
}
