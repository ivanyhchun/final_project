import React, { CSSProperties, FC, useCallback, useEffect, useState, useRef } from 'react'
import { useDrop, XYCoord } from 'react-dnd'
import { ItemTypes } from './ItemTypes'
import { Box } from './Box'
import { DragItem } from './interfaces'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../store'
import styles from './Container.module.scss';
import { fetchCardData, loadbgUrl } from '../redux/data/action'
import html2canvas from 'html2canvas'
import useRouter from 'use-react-router'
import { delLogo, fetchBoxObj, loadlogoUrl, postBackgroundImg, PostLogoImg, saveCard } from '../redux/createdCards.ts/action'
import Jimp from 'jimp/es';
import { ICardData } from '../redux/createdCards.ts/reducer'
import { push } from 'connected-react-router'
import { Components } from './Components'
import { MdWallpaper } from 'react-icons/md'
import { AiOutlineCloudUpload } from 'react-icons/ai'
import { IoTrashOutline } from 'react-icons/io5'
import { HiOutlineSave } from 'react-icons/hi'

export interface ContainerProps {
  hideSourceOnDrag: boolean
}

export const Container: FC = () => {

  const dispatch = useDispatch();

  const token = useSelector((state: RootState) => state.auth.token)
  const nameCardId = useSelector((state: RootState) => state.data.data.id);
  const cardObjStore = useSelector((state: RootState) => state.cardObj.cardObj.content)
  const bgImgUrlStore = useSelector((state: RootState) => state.data.data.bgImgUrl)
  const elementRef = useRef<HTMLDivElement | null>(null);
  const router = useRouter<{ id?: string }>();

  const [draggingText, setDraggingText] = useState("");
  const [color, setColor] = useState('#000000');
  const [logoSize, setLogoSize] = useState('20');
  let queryId = router.match.params.id

  // because CSS set card width = 70vw, card height = 40vw
  const yFactor = window.innerWidth * 0.4
  const xFactor = window.innerWidth * 0.7

  let afterCard: ICardData = {
    firstName: {
      top: cardObjStore.firstName.top * yFactor,
      left: cardObjStore.firstName.left * xFactor,
      title: cardObjStore.firstName.title
    },
    lastName: {
      top: cardObjStore.lastName.top * yFactor,
      left: cardObjStore.lastName.left * xFactor,
      title: cardObjStore.lastName.title
    },
    companyName: {
      top: cardObjStore.companyName.top * yFactor,
      left: cardObjStore.companyName.left * xFactor,
      title: cardObjStore.companyName.title
    },
    address: {
      top: cardObjStore.address.top * yFactor,
      left: cardObjStore.address.left * xFactor,
      title: cardObjStore.address.title
    },
    position: {
      top: cardObjStore.position.top * yFactor,
      left: cardObjStore.position.left * xFactor,
      title: cardObjStore.position.title
    },
  }

  if (cardObjStore.email) {
    afterCard.email = {
      top: cardObjStore.email.top * yFactor,
      left: cardObjStore.email.left * xFactor,
      title: cardObjStore.email.title
    }
  }
  if (cardObjStore.mobile) {
    afterCard.mobile = {
      top: cardObjStore.mobile.top * yFactor,
      left: cardObjStore.mobile.left * xFactor,
      title: cardObjStore.mobile.title
    }
  }
  if (cardObjStore.workContact) {
    afterCard.workContact = {
      top: cardObjStore.workContact.top * yFactor,
      left: cardObjStore.workContact.left * xFactor,
      title: cardObjStore.workContact.title
    }
  }
  if (cardObjStore.logo) {
    afterCard.logo = {
      top: cardObjStore.logo.top * yFactor,
      left: cardObjStore.logo.left * xFactor,
      title: cardObjStore.logo.title
    }
  }

  const [boxes, setBoxes] = useState<ICardData>({
    firstName: { top: 0, left: 0, title: "" },
    companyName: { top: 0, left: 0, title: "" },
    lastName: { top: 0, left: 0, title: "" },
    email: { top: 0, left: 0, title: "" },
    address: { top: 0, left: 0, title: "" },
    position: { top: 0, left: 0, title: "" },
    workContact: { top: 0, left: 0, title: "" },
    mobile: { top: 0, left: 0, title: "" },
    logo: { top: 0, left: 0, title: "/favicon.ico" }
  });

  const style: CSSProperties = {
    height: '40vw',
    width: '70vw',
    maxWidth: 1008,
    maxHeight: 575,
    position: 'relative',
    background: '#978b8b',
    borderRadius: "15px",
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    boxShadow: '3px 3px 10px #000000'
  }

  useEffect(() => {
    dispatch(fetchCardData(queryId + ""))
    dispatch(fetchBoxObj(queryId + ""))
  }, [])

  useEffect(() => {
    setBoxes(afterCard as ICardData)
  }, [cardObjStore])

  const moveBox = useCallback(
    (id: string, left: number, top: number) => {
      if (boxes) {
        setBoxes({
          ...boxes,
          [id]: { ...(boxes[id as keyof ICardData]), top, left }
        })
      }
    },
    [boxes, setBoxes],
  )

  const [, drop] = useDrop(
    () => ({
      accept: ItemTypes.BOX,
      drop(item: DragItem, monitor) {
        const delta = monitor.getDifferenceFromInitialOffset() as XYCoord
        const left = Math.round(item.left + delta.x)
        const top = Math.round(item.top + delta.y)
        moveBox(item.id, left, top)
        return undefined
      },
    }),
    [moveBox],
  )

  const handleChangeBG = async (e: React.ChangeEvent<HTMLInputElement>) => {
    if (!e.target.files) {
      alert("Please select file");
      return;
    }
    if (!e.target.files[0].type.match(/image.*/)) {
      alert("Please upload a valid image file.");
      return;
    }
    const backgroundObjectUrl = URL.createObjectURL(e.target.files[0]);
    dispatch(loadbgUrl(backgroundObjectUrl));
    dispatch(postBackgroundImg(e.target.files[0], parseInt(nameCardId)))
    e.target.files = null
  }


  const handleUploadLogo = async (e: React.ChangeEvent<HTMLInputElement>) => {
    if (!e.target.files) {
      alert("Please select file");
      return;
    }
    if (!e.target.files[0].type.match(/image.*/)) {
      alert("Please upload a valid image file.");
      return;
    }
    const reader = new FileReader();
    reader.onloadend = async () => {
      let image = await Jimp.read(reader.result + "")
      image.scaleToFit(140, 140)
      let newURL = await image.getBase64Async(Jimp.MIME_PNG)
      dispatch(PostLogoImg(newURL, parseInt(nameCardId)))
    };
    reader.readAsDataURL(e.target.files[0])
  }

  const handleSaveCard = async () => {
    dispatch(saveCard(parseInt(nameCardId), boxes))
    const divElement = elementRef.current;
    html2canvas(divElement as HTMLDivElement, { allowTaint: true }).then(async function (canvas) {
      let base64URL = canvas.toDataURL('image/png');
      let res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/DwgToPNG/?nameCardId=${nameCardId ? nameCardId : ""}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: 'Bearer ' + token
        },
        body: JSON.stringify({
          base64URL: base64URL
        })
      })
      if (res.status == 200) {
        dispatch(push('/profile'))
      } else {
        alert('Save Card Fail')
      }
    })
  }

  const handleDeleteLogo = async () => {
    dispatch(delLogo(nameCardId))
    setBoxes(afterCard as ICardData)
  }

  return (
    <div>
      <div className={styles["container"]}>
        <div ref={elementRef}>
          <div ref={drop} id="cardDiv" style={style}>
            <img className={styles["cardBgImg"]} src={bgImgUrlStore ? bgImgUrlStore : "/card_sample_a2.jpeg"} crossOrigin="anonymous" />
            <Components title={draggingText} col={0} />
            {(boxes) ? Object.keys(boxes).map((key) => {
              const { left, top, title } = boxes[(key as keyof ICardData)] as any
              if (key != 'logo') {
                return (
                  <>
                    <Box
                      key={key}
                      id={key}
                      left={left}
                      top={top}
                      onDrag={() => setDraggingText(title)}
                    >
                      <div style={{ color: color }}>
                        {title}
                      </div>
                    </Box>
                  </>
                )
              } else {
                return (
                  <>
                    <Box
                      key={key}
                      id={key}
                      left={left}
                      top={top}
                      onDrag={() => setDraggingText(`(image)`)}
                    >
                      <img src={title} style={{ width: `${logoSize}vw` }} />
                    </Box>
                  </>
                )
              }
            }) : null}
          </div>
        </div>
        <div className={styles["btn-container"]}>
          <div className={styles["btn-group"]}>
            <div className={styles["createBtn"]}>
              <input type="color" value={color} style={{ width: '75%' }} onChange={e => setColor(e.target.value)} />
            </div>
            <div className={styles["btn-text"]}>
              Text Color
            </div>
          </div>
          <div className={styles["btn-group"]}>
            <div className={styles["createBtn"]} onChange={handleUploadLogo}>
              <label htmlFor="uploadlogo-button" className={styles["createIcon"]}><AiOutlineCloudUpload /></label>
              <input type="file" accept="image/*" id="uploadlogo-button" style={{ display: 'none' }} />
            </div>
            <div className={styles["btn-text"]}>Upload Logo</div>
          </div>
          <div className={styles["btn-group"]}>
            <div className={styles["createBtn"]} onChange={handleChangeBG}>
              <label htmlFor="upload-button" className={styles["createIcon"]}><MdWallpaper /></label>
              <input type="file" accept="image/*" id="upload-button" style={{ display: 'none' }} />
            </div>
            <div className={styles["btn-text"]}>Change Background</div>
          </div>
          <div className={styles["btn-group"]}>
            <div className={styles["createBtn"]}>
              <input type="number" value={logoSize} style={{ width: '75%' }} onChange={e => setLogoSize(e.target.value)} />
            </div>
            <div className={styles["btn-text"]}>
              Logo Size
            </div>
          </div>
          <div className={styles["btn-group"]}>
            <div className={styles["createBtn"]} onClick={handleSaveCard}>
              <HiOutlineSave />
            </div>
            <div className={styles["btn-text"]}>Save Card</div>
          </div>
          <div className={styles["btn-group"]}>
            <div className={styles["createBtn"]} onClick={handleDeleteLogo}>
              <IoTrashOutline />
            </div>
            <div className={styles["btn-text"]}>Delete Logo</div>
          </div>
        </div>
      </div>
    </div>
  )
}

