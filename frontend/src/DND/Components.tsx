
import React from 'react'
import { Context, Preview, PreviewState } from 'react-dnd-preview'

import {DragContent} from './common'
import { WithPropFunction, GenPreviewLiteProps } from './common'
export const Components = ({title, col}: GenPreviewLiteProps): JSX.Element => {
  return (
    <>
      <Preview generator={WithPropFunction({title, col})} />
    </>
  )
}