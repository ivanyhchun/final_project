import React, { useContext, CSSProperties, forwardRef, ReactNode } from 'react'
import { Context, PreviewState, usePreviewStateContent } from 'react-dnd-preview'


export type DragContent = {
  type: string,
  color: string,
  left: string
}

export type ShapeProps = {
  style: CSSProperties,
  size: number,
  color: string,
  children?: ReactNode
}

// eslint-disable-next-line react/prop-types
export const Shape = forwardRef<HTMLDivElement, ShapeProps>(({ style, size, color, children }, ref) => {
  return (
    <div ref={ref} style={{
      ...style,
      color: 'black',
      backgroundColor: '#a3a3a325',
      width: `${size}px`,
      height: `${size}px`,
      fontSize:`3vw`
    }}>
      {children}
    </div>
  )
})

Shape.displayName = 'Shape'

export type PreviewProps = usePreviewStateContent<DragContent, HTMLDivElement>

export type GenPreviewProps = {
  row: number,
  col: number,
  title: string,
}

export const generatePreview = ({itemType, item, style}: PreviewProps, {row, col, title}: GenPreviewProps): JSX.Element => {
  
  return (
    <Shape color={item.color} size={0} style={{
      ...style,
      top: `${row *60}px`,
      left: `${col *100}px`,
      whiteSpace: 'nowrap',
    }}>
      {title}
    </Shape>
  )
}

export type WithPreviewState = (props: PreviewProps) => JSX.Element

export type GenPreviewLiteProps = Pick<GenPreviewProps, 'col' | 'title'>

export const WithPropFunction = ({ title }: GenPreviewLiteProps): WithPreviewState => {
  return (props) => {
    return generatePreview(props, { row: 0.5, col:0.5, title })
  }
}

