import React from 'react'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import styles from './DND.module.scss';
import Background from '../Background';
import { IoIosArrowBack } from 'react-icons/io';
import { NavLink } from 'react-router-dom';
import { VscGear } from 'react-icons/vsc';
import { TouchBackend } from 'react-dnd-touch-backend'
import { Container } from './Container';

export default function Dra() {
    const opts = {
        enableTouchEvents: true,
        enableMouseEvents:true,
    }

    return (
        <Background name="bkgd-main">
            <div className={styles["page-header"]}>
                <NavLink to="/profile" className={styles["ioiosArrowBack"]}>
                    <IoIosArrowBack />
                </NavLink>
                <h4>My Profile</h4>
                <div className={styles["vscGear"]}>
                    <VscGear />
                </div>
            </div>
            <div className={styles["drawCard-container"]}>
                <DndProvider backend={TouchBackend} options={opts}>
                    <Container />
                </DndProvider>
            </div>
        </Background>
    )
}