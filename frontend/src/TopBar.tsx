import { push } from 'connected-react-router';
import React, { useEffect } from 'react';
import { GoThreeBars } from 'react-icons/go';
import { IoIosMail } from 'react-icons/io';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import CategoryMenu from './CategoryMenu';
import { fetchMessage } from './redux/message/action';
import { fetchProfile, getProfileImage } from './redux/profile/action';
import { RootState } from './store';
import styles from './TopBar.module.scss';


export interface ITopBar {
    menuState: boolean
    setMenuState: () => void
}

function TopBar(props: ITopBar) {
    const dispatch = useDispatch();
    const messages = useSelector((state: RootState) => state.message.messages);
    let unreadMsg = 0;
    messages.map((message) => {
        if (!message.read) {
            unreadMsg++;
        }
        return unreadMsg;
    })
    const profile = useSelector((state: RootState) => state.profile.profile);
    const imageObjectUrl = useSelector((state: RootState) => state.profile.profile.imageObjectUrl);
    useEffect(() => {
        dispatch(fetchMessage());
        dispatch(fetchProfile());
        dispatch(getProfileImage(profile.profileImg + ""));
    }, [])

    return (
        <>
            <nav className={styles["navbar-container"]}>
                <CategoryMenu menuState={props.menuState} setMenuState={props.setMenuState} />
                <div className={styles["menu-icon"]}>
                    <GoThreeBars className={styles["faBars"]} onClick={props.setMenuState} />
                </div>
                <div className={styles["profile-icon"]}>
                    <div className={styles["profile-img-container"]} onClick={() => {
                        dispatch(push('/profile')); 
                    }}>
                        <div className={styles["profile-img"]}>
                            <img src={imageObjectUrl ? imageObjectUrl : "default-profile.jpeg"}
                                alt="profile image" />
                        </div>
                    </div>
                    <div className={styles["profile-info-container"]}>
                        <div className={styles["profile-name"]}>
                            {profile.firstName} {profile.lastName}
                        </div>
                        <div className={styles["profile-email"]}>
                            {profile.email}
                        </div>
                    </div>
                </div>
                <NavLink to="/message" className={styles["msg-icon"]}>
                    <IoIosMail className={styles["faEnvelope"]} />
                    <div className={unreadMsg !== 0 ? styles["alert-icon"] : styles["alert-icon-hidden"]}>{unreadMsg}</div>
                </NavLink>
            </nav>
            <div className={styles["navbar-block"]}></div>
        </>
    )
}

export default TopBar;