import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styles from './Owncards.module.scss';
import { RootState } from './store';
import { FiPhone } from 'react-icons/fi';
import { HiOutlineMail, HiQrcode } from 'react-icons/hi';
import { push } from 'connected-react-router';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation, Pagination } from 'swiper'; 
import 'swiper/swiper-bundle.css';
import "swiper/components/navigation/navigation.scss";
import "swiper/components/pagination/pagination.scss";
import './style.css'; 
import { generateQRCode, toggleQRCodeDisplay } from './redux/qrcode/action';
import { FaQrcode } from 'react-icons/fa';

SwiperCore.use([Navigation, Pagination]); 

function Owncards() {
    const dispatch = useDispatch();
    const owncards = useSelector((state: RootState) => state.owncards.owncards);
    const slides = [];
    const handleQRcodeClick = (e:any)=>{
        let QRCodeID = e.currentTarget.id;
        let cardToQRCode = owncards.filter(card=>(card.id == QRCodeID))[0]
        dispatch(generateQRCode(cardToQRCode))
        dispatch(toggleQRCodeDisplay(true))
    }

    for (let i = 0; i < owncards.length; i++) {
        slides.push(
            <SwiperSlide className={styles["swiper-container"]}>
                <div className={styles["card-container"]}>
                    <div id = {owncards[i].id} onClick={handleQRcodeClick}><FaQrcode className={styles["qrcode"]} /></div>
                    <div className={styles["card-body"]} onClick={() => {
                        dispatch(push('/profile/' + owncards[i].id))
                    }}>
                        <div className={styles["company-name"]}>
                            {owncards[i].companyName}
                        </div>
                        <div className={styles["job-position"]}>
                            {owncards[i].position}
                        </div>
                        <div className={styles["address"]}>
                            {owncards[i].address}
                        </div>
                        {owncards[i].mobile && <div className={styles["workContact"]}>
                            <FiPhone className={styles["fiPhone"]} />
                            {owncards[i].mobile}
                        </div>}
                        {owncards[i].email && <div className={styles["email"]}>
                            <HiOutlineMail className={styles["hiOutlineMail"]} />
                            {owncards[i].email}
                        </div>}
                    </div>
                </div>
            </SwiperSlide>
        )
    }
    return (
        <>                 
            <Swiper 
                navigation
                pagination={{ clickable: true }}
                loop={true}
                spaceBetween={0}
                slidesPerView={1}
            >   
                {slides}
            </Swiper>
        </>
    )
}

export default Owncards;