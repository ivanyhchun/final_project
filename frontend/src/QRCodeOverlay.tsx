import React from 'react';
import styles from './QRCodeOverlay.module.scss';
import { RootState } from './store';
import { useDispatch, useSelector } from 'react-redux';
import { IoClose } from 'react-icons/io5';
import { toggleQRCodeDisplay } from './redux/qrcode/action';
import QRCode from "react-qr-code";
import vCardFactory from 'vcards-js';


function QRCodeOverlay() {
    const dispatch = useDispatch();
    const QRCodeOpen =  useSelector((state: RootState) => state.QRCode.QRCodeIsOpen)
    const QRCodeInfo = useSelector((state: RootState) => state.QRCode.QRCode)
    
    let vCard = vCardFactory()
    vCard.firstName = QRCodeInfo.firstName? QRCodeInfo.firstName : '';
    vCard.lastName = QRCodeInfo.lastName? QRCodeInfo.lastName : '';
    vCard.organization = QRCodeInfo.companyName? QRCodeInfo.companyName : '';
    vCard.workAddress.street = QRCodeInfo.address? QRCodeInfo.address : '';
    vCard.title = QRCodeInfo.position? QRCodeInfo.position : ''
    vCard.workPhone = QRCodeInfo.workContact? QRCodeInfo.workContact : '';
    vCard.cellPhone = QRCodeInfo.mobile? QRCodeInfo.mobile : '';
    vCard.email = QRCodeInfo.email? QRCodeInfo.email : '';
    vCard.note =  QRCodeInfo.externalId ? QRCodeInfo.externalId : ''
    const vCardString = vCard.getFormattedString()
    const onClick = ()=>{
        dispatch(toggleQRCodeDisplay(false))
    }
    return (
        <>
        {QRCodeOpen && 
            <div className={styles["qrcode-overlay"]}>
                <div className={styles["icon"]}><IoClose className={styles["IoClose"]} onClick={onClick}/></div>
                <QRCode value= {vCardString}/>
            </div>}

        </>
    )

}

export default QRCodeOverlay
