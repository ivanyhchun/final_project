import React, { useState } from 'react';
import { IoIosArrowBack, IoMdDesktop } from 'react-icons/io';
import { VscGear } from 'react-icons/vsc';
import { NavLink } from 'react-router-dom';
import Background from './Background';
import LoginHeader from './LoginHeader';
import styles from './CreateManualNamecard.module.scss';
import { Controller, useForm } from 'react-hook-form';
import { AiOutlineMail } from 'react-icons/ai';
import { CgProfile } from 'react-icons/cg';
import { FiPhone } from 'react-icons/fi';
import { HiOutlineBriefcase } from 'react-icons/hi';
import { IoLocationOutline } from 'react-icons/io5';
import { MdSmartphone } from 'react-icons/md';
import { RiToolsFill } from 'react-icons/ri';
import { useDispatch, useSelector } from 'react-redux';
import { createNewCard } from './redux/namecards/action';
import CreatableSelect from 'react-select/creatable';
import { RootState } from './store';

function CreateManualNamecard() {
    const existingCategoryArray = useSelector((state: RootState) => state.categoryList.categoryList)
    const dispatch = useDispatch();
    const { register, handleSubmit,control,formState: { errors } } = useForm();

    let selectionArray:  Object[] = [];
    selectionArray = existingCategoryArray.map(data=>(
        {value:data.categoryId,
        label:data.categoryName}
    ))

    const onSubmit =  (data: any) => {
        
        if (!firstName || !lastName){
            alert('First name and Last name is mandatory')
        } else {
            dispatch(createNewCard(
                data.firstName,
                data.lastName,
                data.position,
                data.companyName,
                data.address,
                data.workContact,
                data.mobile,
                data.email,
                "manual",
                data.categoryIDorName.value
            )); 
        }
    };

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [address, setAddress] = useState('');
    const [position, setPosition] = useState('');
    const [workContact, setWorkContact] = useState('');
    const [mobile, setMobile] = useState('');
    const [email, setEmail] = useState('');

    return (
        <Background name="bkgd-namecard">
            <div className={styles["page-header"]}>
                <NavLink to="/homepage" className={styles["ioiosArrowBack"]}>
                    <IoIosArrowBack />
                </NavLink>
                <h4>Create New Card</h4>
                <div className={styles["vscGear"]}>
                    <VscGear />
                </div>
            </div>
            <LoginHeader>
                Input Card Details
            </LoginHeader>
            <div className={styles["card-main-container"]}>
                <form onSubmit={handleSubmit(onSubmit)} className={styles["create-form"]} autoComplete="off">
                    <div className={styles["form-container"]}>
                        <CgProfile className={styles["form-icon"]} />
                        <input {...register("firstName")} placeholder="First Name" value={firstName} className={styles["form-group"]} onChange={(event) => {
                            setFirstName(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <CgProfile className={styles["form-icon"]} />
                        <input {...register("lastName")} placeholder="Last Name" value={lastName} className={styles["form-group"]} onChange={(event) => {
                            setLastName(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <HiOutlineBriefcase className={styles["form-icon"]} />
                        <input {...register("companyName")} placeholder="Company Name" value={companyName} className={styles["form-group"]} onChange={(event) => {
                            setCompanyName(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <IoLocationOutline className={styles["form-icon"]} />
                        <input {...register("address")} placeholder="Company Address" value={address} className={styles["form-group"]} onChange={(event) => {
                            setAddress(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <IoMdDesktop className={styles["form-icon"]} />
                        <input {...register("position")} placeholder="Job Position" value={position} className={styles["form-group"]} onChange={(event) => {
                            setPosition(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <FiPhone className={styles["form-icon"]} />
                        <input {...register("workContact")} placeholder="Work Phone" value={workContact} className={styles["form-group"]} onChange={(event) => {
                            setWorkContact(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <MdSmartphone className={styles["form-icon"]} />
                        <input {...register("mobile")} placeholder="Mobile Phone" value={mobile} className={styles["form-group"]} onChange={(event) => {
                            setMobile(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <AiOutlineMail className={styles["form-icon"]} />
                        <input {...register("email")} placeholder="Work Email" value={email} className={styles["form-group"]} onChange={(event) => {
                            setEmail(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <RiToolsFill className={styles["form-icon"]} />
                        <Controller
                            name="categoryIDorName"
                            render={({ field }) => (
                                <CreatableSelect
                                    {...field}
                                    className={styles["selector-bar"]}
                                    options={selectionArray}
                                    isClearable={false}
                                    placeholder={"Type or select category"}
                                    defaultValue={""}
                                />
                            )}
                            control={control}
                            defaultValue={""}
                        />
                    </div>
                    <input type="submit" className={styles["submit-btn"]} value="CREATE NEW CARD" />
                </form>
            </div>
        </Background>
    )
}

export default CreateManualNamecard;

