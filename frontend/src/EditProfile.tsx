import React, { useEffect, useState } from 'react';
import styles from './EditProfile.module.scss';
import Background from './Background';
import { NavLink } from 'react-router-dom';
import EditProfileForm from './EditProfileForm';
import { FaCamera } from 'react-icons/fa'; 
import { useDispatch, useSelector } from 'react-redux';
import { fetchProfile, loadProfileImage, PostProfileImage } from './redux/profile/action';
import { RootState } from './store';
import { IoIosArrowBack } from 'react-icons/io';
import { IoClose } from 'react-icons/io5'; 
import Jimp from 'jimp/es';


interface IEditProfile {
    setMenuState: () => void
}

function EditProfile(props: IEditProfile) {
    const imageObjectUrl = useSelector((state:RootState)=> state.profile.profile.imageObjectUrl)
    const dispatch = useDispatch(); 
    const fileUploadHandler = (e : React.ChangeEvent<HTMLInputElement>) => {
        if (!e.target.files) {
            alert("Please select file");
            return;
        }
        if (!e.target.files[0].type.match(/image.*/)) {
            alert("Please upload a valid image file.");
            return;
        } 
        const imageObjectUrl = URL.createObjectURL(e.target.files[0]);
        dispatch(loadProfileImage(imageObjectUrl));
        const reader = new FileReader();
        reader.onloadend = async () => {
            let image = await Jimp.read(reader.result + '')
            image.scaleToFit(140,140)
            let newURL = await image.getBase64Async(Jimp.MIME_PNG)
            const imageBlob = await (await fetch(newURL)).blob()
            const formattedImage = new File([imageBlob], 'image.png', { type: "image/png" })
            dispatch(PostProfileImage(formattedImage))
            e.target.files = null
          };
        reader.readAsDataURL(e.target.files[0])
    }
    useEffect(() => {
        dispatch(fetchProfile());
    },[])
    

    return (
        <Background name="bkgd-profile">
            <div className={styles["page-header"]}>
                <NavLink to="/profile" className={styles["ioiosArrowBack"]}>
                    <IoIosArrowBack />
                </NavLink>
                <h4>Edit Profile</h4>
                <NavLink onClick={props.setMenuState} to="/profile" className={styles["ioClose"]}>
                    <IoClose />
                </NavLink>
            </div>
            
            <div className={styles["profile-container"]}>
                <div className={styles["profile-img-container"]}>
                    <img src={imageObjectUrl+""} alt="profile image" />
                    <input type="file" accept="image/*" name="avatar" onChange={fileUploadHandler} id = "upload-button" style={{display:'none'}}/>
                    <label htmlFor="upload-button">
                        <div className={styles["camera-icon"]}>
                            <FaCamera  className={styles["faCamera"]}/> 
                        </div>
                    </label>
                </div>
            </div>
            <div className={styles["edit-profile-body"]}>
                <EditProfileForm name="CardForm" setMenuState={props.setMenuState} />
            </div>
        </Background>
    )
}

export default EditProfile;

