import { push } from 'connected-react-router';
import React from 'react';
import { AiFillStar } from 'react-icons/ai';
import { useDispatch, useSelector } from 'react-redux';
import styles from './CardPreview.module.scss';
import { RootState } from './store';

interface ICardPreview {
    name: string
}

function CardPreview(props: ICardPreview) {
    const dispatch = useDispatch()
    let namecards = useSelector((state: RootState) => state.namecardsList.namecardsList);


    if (props.name === 'recent') {
        namecards.sort((a, b) => a.updatedAt > b.updatedAt ? -1 : 1);
    } else if (props.name === 'favorite') {
        namecards = namecards.filter((namecard) => {
            return namecard.favorite === true;
        })
    } else if (props.name === 'contact') {
        namecards.sort((a, b) => a.firstName < b.firstName ? -1 : 1);
    } else if (props.name === 'company') {
        namecards.sort((a, b) => a.companyName < b.companyName ? -1 : 1);
    }

    let textSearchQuery = (useSelector((state: RootState) => state.search.textSearch) + "").toLowerCase()
    let categorySearchQuery = useSelector((state: RootState) => state.search.categoryId)

    let filteredNamecards, textfilteredNamecards = []
    if (!textSearchQuery) {
        textfilteredNamecards = namecards;
    } else {
        textfilteredNamecards = namecards.filter((namecard => {
            return (namecard.firstName.toLowerCase().includes(textSearchQuery) ||
                namecard.lastName.toLowerCase().includes(textSearchQuery) ||
                namecard.companyName.toLowerCase().includes(textSearchQuery) ||
                namecard.position.toLowerCase().includes(textSearchQuery))
        }))
    }
    if (!categorySearchQuery) {
        filteredNamecards = textfilteredNamecards
    } else {

        filteredNamecards = textfilteredNamecards.filter((namecard => {

            return (namecard.categoryId === categorySearchQuery)
        }))
    }


    return (
        <>
            {
                filteredNamecards.map((namecard) => {
                    return (
                        <div className={styles["main-item"]} onClick={() => {
                            dispatch(push('/homepage/' + namecard.externalId))
                        }}>
                            <div className={styles["profile-img-container"]}>
                                <div className={styles["profile-img-body"]}>
                                    <img src={namecard.profileImgURL + ""} alt="Profile" className={styles["profile-img"]} />
                                </div>
                            </div>
                            <div className={styles["card-body"]}>
                                <div className={styles["card-info"]}>
                                    <div className={styles["display-name"]}>
                                        {namecard.firstName} {namecard.lastName}
                                    </div>
                                    <div className={styles["job-position"]}>
                                        {namecard.position}
                                    </div>
                                    <div className={styles["company-name"]}>
                                        {namecard.companyName}
                                    </div>
                                </div>
                                <div className={styles["fav-container"]}>
                                    <AiFillStar className={namecard.favorite ? styles["faHeart"] : styles["faHeart-hidden"]} />
                                </div>
                            </div>
                        </div>
                    )
                })
            }
        </>
    )
}

export default CardPreview;
