import React, { useState, useEffect } from 'react';
import styles from './ProfilePage.module.scss';
import Background from './Background';
import Owncards from './Owncards';
import { useDispatch, useSelector } from 'react-redux';
import { fetchOwncards } from './redux/owncards/action';
import { IoIosArrowBack } from 'react-icons/io';
import { VscGear } from 'react-icons/vsc';
import { NavLink } from 'react-router-dom';
import Settings from './Settings';
import { fetchProfile, getProfileImage } from './redux/profile/action';
import { RootState } from './store';
import { FaPlus } from 'react-icons/fa';


interface IProfilePage {
    menuState: boolean
    setMenuState: () => void
}

function ProfilePage(props: IProfilePage) {

    const dispatch = useDispatch();
    const [openMenu, setOpenMenu] = useState(true);
    const data = useSelector((state: RootState) => state.profile.profile);
    const imageObjectUrl = useSelector((state: RootState) => state.profile.profile.imageObjectUrl); 
    const owncards = useSelector((state: RootState) => state.owncards.owncards); 

    useEffect(() => {
        dispatch(fetchOwncards())
        dispatch(fetchProfile())
        dispatch(getProfileImage(data.profileImg + ""))
    }, [])


    return (
        <Background name="bkgd-profile">
            <div className={styles["page-header"]}>
                <NavLink to="/homepage" className={styles["ioiosArrowBack"]}>
                    <IoIosArrowBack />
                </NavLink>
                <h4>My Profile</h4>
                <div className={styles["vscGear"]} onClick={props.setMenuState}>
                    <VscGear />
                </div>
            </div>
            <div className={styles["profile-container"]}>
                <div className={styles["profile-img-container"]}>
                    <img src={imageObjectUrl ? imageObjectUrl : "default-profile.jpeg"}
                        alt="profile image" />
                </div>
                <div className={styles["profile-name"]}>
                    {data.firstName} {data.lastName}
                </div>
            </div>
            <div className={styles["info-container"]}>
                <div className={styles["info-title"]}>
                    <h4>Browse My Cards</h4>
                </div>
                <div className={styles["cards-container"]}>
                    {owncards.length > 0 ? <Owncards /> : <div className={styles["nocard-text"]}>You have no business card at the moment.</div>}     
                </div>
                <div className={styles["create-btn-container"]}>
                    <NavLink to='/create-owncard'>
                        <button className={styles["create-btn"]}>
                            <FaPlus className={styles["create-icon"]} />
                            <div className={styles["btn-text"]}>
                                CREATE NEW CARD
                            </div>
                        </button>
                    </NavLink>
                </div>
            </div>
            <Settings menuState={props.menuState} setMenuState={props.setMenuState} />
        </Background>
    )
}

export default ProfilePage;