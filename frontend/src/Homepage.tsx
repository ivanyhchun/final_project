import React, { useEffect, useState } from 'react';
import Background from './Background';
import styles from './Homepage.module.scss';
import NavBar from './NavBar';
import { useDispatch, useSelector } from 'react-redux';
import { fetchNamecards } from './redux/namecards/action';
import CardPreview from './CardPreview';
import { fetchCategoryList } from './redux/category/action';
import { AiFillStar, AiOutlineClockCircle } from 'react-icons/ai';
import { HiOutlineBriefcase } from 'react-icons/hi';
import { BsFillPersonFill } from 'react-icons/bs';
import Buttons from './Buttons';
import { RootState } from './store';
import TopBar from './TopBar';
import SearchBar from './SearchBar';

interface IHomepage {
    menuState: boolean
    setMenuState: () => void
}

function Homepage(props: IHomepage) {

    const dispatch = useDispatch()
    const namecards = useSelector((state: RootState) => state.namecardsList.namecardsList);
    const [btnClicked, setBtnClicked] = useState<string>("recent");
    const handleClick = (event: any) => {
        setBtnClicked(event.currentTarget.id);
    }

    useEffect(() => {
        dispatch(fetchNamecards())
        dispatch(fetchCategoryList())
    }, [])

    return (
        <Background name="bkgd-homepage">
            <TopBar menuState={props.menuState} setMenuState={props.setMenuState} />
            <div className={styles["sort-container"]}>
                <div className={btnClicked === "favorite" ? styles["sort-group-clicked"] : styles["sort-group"]} onClick={handleClick} id="favorite">
                    <AiFillStar className={styles["sort-icon"]} />
                    <div className={styles["sort-text"]}>
                        Favorites
                    </div>
                </div>
                <div className={btnClicked === "recent" ? styles["sort-group-clicked"] : styles["sort-group"]} onClick={handleClick} id="recent">
                    <AiOutlineClockCircle className={styles["sort-icon"]} />
                    <div className={styles["sort-text"]}>
                        Recent
                    </div>
                </div>
                <div className={btnClicked === "contact" ? styles["sort-group-clicked"] : styles["sort-group"]} onClick={handleClick} id="contact">
                    <BsFillPersonFill className={styles["sort-icon"]} />
                    <div className={styles["sort-text"]}>
                        Contact Name
                    </div>
                </div>
                <div className={btnClicked === "company" ? styles["sort-group-clicked"] : styles["sort-group"]} onClick={handleClick} id="company">
                    <HiOutlineBriefcase className={styles["sort-icon"]} />
                    <div className={styles["sort-text"]}>
                        Company Name
                    </div>
                </div>
            </div>
            <div className={styles["sort-block"]}></div>
            <SearchBar />
            <div className={styles["main-container"]}>
                {namecards.length > 0 ?
                    <CardPreview name={btnClicked} /> :
                    <div className={styles["nocard-text"]}>You have no contact at the moment.</div>
                }
            </div>
            <div className={styles["buttons"]} >
                <Buttons />
            </div>
        </Background>
    )
}

export default Homepage;

