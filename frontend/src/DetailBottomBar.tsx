import React from 'react'
import styles from './DetailBottomBar.module.scss';
import { MdModeEdit } from 'react-icons/md'
import { MdPhone } from 'react-icons/md';
import { MdEmail } from 'react-icons/md';
import { MdLocationOn } from 'react-icons/md'


export default function DetailBottomBar() {
    return (
        <div>
            <div className={styles["barSpace"]}>
            <div className={styles["barContainer"]}>
                <div className={`${styles["icon-circle"]} ${styles["color1"]}`}><MdPhone /></div>
                <div className={`${styles["icon-circle"]} ${styles["color2"]}`}><MdEmail /></div>
                <div className={`${styles["icon-circle"]} ${styles["color3"]}`}><MdLocationOn /></div>
                <div className={`${styles["icon-circle"]} ${styles["color4"]}`}><MdModeEdit /></div>
            </div>
            </div>
          
        </div>
    )
}