import React from 'react'; 
import { useSelector } from 'react-redux';
import styles from './Background.module.scss'; 
import QRCodeOverlay from './QRCodeOverlay';
import { RootState } from './store';

interface IBackground {
    name: string,
    children: React.ReactNode
}

function Background(props: IBackground): React.ReactElement {
    const getStyle = (name: string) => {
        switch(name) {
            case "bkgd-main": return styles["bkgd-container"]; 
            case "bkgd-homepage": return styles["bkgd-homepage"];
            case "bkgd-profile": return styles["bkgd-profile"]; 
            case "bkgd-namecard": return styles["bkgd-namecard"]; 
        }
    }
    
    return (
        <div className={getStyle(props.name)}>
            <QRCodeOverlay/>
            {props.children}
        </div>
    )
}

export default Background; 
