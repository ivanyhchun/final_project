import { push } from 'connected-react-router';
import React from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import styles from './CardForm.module.scss';
import { createOwncard, loadInputData } from './redux/data/action';
import { RootState } from './store';

export interface ICardForm {
    name: string
}

function CardForm(props: ICardForm) {
    const dispatch = useDispatch(); 
    const { register, handleSubmit, formState: { errors }} = useForm(); 
    const cardData = useSelector((state: RootState) => state.data.data);
    const onSubmit = handleSubmit(data => {
        dispatch(createOwncard(data))
    }); 

    return (
        <form onSubmit={onSubmit} className={styles["create-form"]} autoComplete="off">
            <input {...register("firstName", { required: true, maxLength: 20 })} placeholder=" First Name" className={errors.firstName ? styles["form-group-error"] : styles["form-group"]} />
            {errors.firstName && <span className={styles["error-text"]}>*This field is required.</span>}

            <input {...register("lastName", { required: true, maxLength: 20 })} placeholder=" Last Name" className={errors.lastName ? styles["form-group-error"] : styles["form-group"]} />
            {errors.lastName && <span className={styles["error-text"]}>*This field is required.</span>}

            <input {...register("companyName", { required: true })} placeholder=" Company Name" className={errors.companyName ? styles["form-group-error"] : styles["form-group"]} />
            {errors.companyName && <span className={styles["error-text"]}>*This field is required.</span>}

            <input {...register("address", { required: true })} placeholder=" Company Address" className={errors.address ? styles["form-group-error"] : styles["form-group"]} />
            {errors.address && <span className={styles["error-text"]}>*This field is required.</span>}

            <input {...register("position", { required: true })} placeholder=" Job Position" className={errors.position ? styles["form-group-error"] : styles["form-group"]} />
            {errors.position && <span className={styles["error-text"]}>*This field is required.</span>}

            <input {...register("workContact")} placeholder=" Work Phone" className={styles["form-group"]} />
            <input {...register("mobile")} placeholder=" Mobile Phone" className={styles["form-group"]} />
            <input {...register("email")} placeholder=" Work Email" className={styles["form-group"]} />

            <input type="submit" className={styles["submit-btn"]} value={props.name === "CardForm" ? "CONFIRM" : "DRAW YOUR CARD"} />

        </form>
    )
}

export default CardForm;