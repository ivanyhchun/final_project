import React, { useState, useRef } from 'react'
import QrReader from 'react-qr-reader'
import styles from './UploadQr.module.scss'
import { FiUpload } from "react-icons/fi";
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';
import { processQRCode } from '../redux/memberRelationship/action';


export default function UploadQr() {
    const [scanResultFile, setScanReasultFile] = useState('')
    const qrRef: React.MutableRefObject<any> = useRef(null)
    const token = useSelector((state: RootState) => state.auth.token)
    const dispatch = useDispatch()
    const handleErrorFile = (error: string) => {
        console.log(handleErrorFile)
    }
    const handleScanFile = (result: string | null) => {
        if (result) {
            setScanReasultFile(result);
            dispatch(processQRCode(result))
        }
    }
    const onScanFile = () => {
        qrRef.current.openImageDialog();
    }
    return (
        <div className={styles.hello}>
            <div className={styles["upload-container"]}>
            <div className={styles.plusBtn} onClick={onScanFile}><FiUpload /></div>
            </div>
            <div className={styles["qrContainer"]}>
                <QrReader
                    ref={qrRef}
                    delay={300}
                    style={{ width: '50%' }}
                    onError={handleErrorFile}
                    onScan={handleScanFile}
                    legacyMode={true}
                />
            </div>
        </div>
    )
}