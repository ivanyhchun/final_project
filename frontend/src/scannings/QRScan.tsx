import React, { useState } from 'react'
import { IoIosArrowBack } from 'react-icons/io';
import QrReader from 'react-qr-reader'
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { processQRCode } from '../redux/memberRelationship/action';
import styles from './QRScan.module.scss'
import UploadQr from './UploadQr';

export default function QRScan() {
    const dispatch = useDispatch()
    const [scanResultCam, setScanResultCam] = useState('');
    const handleErrorScanCam = (error: string) => {
        console.log(error);
    }
    
    const handleScanCam = (result: string | null) => {
        if (result) {
            setScanResultCam(result);
            dispatch(processQRCode(result))
        }
    }

    return (
            <div className={styles["qrContainer"]}>
                <NavLink to="/homepage" className={styles["ioiosArrowBack"]}>
                    <IoIosArrowBack />
                </NavLink>
                <div className={styles["camera"]}>
                    <QrReader
                        delay={300}
                        style={{ width: '100%'}}
                        onError={handleErrorScanCam}
                        onScan={handleScanCam}
                        resolution={700}
                    />
                </div>
                <UploadQr />
            </div>
    )
}