import { push } from 'connected-react-router';
import React, { useEffect, useState } from 'react';
import Camera, { FACING_MODES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import { IoIosArrowBack } from 'react-icons/io';
import { IoCheckmarkSharp, IoReturnDownBack } from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import './CameraScan.css'
import styles from './CameraScan.module.scss'
import { RootState } from '../store';
import { FiUpload } from 'react-icons/fi';
import Jimp from 'jimp/es';
import spinner from './Spinner.svg'
import { loadOneNamecard, loadOneNamecardImage } from '../redux/namecards/action';


function CameraScan() {
    const [isLoading,setIsLoading] = useState(false)
    const [dataURL, setDataURL] = useState('')
    const token = useSelector((state: RootState) => state.auth.token)
    const dispatch = useDispatch()

    async function imageProcess(dataURL:any){
        let image = await Jimp.read(dataURL);
        if (image.bitmap.width < image.bitmap.height) {
            image = image.rotate(90)
        }
        image.scaleToFit(1008,612) // 3x size of the namecard image container CSS
        let newURL = await image.getBase64Async(Jimp.MIME_PNG)
        setDataURL(newURL)
    }

    async function handleTakePhoto(dataURL: any) {
        imageProcess(dataURL)
    }

    const handleUploadPhoto = async (e : React.ChangeEvent<HTMLInputElement>)=>{
        setIsLoading(true)
        if (!e.target.files) {
            alert("Please select file");
            return;
        }
        if (!e.target.files[0].type.match(/image.*/)) {
            alert("Please upload a valid image file.");
            return;
        }
        const reader = new FileReader();
        reader.onloadend = async () => {
            imageProcess(reader.result + '')
            setIsLoading(false)
          };
        reader.readAsDataURL(e.target.files[0])
    }

    const handleSubmitPhoto = async () => {
        setIsLoading(true)
        const imageBlob = await (await fetch(dataURL)).blob()
        const imageFile = new File([imageBlob], 'image.png', { type: "image/png" })
        const imageObjectURL = URL.createObjectURL(imageFile)

        let formData = new FormData();
        formData.append("image", imageFile)

        let res = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/namecard/camera/image`, {
            method: 'POST',
            headers: {
                Authorization: 'Bearer ' + token
            },
            body: formData
        });

        if (res.status === 200) {
            const result = await res.json();
            setIsLoading(false)
            dispatch(loadOneNamecard(result))
            dispatch(loadOneNamecardImage(imageObjectURL))
            dispatch(push('/scan/review'))
        } else {
            setIsLoading(false)
            const error = await res.json();
            console.log(error)
            alert("Update failed. Please try again")
        }
    }

    return (
        <div>
            <NavLink to="/homepage" className={styles["ioiosArrowBack"]}>
                <IoIosArrowBack />
            </NavLink>
            {(dataURL) ?
                <div>
                    <div className={styles["preview-image"]}>
                        <img src={dataURL} />
                    </div>
                    <div className={styles["buttons-container"]}>
                        {isLoading && 
                            <div className={styles["spinner"]}><img src = {spinner} /></div>}
                        <div onClick={() => {setDataURL('')}}>
                            <IoReturnDownBack />
                        </div>
                        <div onClick={handleSubmitPhoto}>
                            <IoCheckmarkSharp />
                        </div>
                    </div>
                </div>
                :
                <div >
                    <Camera
                        onTakePhoto={(dataURL) => { handleTakePhoto(dataURL); }}
                        isImageMirror={false}
                        idealFacingMode = {FACING_MODES.ENVIRONMENT}
                        isMaxResolution = {true}
                        isFullscreen = {true}
                        imageCompression = {0.97}
                    />
                    {isLoading && 
                        <div className={styles["spinner"]}><img src = {spinner} /></div>}
                    <div className={styles["message"]}>This side upwards</div>
                    <div className={styles["frame-overlay"]}></div>
                    <input type="file" accept="image/*" name="photo" onChange={handleUploadPhoto} id = "upload-button" style={{display:'none'}}/>
                    <label htmlFor='upload-button' style={{position:"absolute",bottom:"0",left:"0"}}>
                        <div className={styles["upload-button-container"]}>
                            <div><FiUpload /></div>
                        </div>
                    </label>
                </div>
            }
        </div>
    );
}

export default CameraScan;

