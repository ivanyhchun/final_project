import { push } from 'connected-react-router';
import React, { useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { AiOutlineMail } from 'react-icons/ai';
import { CgProfile } from 'react-icons/cg';
import { FiPhone } from 'react-icons/fi';
import { HiOutlineBriefcase } from 'react-icons/hi';
import { IoIosArrowBack, IoMdDesktop } from 'react-icons/io';
import { IoLocationOutline, IoTrashOutline } from 'react-icons/io5';
import { MdSmartphone } from 'react-icons/md';
import { useDispatch, useSelector } from 'react-redux';
import Background from '../Background';
import styles from './ReviewScannedCardForm.module.scss';
import { RootState } from '../store';
import { RiToolsFill } from 'react-icons/ri';
import CreatableSelect from 'react-select/creatable';
import { createNewCard } from '../redux/namecards/action';

function ReviewScannedCardForm() {
    const dispatch = useDispatch()
    const namecardInReview = useSelector((state: RootState) => state.oneNamecard.oneNamecard);
    const existingCategoryArray = useSelector((state: RootState) => state.categoryList.categoryList)
    
    const [firstName, setFirstName] = useState(namecardInReview.firstName);
    const [lastName, setLastName] = useState(namecardInReview.lastName);
    const [companyName, setCompanyName] = useState(namecardInReview.companyName);
    const [address, setAddress] = useState(namecardInReview.address);
    const [position, setPosition] = useState(namecardInReview.position);
    const [workContact, setWorkContact] = useState(namecardInReview.workContact);
    const [mobile, setMobile] = useState(namecardInReview.mobile);
    const [email, setEmail] = useState(namecardInReview.email);

    const [data, setData] = useState(null);

    let selectionArray:  Object[] = [];
    selectionArray = existingCategoryArray.map(data=>(
        {value:data.categoryId,
        label:data.categoryName}
    ))

    const { register, handleSubmit,control,formState: { errors }} = useForm();

    const onSubmit =  (data: any) => {
        if (!firstName || !lastName){
            alert('First name and Last name is mandatory')
        }else {
            setData(data);
            dispatch(createNewCard(
                data.firstName,
                data.lastName,
                data.companyName,
                data.address,
                data.position,
                data.workContact,
                data.mobile,
                data.email,
                "camera",
                data.categoryIDorName.value,
                namecardInReview.namecardImg+"",
            ))
        }
    };
   
    return (

        <Background name="bkgd-main">
            <div className={styles["page-header"]}>
                <div className={styles["ioiosArrowBack"]} onClick={() => {
                    dispatch(push('/CameraScan'));
                }}>
                    <IoIosArrowBack />
                </div>
                <h4>Review Scanning</h4>
                <div className={styles["placeholder"]}>
                    <IoTrashOutline />
                </div>
            </div>
            <div className={styles["pageContainer"]}>
                <div className={styles["card-container"]}>
                    <img src={namecardInReview.namecardImgURL+''} alt=""></img>
                </div>
            </div>
            <div className={styles["card-main-container"]}>
                <form onSubmit={handleSubmit(onSubmit)} className={styles["create-form"]} autoComplete="off">
                    <div className={styles["form-container"]}>
                        <CgProfile className={styles["form-icon"]} />
                        <input {...register("firstName")} placeholder="First Name" value={firstName} className={styles["form-group"]} onChange={(event) => {
                            setFirstName(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <CgProfile className={styles["form-icon"]} />
                        <input {...register("lastName")} placeholder="Last Name" value={lastName} className={styles["form-group"]} onChange={(event) => {
                            setLastName(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <HiOutlineBriefcase className={styles["form-icon"]} />
                        <input {...register("companyName")} placeholder="Company Name" value={companyName} className={styles["form-group"]} onChange={(event) => {
                            setCompanyName(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <IoLocationOutline className={styles["form-icon"]} />
                        <input {...register("address")} placeholder="Company Address" value={address} className={styles["form-group"]} onChange={(event) => {
                            setAddress(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <IoMdDesktop className={styles["form-icon"]} />
                        <input {...register("position")} placeholder="Job Position" value={position} className={styles["form-group"]} onChange={(event) => {
                            setPosition(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <FiPhone className={styles["form-icon"]} />
                        <input {...register("workContact")} placeholder="Work Phone" value={workContact} className={styles["form-group"]} onChange={(event) => {
                            setWorkContact(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <MdSmartphone className={styles["form-icon"]} />
                        <input {...register("mobile")} placeholder="Mobile Phone" value={mobile} className={styles["form-group"]} onChange={(event) => {
                            setMobile(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <AiOutlineMail className={styles["form-icon"]} />
                        <input {...register("email")} placeholder="Work Email" value={email} className={styles["form-group"]} onChange={(event) => {
                            setEmail(event.currentTarget.value);
                        }} />
                    </div>
                    <div className={styles["form-container"]}>
                        <RiToolsFill className={styles["form-icon"]} />
                        <Controller
                            name="categoryIDorName"
                            render={({ field }) => (
                                <CreatableSelect
                                    {...field}
                                    className={styles["selector-bar"]}
                                    options={selectionArray}
                                    isClearable={false}
                                    placeholder={"Type or select category"}
                                    defaultValue={""}
                                />
                            )}
                            control={control}
                            defaultValue={""}
                        />
                    </div>
                    <input type="submit" className={styles["submit-btn"]} value="CONFIRM AND SAVE" />
                </form>
            </div>
        </Background>
    )
}

export default ReviewScannedCardForm;
