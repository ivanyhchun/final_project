import React, { useState } from 'react';
import styles from './Settings.module.scss';
import { IoClose } from 'react-icons/io5';
import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io';
import { NavLink } from 'react-router-dom';
import { IoPerson } from 'react-icons/io5';
import { AiFillProfile } from 'react-icons/ai';
import { BsShieldLockFill } from 'react-icons/bs';
import { logout } from './redux/auth/action';
import { useDispatch } from 'react-redux';

interface ISettings {
    menuState: boolean
    setMenuState: () => void
}

function Settings(props: ISettings) {
    const dispatch = useDispatch();
    return (
        <>
            <div className={props.menuState ? styles["myNavCls"] : styles["myNav"]}>
                <div className={styles["page-header"]}>
                    <NavLink to="#" className={styles["ioiosArrowBack"]}>
                        <IoIosArrowBack />
                    </NavLink>
                    <h4>Settings</h4>
                    <div className={styles["ioClose"]} onClick={props.setMenuState}>
                        <IoClose />
                    </div>
                </div>
                <div className={styles["myNavList"]}>
                    <NavLink to="/editProfile" className={styles["navLink"]}>
                        <IoPerson className={styles["navLink-icon"]} />
                        <div className={styles["navLink-text"]}>
                            Profile
                        </div>
                        <IoIosArrowForward className={styles["navLink-icon"]} />
                    </NavLink>
                    <NavLink to="/resetPassword" className={styles["navLink"]}>
                        <AiFillProfile className={styles["navLink-icon"]} />
                        <div className={styles["navLink-text"]}>
                            Account
                        </div>
                        <IoIosArrowForward className={styles["navLink-icon"]} />
                    </NavLink>
                </div>
                <div className={styles["logout-container"]}>
                    <a href="/login"> <button className={styles["logout-btn"]} onClick={() => dispatch(logout())}>LOGOUT</button></a>
                </div>
            </div>
        </>
    )
}

export default Settings;
