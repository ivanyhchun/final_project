import React from 'react';
import { AiOutlineMail } from 'react-icons/ai';
import { ImPhone } from 'react-icons/im';
import { IoIosMail, IoMdMail } from 'react-icons/io';
import { MdMail, MdSmartphone } from 'react-icons/md';
import styles from './ContactBar.module.scss';


export interface IContactBar {
    workContact: string
    mobile: string
    email: string
}

function ContactBar(props: IContactBar) {

    return (
        <div className={styles["contact-bar-container"]}>
            <a href={`tel:${props.workContact}`} className={styles["contact-group"]}>
                <ImPhone className={styles["contact-icon"]} />
            </a>
            <a href={`tel:${props.mobile}`} className={styles["contact-group"]}>
                <MdSmartphone className={styles["contact-icon"]} />
            </a>
            <a href={`mailto:${props.email}`} className={styles["contact-group"]}>
                <MdMail className={styles["contact-icon"]} />
            </a>
        </div>
    )
}

export default ContactBar;