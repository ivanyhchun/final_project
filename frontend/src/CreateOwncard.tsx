import React from 'react'; 
import { IoIosArrowBack } from 'react-icons/io';
import { VscGear } from 'react-icons/vsc';
import { NavLink } from 'react-router-dom';
import Background from './Background';
import CardForm from './CardForm';
import styles from './CreateOwncard.module.scss'; 
import LoginFooter from './LoginFooter';
import LoginHeader from './LoginHeader';

function CreateOwncard() {
    return (
        <Background name="bkgd-profile">
            <div className={styles["page-header"]}>
                <NavLink to="/profile" className={styles["ioiosArrowBack"]}>
                    <IoIosArrowBack />
                </NavLink>
                <h4>Create New Card</h4>
                <div className={styles["vscGear"]}>
                    <VscGear />
                </div>
            </div>
            <LoginHeader>
                Card Information
            </LoginHeader>
            <div className={styles["form-body"]}>
                <CardForm name="CreateCard" />
            </div>
        </Background>
    )
}

export default CreateOwncard; 