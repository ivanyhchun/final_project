import React from 'react';
import styles from './IndexPage.module.scss';
import Background from './Background';
import { display } from 'html2canvas/dist/types/css/property-descriptors/display';
import { useDispatch } from 'react-redux';
import { push } from 'connected-react-router';

function IndexPage() {

    const dispatch = useDispatch(); 

    return (
        <Background name="bkgd-main">
            <div className={styles["logo-container"]}>
                <div className={styles["logo-main"]}>
                    <img src="apple-touch-icon.png" alt="LOGO HERE" />
                    Keep-in-Touch
                    </div>
                <div className={styles["logo-text"]}>
                    Business Card Creation and Management
                    </div>
            </div>
            <div className={styles["login-container"]}>
                <button className={styles["signup-btn"]} onClick={() => {
                    dispatch(push('/signup')); 
                }}>
                    GET START
                    </button>
                <button className={styles["login-btn"]} onClick={() => {
                    dispatch(push('/login')); 
                }}>
                    SIGN IN
                    </button>
            </div>
        </Background>
    )
}

export default IndexPage;