import React from 'react'; 
import { useForm } from 'react-hook-form'; 
import styles from './CreateCard.module.scss'; 
import Background from './Background'; 
import LoginHeader from './LoginHeader'; 
import LoginFooter from './LoginFooter'; 
import CardForm from './CardForm'; 


function CreateCard() { 

    return (
        <Background name="bkgd-main">
            <LoginHeader>
                Create Your Own Business Card
            </LoginHeader>
            <div className={styles["form-body"]}>
                <CardForm name="CreateCard" />
                <LoginFooter>
                    SKIP DRAWING
                </LoginFooter>
            </div>
        </Background>
    )
}

export default CreateCard; 
