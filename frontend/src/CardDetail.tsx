import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from './store';
import styles from './CardDetail.module.scss';
import Background from './Background';
import useRouter from 'use-react-router';
import { addFavorite, deleteNamecard, fetchOneCard } from './redux/namecards/action';
import { FiPhone } from 'react-icons/fi';
import { AiFillHeart, AiFillStar, AiOutlineMail } from 'react-icons/ai'
import { MdSmartphone } from 'react-icons/md'
import { IoLocationOutline, IoTrashOutline } from 'react-icons/io5'
import { HiOutlineBriefcase } from 'react-icons/hi'
import { NavLink } from 'react-router-dom';
import { IoIosArrowBack, IoMdDesktop } from 'react-icons/io';
import { VscGear } from 'react-icons/vsc';
import { RiToolsFill } from 'react-icons/ri';
import { push } from 'connected-react-router';
import CreatableSelect from 'react-select/creatable';
import { fetchCategoryList, updateNamecardCategory } from './redux/category/action';
import ContactBar from './ContactBar';

export default function CardDetail() {
    const selectedNamecard = useSelector((state: RootState) => state.oneNamecard.oneNamecard);
    const existingCategoryArray = useSelector((state: RootState) => state.categoryList.categoryList)
    const router = useRouter<{ id?: string }>();
    const addressQuery = 'https://www.google.com/maps/search/?api=1&query=' + encodeURI(selectedNamecard.address)

    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(fetchOneCard(router.match.params.id + ''));
        dispatch(fetchCategoryList())
    }, [])
    let selectionArray:  Object[] = [];
    selectionArray = existingCategoryArray.map(data=>(
        {value:data.categoryId,
        label:data.categoryName}
    ))
    const handleChange = (newValue: any) =>{
        dispatch(updateNamecardCategory(selectedNamecard.id,selectedNamecard.memberId,newValue.value,newValue.label))
    }

    return (
        <Background name="bkgd-namecard">
            {selectedNamecard &&
                <>
                    <div className={styles["page-header"]}>
                        <NavLink to="/homepage" className={styles["ioiosArrowBack"]}>
                            <IoIosArrowBack />
                        </NavLink>
                        <h4>{selectedNamecard.firstName}'s Profile</h4>
                        {!selectedNamecard.memberId &&
                            <div className={styles["vscGear"]} onClick={() => {
                                dispatch(push('/homepage/' + selectedNamecard.externalId + '/editNamecard'));
                            }}>
                                <VscGear />
                            </div>
                        }
                        {selectedNamecard.memberId &&
                            <div className={styles["vscGear"]} onClick={() => {
                                if (selectedNamecard.memberId) {
                                    const confirmBox = window.confirm(`Are you sure you want to delete ${selectedNamecard.firstName}'s card?`);
                                    if (confirmBox) {
                                        dispatch(deleteNamecard(selectedNamecard.memberId, selectedNamecard.id));
                                    }
                                }
                            }}>
                                <IoTrashOutline />
                            </div>
                        }
                    </div>
                    <div className={styles["profile-container"]}>
                        <div className={styles["profile-img-container"]}>
                            <img src={selectedNamecard.profileImgURL + ""}
                                alt="profile image" />
                            <div className={styles["fav-btn-container"]} onClick={() => {
                                dispatch(addFavorite(selectedNamecard.externalId, selectedNamecard.favorite));
                            }}>
                                <AiFillStar className={selectedNamecard.favorite ? styles["fav-btn-clicked"] : styles["fav-btn"]} />
                            </div>
                        </div>
                        <div className={styles["profile-name"]}>
                            {selectedNamecard.firstName} {selectedNamecard.lastName}
                        </div>
                        <div className={styles["email"]}>
                            {selectedNamecard.position}
                        </div>

                    </div>
                    <ContactBar workContact={selectedNamecard.workContact} mobile={selectedNamecard.mobile} email={selectedNamecard.email} />
                    <div className={styles["info-container"]}>
                        <div className={styles["cards-container"]}>
                            <div className={styles["form-container"]}>
                                <HiOutlineBriefcase className={styles["form-icon"]} />
                                <div className={styles["form-group"]}>
                                    {selectedNamecard.companyName}
                                </div>
                            </div>
                            <div className={styles["form-container"]}>
                                <IoLocationOutline className={styles["form-icon"]} />
                                <div className={styles["form-group"]}>
                                    <a href={addressQuery}>
                                        {selectedNamecard.address}
                                    </a>
                                </div>
                            </div>
                            <div className={styles["form-container"]}>
                                <IoMdDesktop className={styles["form-icon"]} />
                                <div className={styles["form-group"]}>
                                    {selectedNamecard.position}
                                </div>
                            </div>
                            <div className={styles["form-container"]}>
                                <FiPhone className={styles["form-icon"]} />
                                <div className={styles["form-group"]}>
                                    <a href={`tel:${selectedNamecard.workContact}`}>
                                        {selectedNamecard.workContact}
                                    </a>
                                </div>
                            </div>
                            <div className={styles["form-container"]}>
                                <MdSmartphone className={styles["form-icon"]} />
                                <div className={styles["form-group"]}>
                                    <a href={`tel:${selectedNamecard.mobile}`}>
                                        {selectedNamecard.mobile}
                                    </a>
                                </div>
                            </div>
                            <div className={styles["form-container"]}>
                                <AiOutlineMail className={styles["form-icon"]} />
                                <div className={styles["form-group"]}>
                                    <a href={`mailto:${selectedNamecard.email}`}>
                                        {selectedNamecard.email}
                                    </a>
                                </div>
                            </div>
                            <div className={styles["form-container"]}>
                                <RiToolsFill className={styles["form-icon"]} />
                                {selectedNamecard.memberId? 
                                    <CreatableSelect
                                        className={styles["selector-bar"]}
                                        options={selectionArray}
                                        isClearable={true}
                                        placeholder={selectedNamecard.categoryName}
                                        onChange={handleChange}
                                        defaultValue={{key:selectedNamecard.categoryId, value:selectedNamecard.categoryName}}
                                    />
                                :
                                <div className={styles["form-group"]}>
                                    {selectedNamecard.categoryName}
                                </div>
                                }
                            </div>
                        </div>
                        <div className={styles["pageContainer"]}>
                            <div className={styles["card-container"]}>
                                <img src={selectedNamecard.namecardImgURL + ''} alt=""></img>
                            </div>
                        </div>
                    </div>
                </>
            }
        </Background>
    )
}