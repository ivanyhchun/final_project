import { UserController } from './controllers/UserController'
import { UserService } from './services/UserServices';
import express from 'express';
import path from "path";
import multer from "multer";
import KnexFunction from 'knex';
import { initRoute, userRoutes } from './routes';
import { NamecardsService } from './services/NamecardsServices';
import { NamecardsController } from './controllers/NamecardsController';
import { OwncardsService } from './services/OwncardsServices';
import { OwncardsController } from './controllers/OwncardsController';
import cors from 'cors';
import { Bearer } from 'permit';
import jwtSimple from 'jwt-simple';
import { CategoryController } from './controllers/CategoryController';
import { CategoryService } from './services/CategoryServices';
import { MessageService } from './services/MessageServices';
import { MessageController } from './controllers/MessageController';
import { CreatedCardsService } from './services/CreatedCardsServices';
import { CreatedCardsController } from './controllers/CreatedCardsContraller';

const knexfile = require('./knexfile');
const configEnv = process.env.NODE_ENV || 'development';
const knex = KnexFunction(knexfile[configEnv]);

const storageProfileImg = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve("./protected/profileImg"));
    },
    filename: function (req, file, cb) {
        cb(
            null,
            `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`
        );
    },
});

const storageCard = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve("./protected/non-mem-namecards"));
    },
    filename: function (req, file, cb) {
        cb(
            null,
            `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`
        );
    },
});

const storageCardBG = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve("./protected/owncard-image"));
    },
    filename: function (req, file, cb) {
        cb(
            null,
            `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`
        );
    },
});

export const uploadProfileImg = multer({ storage:storageProfileImg });
export const uploadCards = multer({ storage: storageCard });
export const uploadCardBG = multer({ storage: storageCardBG });

const app = express();
app.use(cors())

app.use(express.urlencoded({ extended: true, limit: "10mb" }))
app.use(express.json({ limit: "10mb" }));

const permit = new Bearer({
    query: 'access_token'
})

export const isLoggedIn = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try{
        const token = permit.check(req)
        const payload = jwtSimple.decode(token, process.env.JWT_SECRET!)
        if (payload) {
            let user = await knex.select('*').from('users').where('users.email', payload.email).first();
            req['user']={
                id:user.id,
            }
            next();
        } else {
            res.status(401).json({ message: "Unauthorized" });
        }
    }catch (e){
        res.status(401).json({ message: "invalid token" });
    }
}


app.get('/current-user', isLoggedIn, async function (req, res) {
    try {
        res.json(
            req['user']
        );
    } catch (err) {
        console.log(err);
        res.status(401).json({ message: "Unauthorized" })
    }
});

export const userService = new UserService(knex);
export const userController = new UserController(userService);
export const categoryService = new CategoryService(knex)
export const categoryController = new CategoryController(categoryService) 
export const namecardsService = new NamecardsService(knex); 
export const namecardsController = new NamecardsController(namecardsService,categoryService);
export const messageService = new MessageService(knex); 
export const messageController = new MessageController(messageService); 
export const createdCardsServicer = new CreatedCardsService(knex); 
export const createdCardsController = new CreatedCardsController(createdCardsServicer); 
export const owncardsService = new OwncardsService(knex); 
export const owncardsController = new OwncardsController(owncardsService, messageService); 


initRoute();

app.use(express.static("public"));

app.use("/", userRoutes);
app.use(isLoggedIn, express.static('protected'));

const PORT = 8080; 

app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT})`); 
});