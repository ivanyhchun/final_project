import express from "express";
import { userController, 
    namecardsController, 
    owncardsController, 
    categoryController, 
    uploadProfileImg, 
    uploadCards, 
    isLoggedIn, 
    messageController, 
    createdCardsController, 
    uploadCardBG } from "./main";

export const userRoutes = express.Router();

export function initRoute() {

    userRoutes.post('/signup', userController.signup);
    userRoutes.post("/login", userController.login);

    userRoutes.use(isLoggedIn);
    userRoutes.get('/userprofile', userController.loadUserProfile);
    userRoutes.put('/userprofile', userController.editProfileName);
    userRoutes.put('/password', userController.chgPassword);
    userRoutes.get('/userprofile/image/:path', userController.getProfileImage)
    userRoutes.put('/userprofile/image', uploadProfileImg.single('image'), userController.putProfileImage);

    userRoutes.get('/namecard', namecardsController.loadOneNamecard);
    userRoutes.post('/namecard',namecardsController.postNonMemberCard);
    userRoutes.put('/namecard', namecardsController.editNonMemberCard); 
    userRoutes.delete('/namecard', namecardsController.deleteNamecard);
    userRoutes.get('/namecard/list', namecardsController.loadNamecardsList);

    userRoutes.get('/namecard/camera/image/:path', namecardsController.getNonMemberCardImage);
    userRoutes.post('/namecard/camera/image', uploadCards.single('image'),namecardsController.uploadNonMemberCardImage);
    userRoutes.post('/namecard/qrcode/:externalId', owncardsController.QRCodeAddCard);

    userRoutes.put('/namecard/favorite', namecardsController.addFavorite); 
    userRoutes.put('/namecard/catgory', namecardsController.updateNamecardCategory); 

    userRoutes.get('/category',categoryController.getCategory); 
    userRoutes.post('/category',categoryController.postCategory); 
    userRoutes.put('/category',categoryController.putCategory); 
    userRoutes.delete('/category',categoryController.deleteCategory); 
    
    userRoutes.get('/message', messageController.loadMessage); 
    userRoutes.put('/message', messageController.messageIsRead); 
    userRoutes.delete('/message', messageController.deleteMessage); 
    
    userRoutes.get('/owncard', owncardsController.loadOneOwncard);
    userRoutes.post('/owncard', owncardsController.createOwncard);
    userRoutes.put('/owncard', owncardsController.editOwncard);
    userRoutes.delete('/owncard', owncardsController.deleteOwncard);
    userRoutes.get('/owncard/list', owncardsController.loadOwncardsList);
    userRoutes.get('/owncard/image/:path', owncardsController.getNameCardImg)
    userRoutes.get('/owncard/backgroundimage/:path', owncardsController.CardBGImage)
    userRoutes.put('/owncard/backgroundimage', uploadCardBG.single('image'), createdCardsController.putBgImage);
    userRoutes.put('/owncard/logoimage', uploadCardBG.single('image'), createdCardsController.putLogoImage);
    userRoutes.delete('/owncard/logoimage',createdCardsController.delLogoImage);

    userRoutes.post('/cardElements', owncardsController.cardElements); 
    userRoutes.post('/DwgToPNG', owncardsController.drawingToPNG);
    userRoutes.get('/cardObj', createdCardsController.loadCardObj);
}