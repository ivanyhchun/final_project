import { Knex } from 'knex';
import { Namecard} from './models';

export class NamecardsService {
    constructor(private knex: Knex) {
    }

    async loadNamecardsList(id: number) {
        try{
            const nonMemCardColumnNames = [
            'non_member_card.id',
            'non_member_card.first_name as firstName',
            'non_member_card.last_name as lastName',
            'non_member_card.company_name as companyName',
            'non_member_card.position',
            'non_member_card.external_id as externalId',
            'non_member_card.favorite',
            'non_member_card.updated_at as updatedAt',
            'category.id as categoryId',
            'category.category_name as categoryName'
            ]
            const MemCardColumnNames = [
            'namecard.id',
            'namecard.user_id as memberId',
            'namecard.first_name as firstName',
            'namecard.last_name as lastName',
            'namecard.company_name as companyName',
            'namecard.position',
            'namecard.external_id as externalId',
            'namecard.favorite',
            'member_relationship.updated_at as updatedAt',
            'category.id as categoryId',
            'category.category_name as categoryName',
            'users.profile_img as profileImg'
            ]
            let nonMemCardQuery = this.knex.select(nonMemCardColumnNames)
                .from('non_member_card')
                .leftJoin('category', 'non_member_card.category_id', 'category.id')
                .where('non_member_card.user_id', id);
            let memCardQuery = this.knex.select(MemCardColumnNames)
                .from('namecard')
                .innerJoin('member_relationship', 'namecard.id', 'member_relationship.namecard_id')
                .innerJoin('users', 'namecard.user_id', 'users.id')
                .leftJoin('category', 'category.id', 'member_relationship.category_id')
                .where('member_relationship.user_id', id);
            let tempArr = await nonMemCardQuery
            const nonMemCardResult: Namecard[] = tempArr.map(item => ({
                ...item,
                memberId: null
            }))
            const memCardResult: Namecard[] = await memCardQuery
            let totalResult: Namecard[] = nonMemCardResult.concat(memCardResult)
            return totalResult;
        } catch (error){
            throw error;
        }
    }

    async loadOneNamecard(id: number, queryId: any) {
        try{
            const nonMemCardColumnNames = [
            'non_member_card.id',
            'non_member_card.first_name as firstName',
            'non_member_card.last_name as lastName',
            'non_member_card.company_name as companyName',
            'non_member_card.namecard_img as namecardImg',
            'non_member_card.mobile',
            'non_member_card.work_tel as workContact',
            'non_member_card.email',
            'non_member_card.address',
            'non_member_card.position',
            'non_member_card.external_id as externalId',
            'non_member_card.favorite',
            'category.id as categoryId',
            'category.category_name as categoryName'
            ]
            const MemCardColumnNames = [
            'namecard.id',
            'namecard.user_id as memberId',
            'namecard.first_name as firstName',
            'namecard.last_name as lastName',
            'namecard.company_name as companyName',
            'namecard.namecard_img as namecardImg',
            'namecard.mobile',
            'namecard.work_tel as workContact',
            'namecard.email',
            'namecard.address',
            'namecard.position',
            'namecard.external_id as externalId',
            'namecard.favorite',
            'category.id as categoryId',
            'category.category_name as categoryName',
            'users.profile_img as profileImg'
            ]
            let nonMemCardQuery = await this.knex.select(nonMemCardColumnNames)
                .from('non_member_card')
                .leftJoin('category', 'non_member_card.category_id', 'category.id')
                .where('external_id', queryId)
                .andWhere('non_member_card.user_id', id)
            let memCardQuery = await this.knex.select(MemCardColumnNames)
                .from('namecard')
                .innerJoin('member_relationship', 'namecard.id', 'member_relationship.namecard_id')
                .innerJoin('users', 'users.id', 'namecard.user_id')
                .leftJoin('category', 'category.id', 'member_relationship.category_id')
                .where('external_id', queryId)
                .andWhere('member_relationship.user_id', id)
            let totalResult = nonMemCardQuery.concat(memCardQuery)
            return totalResult;
        } catch (error){
            throw error
        }
    }

    async insertNonMemberCard(
        userId: number,
        firstName: string,
        lastName: string,
        position: string,
        companyName: string,
        address: string,
        workContact: string,
        mobile: string,
        email: string,
        namecardImg?: string|null,
        categoryId?:number|null
    ) {
        try {
            const externalId = Math.random().toString(36).slice(-8); 
            return await this.knex('non_member_card').insert({
                "user_id":userId,
                'first_name': firstName,
                'last_name': lastName,
                'position': position,
                'company_name': companyName,
                'address': address,
                'work_tel': workContact,
                'mobile': mobile,
                'email': email,
                'namecard_img': namecardImg,
                'category_id':categoryId,
                'external_id':externalId
            }).returning('id');
        } catch (error) {
            throw error;
        }
    }
    
    async editNonMemberCard(
        cardId: string,
        userId: number,
        firstName: string,
        lastName: string,
        position: string,
        companyName: string,
        address: string,
        workContact: string,
        mobile: string,
        email: string,
        namecardImg?: string|null,
        categoryId?:number|null
    ) {
        try {
            await this.knex('non_member_card').where('external_id', cardId).andWhere('user_id', userId).update({
                'first_name': firstName,
                'last_name': lastName,
                'position': position,
                'company_name': companyName,
                'address': address,
                'work_tel': workContact,
                'mobile': mobile,
                'email': email,
                'namecard_img': namecardImg,
                'category_id':categoryId,
            })
        } catch (error) {
            throw error;
        }
    }

    async deleteNamecard(userId: number, isMember: boolean, cardId: number) {
        const trx = await this.knex.transaction();
        try {
            if (isMember) {
                await trx('message').where('namecard_id', cardId).andWhere('user_id', userId).del();
                await trx('member_relationship').where('namecard_id', cardId).andWhere('user_id', userId).del();
                await trx('namecard').where('id', cardId).andWhere('user_id', userId).del();
                await trx.commit();
            } else {
                await trx('non_member_card').where('id', cardId).andWhere('user_id', userId).del();
                await trx.commit();
            }
        } catch (error) {
            await trx.rollback();
            throw error;
        }
    }

    async updateNamecardCategoryMember(userId:number,categoryId:number,namecardId:number){
        try{
            return await this.knex("member_relationship")
                                .where('namecard_id', namecardId)
                                .andWhere('user_id', userId)
                                .update('category_id',categoryId).returning("id")
        } catch (error){
            throw error;
        }
    }

    async updateNamecardCategoryNonMember(userId:number,categoryId:number,namecardId:number){
        try{
            return await this.knex("non_member_card")
                                .where('namecard_id', namecardId)
                                .andWhere('user_id', userId)
                                .update('category_id',categoryId).returning("id")
        } catch (error){
            throw error;
        }
    }

    async addFavorite(cardId: any, isFavorite: boolean) {
        try {
            const chkCard = await this.knex.select('*').from('namecard').where('external_id', cardId);
            if (chkCard.length > 0) {
                await this.knex('namecard').where('external_id', cardId).update('favorite', !isFavorite);
            } else {
                await this.knex('non_member_card').where('external_id', cardId).update('favorite', !isFavorite);
            }
        } catch (error) {
            throw error;
        }
    }

}