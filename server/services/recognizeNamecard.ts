const { FormRecognizerClient, AzureKeyCredential } = require("@azure/ai-form-recognizer");

const fs = require("fs");
const dotenv = require("dotenv");
dotenv.config();

export async function recognizeCards(input: string) {
	const endpoint = "https://business-card-app.cognitiveservices.azure.com/";
	const apiKey = process.env.AZURE_KEY;
	const fileName = input;

	if (!fs.existsSync(fileName)) {
		throw new Error(`Expected file "${fileName}" to exist.`);
	}

	const readStream = fs.createReadStream(fileName);

	const client = new FormRecognizerClient(endpoint, new AzureKeyCredential(apiKey));
	const poller = await client.beginRecognizeBusinessCards(readStream, {
		contentType: "image/png",
		onProgress: (state: { status: string }) => {
			console.log(`status: ${state.status}`);
		}
	});

	const [businessCard] = await poller.pollUntilDone();

	if (businessCard === undefined) {
		throw new Error("Failed to extract data from at least one business card.");
	}

	let namecardFirstName = "";
	let namecardLastName = "";
	let namecardCompanyName = "";
	let namecardEmail = "";
	let namecardAddress = "";
	let namecardMobile = "";
	let namecardWorkContact = "";
	let namecardPosition = "";

	if (businessCard.fields["ContactNames"]?.value[0].value["FirstName"].value) {
		namecardFirstName = businessCard.fields["ContactNames"].value[0].value["FirstName"].value
	}

	if (businessCard.fields["ContactNames"]?.value[0].value["LastName"].value) {
		namecardLastName = businessCard.fields["ContactNames"].value[0].value["LastName"].value
	}

	if (businessCard.fields["CompanyNames"]?.value[0].value) {
		namecardCompanyName = businessCard.fields["CompanyNames"].value[0].value
	}

	if (businessCard.fields["Addresses"]?.value[0].value) {
		namecardAddress = businessCard.fields["Addresses"].value[0].value
	}

	if (businessCard.fields["MobilePhones"]?.value[0].value) {
		namecardMobile = businessCard.fields["MobilePhones"].value[0].value
	}

	if (businessCard.fields["WorkPhones"]?.value[0].value) {
		namecardWorkContact = businessCard.fields["WorkPhones"].value[0].value
	}

	if (businessCard.fields["Emails"]?.value[0].value) {
		namecardEmail = businessCard.fields["Emails"].value[0].value
	}

	if (businessCard.fields["JobTitles"]?.value[0].value) {
		namecardPosition = businessCard.fields["JobTitles"].value[0].value
	}

	
	const formattedBusinessCard = {
		firstName: namecardFirstName,
		lastName:namecardLastName,
		companyName: namecardCompanyName,
		address: namecardAddress,
		mobile: namecardMobile,
		workContact: namecardWorkContact,
		email: namecardEmail,
		position: namecardPosition
	}

	return formattedBusinessCard

}

