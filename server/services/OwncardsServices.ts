import { Knex } from 'knex';

export class OwncardsService {

    constructor(private knex: Knex) {

    }

    async loadProfile(id: number) {
        try{
            const columnNames = [
                'users.first_name as firstName',
                'users.last_name as lastName',
                'users.email',
                'users.profile_img as profileImg'
            ]
            return await this.knex.select(columnNames).from('users').where('id', id);
        } catch (error){
            throw error
        }
    }

    async loadOwncardsList(id: number) {
        try{
            const columnNames = [
                'namecard.id',
                'namecard.first_name as firstName',
                'namecard.last_name as lastName',
                'namecard.company_name as companyName',
                'namecard.mobile',
                'namecard.work_tel as workContact',
                'namecard.email',
                'namecard.address',
                'namecard.position',
                'namecard.namecard_img as namecardImg',
                'namecard.external_id as externalId',
            ]
            return await this.knex.select(columnNames).from('namecard').where('user_id', id);
        } catch (error){
            throw error
        }
        
    }

    async loadOneOwncard(queryId: number) {
        try{
            const columnNames = [
                'namecard.id',
                'namecard.first_name as firstName',
                'namecard.last_name as lastName',
                'namecard.company_name as companyName',
                'namecard.mobile',
                'namecard.work_tel as workContact',
                'namecard.email',
                'namecard.address',
                'namecard.position',
                'namecard.namecard_img as namecardImg',
                'namecard.external_id as externalId',
                'namecard.bg_image as bgImg'
            ]
            return await this.knex.select(columnNames).from('namecard').where('id', queryId);    
        } catch (error){
            throw error
        }
    }

    async createOwncard(
        userId: number,
        firstName: string,
        lastName: string,
        position: string,
        companyName: string,
        address: string,
        workContact: string,
        mobile: string,
        email: string
    ) {
        const externalId = Math.random().toString(36).slice(-8); 
        const trx = await this.knex.transaction()
        try {
            const cardId = await trx('namecard').insert({
                'user_id': userId,
                'first_name': firstName,
                'last_name': lastName,
                'position': position,
                'company_name': companyName,
                'address': address,
                'work_tel': workContact,
                'mobile': mobile,
                'email': email,
                'external_id':externalId,
                'bg_image': "card_sample_a2.jpeg"
            }).returning('id'); 

            let contentObj = JSON.stringify({
                firstName: { top: 0, left: 0, title: firstName },
                companyName: { top: 0, left: 0, title: companyName },
                lastName: { top: 0, left: 0, title: lastName },
                email: { top: 0, left: 0, title: email },
                address: { top: 0, left: 0, title: address },
                position: { top: 0, left: 0, title: position },
                workContact: { top: 0, left: 0, title: workContact },
                mobile: { top: 0, left: 0, title: mobile }
            })
            let cardIdnum = parseInt(cardId+"")
            await trx('createdcard').insert({
                'user_id': userId,
                'namecard_id': cardIdnum,
                'content': contentObj
            })
            trx.commit()
            return cardId;
        } catch (error) {
            trx.rollback()
            throw error;
        }
    }

    async cardData(userId: string, cardData: {}) {
        try{
            return await this.knex('namecard').insert({
                'user_id': userId,
                'content': cardData
            }).returning('id')
        } catch (error){
            throw error
        }
    }

    async cardElements(namecard_id: string, userId: string, boxes: any) {
        try{
            return await this.knex('createdcard').insert({
                'namecard_id': namecard_id,
                'user_id': userId,
                'content': boxes
            }).onConflict('namecard_id').merge(['user_id', 'content']).returning('id')
        } catch (error) {
            throw error
        }
    }

    async editOwncard(
        id: number,
        userId: number,
        firstName: string,
        lastName: string,
        position: string,
        companyName: string,
        address: string,
        workContact: string,
        mobile: string,
        email: string,
        namecardImg: string
    ) {
        try {
            await this.knex('namecard').where('id', id)
                                        .andWhere('user_id', userId)
                                        .update({
                                            'first_name': firstName,
                                            'last_name': lastName,
                                            'position': position,
                                            'company_name': companyName,
                                            'address': address,
                                            'work_tel': workContact,
                                            'mobile': mobile,
                                            'email': email,
                                            'namecard_img': namecardImg
                                        })

            let result = (await this.knex.select("*")
                                            .from('createdcard')
                                            .where('namecard_id', id)
                                            .andWhere('user_id', userId))[0]
            let namecardContent = JSON.parse(result.content)
            namecardContent.firstName.title = firstName
            namecardContent.lastName.title = lastName
            namecardContent.companyName.title = companyName
            namecardContent.address.title = address
            namecardContent.position.title = position

            if (workContact != "" ){
                if (namecardContent.workContact) {
                    namecardContent.workContact.title = workContact
                } else {
                    namecardContent.workContact = { top: 0, left: 0, title: workContact }
                }
            } else {
                delete namecardContent.workContact
            }

            if (mobile != "" ){
                if (namecardContent.mobile) {
                    namecardContent.mobile.title = mobile
                } else {
                    namecardContent.mobile = { top: 0, left: 0, title: mobile }
                }
            } else {
                delete namecardContent.mobile
            }

            if (email != "" ){
                if (namecardContent.email) {
                    namecardContent.email.title = email
                } else {
                    namecardContent.email = { top: 0, left: 0, title: email }
                    }
            } else {
                delete namecardContent.email
            }

            await this.knex('createdcard')
                        .update('content', namecardContent)
                        .where('namecard_id', id)
                        .returning('id')
            
        } catch (error) {
            throw error;
        }
    }

    async deleteOwncard(cardId: number) {
        const trx = await this.knex.transaction();
        try {
            await trx('createdcard').where('namecard_id', cardId).del();
            await trx('message').where('namecard_id', cardId).del();
            await trx('member_relationship').where('namecard_id', cardId).del();
            await trx('namecard').where('id', cardId).del();
            await trx.commit();
        } catch (error) {
            await trx.rollback();
            throw error;
        }
    }

    async drawingToPNG(userId: string, id: string, filepath: string) {
        try {
            return await this.knex('namecard').insert({
                'user_id': userId,
                'id': id,
                'first_name': "",
                "last_name": "",
                'namecard_img': filepath,
                'created_at': new Date(),
                'updated_at': new Date()
            }).onConflict('id')
                .merge(['namecard_img', 'updated_at'])
                .returning('id');
        } catch (error){
            throw error
        }
    }

    async checkQRCodeExternalID(externalId: string) {
        try {
            return await this.knex.select('namecard.id')
                .from('namecard')
                .where('external_id', externalId)
        } catch (error) {
            throw error
        }
    }

    async addMemberRelationship(userId: number, cardId: number) {
        try {
            let categoryId = 0;
            let firstCategoryId = await this.knex.select("id")
                .from('category')
                .where('user_id', userId).first()
            if (firstCategoryId) {
                categoryId = firstCategoryId.id
            } else {
                categoryId = (await this.knex('category').insert({
                    'user_id': userId,
                    'category_name': "General"
                }).returning('id'))[0]
            }

            let checkExist = await this.knex.select('*')
                .from('member_relationship')
                .where('user_id', userId)
                .andWhere("namecard_id", cardId)
            if (checkExist.length == 0) {
                return await this.knex('member_relationship').insert({
                    'user_id': userId,
                    "namecard_id": cardId,
                    "category_id": categoryId
                }).returning('id')
            } else {
                return false
            }
        } catch (error) {
            throw error
        }
    }
}