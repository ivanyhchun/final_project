import { Knex } from 'knex'

export class CreatedCardsService {

    constructor(private knex: Knex) {

    }

    async loadCardObj(queryId: string) {
        try{
            return await this.knex.select('*').from('createdcard').where('namecard_id', queryId)
        } catch (error){
            throw error
        }
    }

    async putBgImage(id: number, bgImg: string | null) {
        try{
            return await this.knex('namecard').where('namecard.id', id).update('bg_image', bgImg)
        } catch (error){
            throw error
        }
    }

    async putLogoImage(id: number, image64: string) {
        try{
            let cardcontent = await this.knex.select('content').from('createdcard').where('namecard_id', id)
            let contentObj = JSON.parse((cardcontent[0].content))
            contentObj.logo = { top: 0, left: 60, title: image64 }
            return await this.knex('createdcard').where('namecard_id', id).update('content', contentObj)
        } catch (error){
            throw error
        }
    }

    async delLogoImage(id:number){
        try{
            let cardcontent = await this.knex.select('content').from('createdcard').where('namecard_id', id)
            let contentObj = JSON.parse((cardcontent[0].content))
            delete contentObj.logo
            return await this.knex('createdcard').where('namecard_id', id).update('content', contentObj)
        } catch (error) {
            throw error
        }
    }

}