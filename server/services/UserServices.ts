import { Knex } from 'knex';
import { User } from './models';

export class UserService {

    constructor(private knex: Knex) {

    }

    async login(email: string): Promise<User> {
        try{
            return await this.knex.select('*')
                                    .from('users')
                                    .where('users.email', email)
                                    .first();
        } catch (error){
            throw error
        }
    }

    async editProfileName(id: number, firstName: string, lastName: string) {
        try {
            await this.knex('users').where('id', id).update({
                'first_name': firstName,
                'last_name': lastName
            }); 
            return await this.knex.select('*')
                                    .from('users')
                                    .where('id', id)
                                    .first(); 
        } catch (error){
            throw error
        }
    }

    async putProfileImage(id:number,profileImg:string|null){
        try{
            return await this.knex('users')
                                .where('id',id)
                                .update('profile_img',profileImg)
        } catch (error){
            throw error
        }
    }

    async signUp(
        firstName: string,
        lastName:string,
        email: string,
        hashedPassword: string): Promise<{ id: number }[]> {
        try {
            return await this.knex('users').insert({
                'first_name': firstName,
                'last_name':lastName,
                'email': email,
                'password': hashedPassword,
            }).returning('id'); 
        } catch (error){
            throw error
        }
    }

    async loadUserProfile(id: number) {
        try{
            const columnNames = [
                'users.first_name as firstName',
                'users.last_name as lastName',
                'users.email',
                'users.profile_img as profileImg'
            ]
            return await this.knex.select(columnNames).from('users').where('id', id);    
        } catch (error){
            throw error
        }
    }

    async changePassword(userId: number, userPassword: string) {
        try {
            return await this.knex('users')
                            .update('password', userPassword)
                            .where('id', userId)
                            .returning('id'); 
        } catch (error){
            throw error
        }
    }
    
    async getPassword(userId: number) {
        try {
            return await this.knex.select('password')
                                    .from('users')
                                    .where('id', userId)
                                    .first();
        } catch (error){
            throw error
        }
    }
}