import { Knex } from 'knex';

export class MessageService {

    constructor(private knex: Knex) {
    }

    async loadMessage(userId: number) {
        try {
            const columnNames = [
                'message.id',
                'message.user_id as userId',
                'message.namecard_id as namecardId',
                'message.content',
                'message.updated_at as updatedAt',
                'message.read',
                'namecard.first_name as firstName',
                'namecard.external_id as externalId'
            ]

            return await this.knex.select(columnNames).from('message')
                .innerJoin('namecard', 'message.namecard_id', 'namecard.id')
                .where('message.user_id', userId);

        } catch (error) {
            throw error;
        }
    }

    async generateMessage(cardId: number, cardName: string) {
        try {
            const userIds = await this.knex.select('member_relationship.user_id')
                                    .from('member_relationship')
                                    .innerJoin('namecard', 'member_relationship.namecard_id', 'namecard.id')
                                    .where('member_relationship.namecard_id', cardId);
            let insertItem = [];
            for (let userId of userIds) {
                insertItem.push({
                    "user_id": userId.user_id,
                    "namecard_id": cardId,
                    "content": cardName + "'s namecard has been updated! Click here and view! "
                })
            }
            if (insertItem.length > 0){
                const msgId = await this.knex('message').insert(insertItem).returning('id');
                return msgId;
            } else {
                return
            }
        } catch (error) {
            throw error;
        }
    }

    async messageIsRead(userId: number, msgId: number) {
        try {
            await this.knex('message').where('user_id', userId).andWhere('id', msgId).update('read', true); 
        } catch (error) {
            throw error; 
        }
    }

    async deleteMessage(msgIds: number[]) {
        try {
            await this.knex('message').whereIn('id', msgIds).del(); 
        } catch (error) {
            throw error; 
        }
    }

}