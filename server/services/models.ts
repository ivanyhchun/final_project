export interface User {
    id:string
    email:string;
    firstName:string ;
    lastName:string;
    password: string;
    profileImg?:string
}

export interface DrawingElement {
    profileId:string;
    html:string
}

export interface OwnCard {
    id:string;
    firstName:string;
    lastName:string;
    companyName:string;
    namecardImg:string;
    mobile:string;
    workContact:string;
    email:string;
    address:string;
    position:string;
    externalId:string
}

export interface Namecard extends OwnCard {
    categoryId:string;
    categoryName:string;
    memberId:number | null; 
    profileImg:string; 
}

export interface NameInfo{
  fullName?:string;
  company?:string;
  title?:string;
  addresses?:string;
  phone1?:string;
  phone2?:string;
  email?:string;
  websites?:string;
  linkedin?:string;
  category_id?:number;
}

export interface Category{
    id:string;
    categoryName:string
}