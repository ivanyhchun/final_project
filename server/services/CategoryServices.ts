import { Knex } from 'knex';

export class CategoryService{
    constructor(private knex:Knex){
    }

    async getCategory(memberId:number){
        try{
            return await this.knex.select(['category.id as categoryId','category.category_name as categoryName'])
            .from('category')
            .where('user_id',memberId)
        } catch (error){
            throw error
        }
    }

    async postCategory(memberId:number,categoryName:string){
        try{
            return await this.knex('category').insert({
                'user_id':memberId,
                'category_name':categoryName
            }).returning('id')
        } catch (error){
            throw error
        }
    }

    async putCategory(memberId:number,oldCategoryId:string,newCategoryName:string){
        try{
            return await this.knex('category')
            .update('category_name', newCategoryName)
            .where('id', oldCategoryId)
            .andWhere('user_id',memberId)
        } catch (error){
            throw error
        }
    }

    async deleteCategory(memberId:number,oldCategoryId:string){
        try{
            return await this.knex('category')
            .where('id', oldCategoryId)
            .andWhere('user_id',memberId)
            .del()
        } catch (error){
            throw error
        }
    }
}