import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (!(await knex.schema.hasTable('createdcard'))){
        await knex.schema.createTable('createdcard',(table)=>{
            table.increments();
            table.integer("user_id").unsigned().notNullable();
            table.foreign('user_id').references('users.id');
            table.integer("namecard_id").unsigned().notNullable().unique();
            table.foreign('namecard_id').references('namecard.id');
            table.text('content');
            table.timestamps(false,true); 
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('createdcard')  
}

