import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('message')) {
        await knex.schema.alterTable('message', (table) => {
            table.boolean('read').defaultTo('false'); 
        })
    } else {
        console.log('Table "message" does not exist.'); 
    }
}


export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('message')) {
        await knex.schema.alterTable('message', (table) => {
            table.dropColumn('read'); 
        })
    } else {
        console.log('Table "message" does not exist.'); 
    }
}

