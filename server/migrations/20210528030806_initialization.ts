import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (!(await knex.schema.hasTable('users'))){
        await knex.schema.createTable('users',(table)=>{
            table.increments();
            table.string("first_name").notNullable();
            table.string("last_name").notNullable();
            table.string("email").unique().notNullable();
            table.string("password").notNullable();
            table.string("profile_img"); 
            table.timestamps(false,true); 
        })
    }

    if (!(await knex.schema.hasTable('namecard'))){
        await knex.schema.createTable('namecard',(table)=>{
            table.increments();
            table.integer("user_id").unsigned().notNullable();
            table.foreign('user_id').references('users.id');
            table.string("first_name").notNullable();
            table.string("last_name").notNullable();
            table.string("position");
            table.string("company_name");
            table.text("address");
            table.string("work_tel");
            table.string("mobile");
            table.string("email")
            table.string("namecard_img"); 
            table.timestamps(false,true); 
        })
    }

    if (!(await knex.schema.hasTable('category'))){
        await knex.schema.createTable('category',(table)=>{
            table.increments();
            table.integer("user_id").unsigned().notNullable();
            table.foreign('user_id').references('users.id');
            table.string("category_name").notNullable();
            table.timestamps(false,true); 
        })
    }

    if (!(await knex.schema.hasTable('member_relationship'))){
        await knex.schema.createTable('member_relationship',(table)=>{
            table.increments();
            table.integer("user_id").unsigned().notNullable();
            table.foreign('user_id').references('users.id');
            table.integer("namecard_id").unsigned().notNullable();
            table.foreign('namecard_id').references('namecard.id');
            table.integer("category_id").unsigned().notNullable();
            table.foreign('category_id').references('category.id');
            table.timestamps(false,true); 
        })
    }

    if (!(await knex.schema.hasTable('non_member_card'))){
        await knex.schema.createTable('non_member_card',(table)=>{
            table.increments();
            table.integer("user_id").unsigned().notNullable();
            table.foreign('user_id').references('users.id');
            table.string("first_name").notNullable();
            table.string("last_name").notNullable();
            table.string("position");
            table.string("company_name");
            table.text("address");
            table.string("work_tel");
            table.string("mobile");
            table.string("email")
            table.string("namecard_img"); 
            table.integer("category_id").unsigned().notNullable();
            table.foreign('category_id').references('category.id');
            table.timestamps(false,true); 
        })
    }

    if (!(await knex.schema.hasTable('message'))){
        await knex.schema.createTable('message',(table)=>{
            table.increments();
            table.integer("user_id").unsigned().notNullable();
            table.foreign('user_id').references('users.id');
            table.integer("namecard_id").unsigned().notNullable();
            table.foreign('namecard_id').references('namecard.id');
            table.text('content');
            table.timestamps(false,true); 
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('message')    
    await knex.schema.dropTableIfExists('non_member_card')
    await knex.schema.dropTableIfExists('member_relationship')
    await knex.schema.dropTableIfExists('category')
    await knex.schema.dropTableIfExists('namecard')
    await knex.schema.dropTableIfExists('users')
}

