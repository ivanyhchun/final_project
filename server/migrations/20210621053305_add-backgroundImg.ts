import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('namecard')) {
        await knex.schema.alterTable('namecard', (table) => {
            table.text('bg_image'); 
        })
    } else {
        console.log('Table "namecard" does not exist.'); 
    }
}


export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('namecard')) {
        await knex.schema.alterTable('namecard', (table) => {
            table.dropColumn('bg_image'); 
        })
    } else {
        console.log('Table "namecard" does not exist.'); 
    }
}

