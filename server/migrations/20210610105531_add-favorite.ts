import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('namecard')) {
        await knex.schema.alterTable('namecard', (table) => {
            table.boolean('favorite').defaultTo('false');
        })
    } else {
        console.log('Table "namecard" does not exist.');
    }

    if (await knex.schema.hasTable('non_member_card')) {
        await knex.schema.alterTable('non_member_card', (table) => {
            table.boolean('favorite').defaultTo('false');
        })
    } else {
        console.log('Table "non_member_card" does not exist.');
    }
}


export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('namecard')) {
        await knex.schema.alterTable('namecard', (table) => {
            table.dropColumn('favorite'); 
        })
    } else {
        console.log('Table "namecard" does not exist.'); 
    }

    if (await knex.schema.hasTable('non_member_card')) {
        await knex.schema.alterTable('non_member_card', (table) => {
            table.dropColumn('favorite'); 
        })
    } else {
        console.log('Table "non_member_card" does not exist.'); 
    }
}

