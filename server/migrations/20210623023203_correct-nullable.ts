import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('non_member_card')) {
        await knex.schema.alterTable('non_member_card', (table) => {
            table.integer("category_id").unsigned().nullable().alter();
        })
    } else {
        console.log('Table "non_member_card" does not exist.');
    }
    if (await knex.schema.hasTable('member_relationship')) {
        await knex.schema.alterTable('member_relationship', (table) => {
            table.integer("category_id").unsigned().nullable().alter();
        })
    } else {
        console.log('Table "member_relationship" does not exist.');
    }
}


export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('non_member_card')) {
        await knex.schema.alterTable('non_member_card', (table) => {
            table.integer("category_id").unsigned().notNullable().alter();
        })
    } else {
        console.log('Table "non_member_card" does not exist.');
    }
    if (await knex.schema.hasTable('member_relationship')) {
        await knex.schema.alterTable('member_relationship', (table) => {
            table.integer("category_id").unsigned().notNullable().alter();
        })
    } else {
        console.log('Table "member_relationship" does not exist.');
    }
}

