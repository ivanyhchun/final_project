import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    await knex("message").del();
    await knex("non_member_card").del();
    await knex("member_relationship").del();
    await knex("category").del();
    await knex("namecard").del();
    await knex("users").del();

    await knex("users").insert([{
        "first_name": "test",
        "last_name": "account 1",
        "email": "test1@gmail.com",
        "password": "$2a$10$cQnzpyXgyjv4HXZomLVKKOetcu6eGhh/6X9qlUgxM6lGQ0.dLHYC2",
        "profile_img": "kimsoohyun_public.jpg"
      }, {
        "first_name": "test",
        "last_name": "account 2",
        "email": "test2@gmail.com",
        "password": "$2a$10$cQnzpyXgyjv4HXZomLVKKOetcu6eGhh/6X9qlUgxM6lGQ0.dLHYC2",
        "profile_img": "face.jpeg"
      }, {
        "first_name": "Danika",
        "last_name": "Moro",
        "email": "test3@gmail.com",
        "password": "$2a$10$cQnzpyXgyjv4HXZomLVKKOetcu6eGhh/6X9qlUgxM6lGQ0.dLHYC2",
        "profile_img": "face.jpeg"
      }, {
        "first_name": "Maressa",
        "last_name": "Meus",
        "email": "test4@gmail.com",
        "password": "$2a$10$cQnzpyXgyjv4HXZomLVKKOetcu6eGhh/6X9qlUgxM6lGQ0.dLHYC2",
        "profile_img": null
      }, {
        "first_name": "Flint",
        "last_name": "Clever",
        "email": "test5@gmail.com",
        "password": "$2a$10$cQnzpyXgyjv4HXZomLVKKOetcu6eGhh/6X9qlUgxM6lGQ0.dLHYC2",
        "profile_img": "kimsoohyun_public.jpg"
      }, {
        "first_name": "Myer",
        "last_name": "Fiveash",
        "email": "test66@gmail.com",
        "password": "$2a$10$cQnzpyXgyjv4HXZomLVKKOetcu6eGhh/6X9qlUgxM6lGQ0.dLHYC2",
        "profile_img": "face.jpeg"
      }, {
        "first_name": "Ted",
        "last_name": "Clift",
        "email": "test7@gmail.com",
        "password": "$2a$10$cQnzpyXgyjv4HXZomLVKKOetcu6eGhh/6X9qlUgxM6lGQ0.dLHYC2",
        "profile_img": "kimsoohyun_public.jpg"
      }, {
        "first_name": "Doralin",
        "last_name": "Peile",
        "email": "test8@gmail.com",
        "password": "$2a$10$cQnzpyXgyjv4HXZomLVKKOetcu6eGhh/6X9qlUgxM6lGQ0.dLHYC2",
        "profile_img": null
      }, {
        "first_name": "Hanson",
        "last_name": "Joanaud",
        "email": "test9@gmail.com",
        "password": "$2a$10$cQnzpyXgyjv4HXZomLVKKOetcu6eGhh/6X9qlUgxM6lGQ0.dLHYC2",
        "profile_img": null
      }, {
        "first_name": "Christye",
        "last_name": "Brinkley",
        "email": "test10@gmail.com",
        "password": "$2a$10$cQnzpyXgyjv4HXZomLVKKOetcu6eGhh/6X9qlUgxM6lGQ0.dLHYC2",
        "profile_img": "face.jpeg"
      }, {
        "first_name": "Melody",
        "last_name": "Godart",
        "email": "test11@gmail.com",
        "password": "$2a$10$cQnzpyXgyjv4HXZomLVKKOetcu6eGhh/6X9qlUgxM6lGQ0.dLHYC2",
        "profile_img": "kimsoohyun_public.jpg"
      }, {
        "first_name": "Emelen",
        "last_name": "Schoular",
        "email": "test12@gmail.com",
        "password": "$2a$10$cQnzpyXgyjv4HXZomLVKKOetcu6eGhh/6X9qlUgxM6lGQ0.dLHYC2",
        "profile_img": "face.jpeg"
      }, {
        "first_name": "Brandea",
        "last_name": "Landrick",
        "email": "test13@gmail.com",
        "password": "$2a$10$cQnzpyXgyjv4HXZomLVKKOetcu6eGhh/6X9qlUgxM6lGQ0.dLHYC2",
        "profile_img": "kimsoohyun_public.jpg"
      }, {
        "first_name": "Petronille",
        "last_name": "Paladini",
        "email": "test14@gmail.com",
        "password": "$2a$10$cQnzpyXgyjv4HXZomLVKKOetcu6eGhh/6X9qlUgxM6lGQ0.dLHYC2",
        "profile_img": null
      }, {
        "first_name": "Ryon",
        "last_name": "Bellino",
        "email": "test15@gmail.com",
        "password": "$2a$10$cQnzpyXgyjv4HXZomLVKKOetcu6eGhh/6X9qlUgxM6lGQ0.dLHYC2",
        "profile_img": "face.jpeg"
      }]);

    await knex("namecard").insert([{
      "user_id": 2,
      "first_name": "Giff",
      "last_name": "Dilloway",
      "position": "Senior Sales Associate",
      "company_name": "Russel, Adams and Kerluke",
      "address": "438 Beilfuss Center",
      "work_tel": "1541990552",
      "mobile": "6829787721",
      "email": "gdilloway0@msn.com",
      "namecard_img": "http://dummyimage.com/136x100.png/cc0000/ffffff",
      "external_id": "3wlf6aWIM"
    }, {
      "user_id": 12,
      "first_name": "Shae",
      "last_name": "Wilcott",
      "position": "Help Desk Technician",
      "company_name": "Christiansen, Hauck and Treutel",
      "address": "074 American Road",
      "work_tel": "2527112282",
      "mobile": "1018610850",
      "email": "swilcott1@disqus.com",
      "namecard_img": "http://dummyimage.com/143x100.png/5fa2dd/ffffff",
      "external_id": "bbBY8EoM9"
    }, {
      "user_id": 5,
      "first_name": "Melissa",
      "last_name": "Vedyaev",
      "position": "VP Marketing",
      "company_name": "Robel, McGlynn and Jenkins",
      "address": "24 Pearson Pass",
      "work_tel": "5216430572",
      "mobile": "8817987436",
      "email": "mvedyaev2@hostgator.com",
      "namecard_img": "http://dummyimage.com/128x100.png/ff4444/ffffff",
      "external_id": "efO8jjqTqT"
    }, {
      "user_id": 1,
      "first_name": "Desiree",
      "last_name": "Wintle",
      "position": "Staff Accountant IV",
      "company_name": "Kertzmann, Rau and Kessler",
      "address": "39 Merrick Avenue",
      "work_tel": "9492120971",
      "mobile": "4869063346",
      "email": "dwintle3@digg.com",
      "namecard_img": "http://dummyimage.com/146x100.png/dddddd/000000",
      "external_id": "txpfvUmsw"
    }, {
      "user_id": 9,
      "first_name": "Karyn",
      "last_name": "Alsobrook",
      "position": "Help Desk Technician",
      "company_name": "McClure, Kris and Baumbach",
      "address": "43 Crowley Trail",
      "work_tel": "2391296911",
      "mobile": "4114761644",
      "email": "kalsobrook4@soundcloud.com",
      "namecard_img": "http://dummyimage.com/138x100.png/dddddd/000000",
      "external_id": "GNu6aVUiByq"
    }, {
      "user_id": 13,
      "first_name": "Alvira",
      "last_name": "O'Cullen",
      "position": "Database Administrator III",
      "company_name": "Considine, Witting and Baumbach",
      "address": "06942 Melody Drive",
      "work_tel": "6902838790",
      "mobile": "4753049176",
      "email": "aocullen5@aboutads.info",
      "namecard_img": "http://dummyimage.com/222x100.png/cc0000/ffffff",
      "external_id": "K0HzuHCym1b"
    }, {
      "user_id": 5,
      "first_name": "Wald",
      "last_name": "Simnel",
      "position": "VP Marketing",
      "company_name": "Nikolaus-Smith",
      "address": "00626 Oakridge Road",
      "work_tel": "2032408608",
      "mobile": "7593173987",
      "email": "wsimnel6@feedburner.com",
      "namecard_img": "http://dummyimage.com/219x100.png/dddddd/000000",
      "external_id": "GbNK5f"
    }, {
      "user_id": 2,
      "first_name": "Agatha",
      "last_name": "Sayce",
      "position": "Office Assistant IV",
      "company_name": "Greenholt, Langosh and Anderson",
      "address": "78 Bartelt Hill",
      "work_tel": "3697725839",
      "mobile": "7148020130",
      "email": "asayce7@xrea.com",
      "namecard_img": "http://dummyimage.com/215x100.png/ff4444/ffffff",
      "external_id": "EnUIJsd0a"
    }, {
      "user_id": 15,
      "first_name": "Walker",
      "last_name": "Klees",
      "position": "Staff Scientist",
      "company_name": "Windler-Powlowski",
      "address": "43 Grover Street",
      "work_tel": "1608784467",
      "mobile": "8316218899",
      "email": "wklees8@bloglovin.com",
      "namecard_img": "http://dummyimage.com/223x100.png/cc0000/ffffff",
      "external_id": "e3K71iw"
    }, {
      "user_id": 6,
      "first_name": "Thorin",
      "last_name": "Soppit",
      "position": "Senior Financial Analyst",
      "company_name": "Hahn, Schuster and Lowe",
      "address": "5808 Ramsey Road",
      "work_tel": "2647012734",
      "mobile": "7582289624",
      "email": "tsoppit9@thetimes.co.uk",
      "namecard_img": "http://dummyimage.com/186x100.png/ff4444/ffffff",
      "external_id": "f5ZhZ5W8K"
    }, {
      "user_id": 4,
      "first_name": "Kala",
      "last_name": "Polhill",
      "position": "Software Engineer II",
      "company_name": "McLaughlin, Stamm and Heathcote",
      "address": "48907 Bonner Parkway",
      "work_tel": "7341647723",
      "mobile": "5858266686",
      "email": "kpolhilla@yandex.ru",
      "namecard_img": "http://dummyimage.com/165x100.png/ff4444/ffffff",
      "external_id": "DjmcReaT0g"
    }, {
      "user_id": 9,
      "first_name": "Brianne",
      "last_name": "Rivilis",
      "position": "Sales Representative",
      "company_name": "Sawayn Inc",
      "address": "099 Hovde Way",
      "work_tel": "8299617951",
      "mobile": "5566127212",
      "email": "brivilisb@uiuc.edu",
      "namecard_img": "http://dummyimage.com/227x100.png/ff4444/ffffff",
      "external_id": "LMSYVWPlQ"
    }, {
      "user_id": 3,
      "first_name": "Peyton",
      "last_name": "Pryde",
      "position": "Biostatistician III",
      "company_name": "Shanahan-Ankunding",
      "address": "2017 Lotheville Street",
      "work_tel": "3725952706",
      "mobile": "7434662275",
      "email": "pprydec@huffingtonpost.com",
      "namecard_img": "http://dummyimage.com/233x100.png/ff4444/ffffff",
      "external_id": "v29XL9PdM"
    }, {
      "user_id": 7,
      "first_name": "Elysee",
      "last_name": "Stewartson",
      "position": "Clinical Specialist",
      "company_name": "Dach LLC",
      "address": "12467 Ruskin Alley",
      "work_tel": "4457453516",
      "mobile": "3232889430",
      "email": "estewartsond@un.org",
      "namecard_img": "http://dummyimage.com/202x100.png/ff4444/ffffff",
      "external_id": "zElVqXIHfI"
    }, {
      "user_id": 14,
      "first_name": "Tiler",
      "last_name": "Devonshire",
      "position": "Professor",
      "company_name": "Doyle, Torp and Hintz",
      "address": "354 Sommers Junction",
      "work_tel": "3781459229",
      "mobile": "5691262375",
      "email": "tdevonshiree@nationalgeographic.com",
      "namecard_img": "http://dummyimage.com/221x100.png/ff4444/ffffff",
      "external_id": "FomLmS3hxHFB"
    }, {
      "user_id": 9,
      "first_name": "Cortney",
      "last_name": "Bernard",
      "position": "Nurse Practicioner",
      "company_name": "Kiehn Group",
      "address": "06 Sundown Crossing",
      "work_tel": "7921366345",
      "mobile": "9593783102",
      "email": "cbernardf@hc360.com",
      "namecard_img": "http://dummyimage.com/196x100.png/5fa2dd/ffffff",
      "external_id": "cngbIOoGjEY8"
    }, {
      "user_id": 4,
      "first_name": "Sammie",
      "last_name": "Copnall",
      "position": "Nurse",
      "company_name": "Hansen Inc",
      "address": "373 Vermont Junction",
      "work_tel": "6231286349",
      "mobile": "2631323047",
      "email": "scopnallg@geocities.com",
      "namecard_img": "http://dummyimage.com/164x100.png/5fa2dd/ffffff",
      "external_id": "01M1Wm"
    }, {
      "user_id": 6,
      "first_name": "Bell",
      "last_name": "Bow",
      "position": "Sales Associate",
      "company_name": "Reilly, Dach and Mertz",
      "address": "7 Sherman Trail",
      "work_tel": "6941433584",
      "mobile": "8438936345",
      "email": "bbowh@whitehouse.gov",
      "namecard_img": "http://dummyimage.com/194x100.png/ff4444/ffffff",
      "external_id": "bhfRzB9pbd"
    }, {
      "user_id": 8,
      "first_name": "Gorden",
      "last_name": "Zotto",
      "position": "Desktop Support Technician",
      "company_name": "Ziemann Group",
      "address": "21 Sloan Pass",
      "work_tel": "4498245911",
      "mobile": "4477287516",
      "email": "gzottoi@aol.com",
      "namecard_img": "http://dummyimage.com/212x100.png/ff4444/ffffff",
      "external_id": "NqZm74Qef"
    }, {
      "user_id": 4,
      "first_name": "Magdalene",
      "last_name": "Padilla",
      "position": "Assistant Professor",
      "company_name": "Cummings Inc",
      "address": "70525 Hoepker Crossing",
      "work_tel": "7205846425",
      "mobile": "8658340741",
      "email": "mpadillaj@cafepress.com",
      "namecard_img": "http://dummyimage.com/179x100.png/5fa2dd/ffffff",
      "external_id": "VeeqFfd"
    }, {
      "user_id": 10,
      "first_name": "Sofie",
      "last_name": "Adamek",
      "position": "Nurse Practicioner",
      "company_name": "Leffler-Hills",
      "address": "7242 Arrowood Place",
      "work_tel": "5606245316",
      "mobile": "8129766037",
      "email": "sadamekk@va.gov",
      "namecard_img": "http://dummyimage.com/172x100.png/ff4444/ffffff",
      "external_id": "mgYl3ShOmKF"
    }, {
      "user_id": 3,
      "first_name": "Verney",
      "last_name": "Luff",
      "position": "General Manager",
      "company_name": "Wyman-Boehm",
      "address": "8 Fremont Plaza",
      "work_tel": "1475540818",
      "mobile": "4305123346",
      "email": "vluffl@pcworld.com",
      "namecard_img": "http://dummyimage.com/163x100.png/ff4444/ffffff",
      "external_id": "0raNYC9DGr"
    }, {
      "user_id": 3,
      "first_name": "Selby",
      "last_name": "Blodget",
      "position": "VP Product Management",
      "company_name": "Collins-Grant",
      "address": "821 Golf View Junction",
      "work_tel": "7111979713",
      "mobile": "9104102334",
      "email": "sblodgetm@rakuten.co.jp",
      "namecard_img": "http://dummyimage.com/126x100.png/dddddd/000000",
      "external_id": "ESM1h6Y68Nfk"
    }, {
      "user_id": 3,
      "first_name": "Babbette",
      "last_name": "Stubbe",
      "position": "Nurse",
      "company_name": "Lindgren-Leffler",
      "address": "694 Randy Trail",
      "work_tel": "8254967395",
      "mobile": "3793569139",
      "email": "bstubben@patch.com",
      "namecard_img": "http://dummyimage.com/152x100.png/5fa2dd/ffffff",
      "external_id": "b3s5w5mRs6is"
    }, {
      "user_id": 2,
      "first_name": "Marjy",
      "last_name": "Burgisi",
      "position": "Compensation Analyst",
      "company_name": "Watsica, Bartell and Rath",
      "address": "079 Arrowood Court",
      "work_tel": "3295967963",
      "mobile": "9032851494",
      "email": "mburgisio@tiny.cc",
      "namecard_img": "http://dummyimage.com/214x100.png/5fa2dd/ffffff",
      "external_id": "H7g0DveE2"
    }, {
      "user_id": 10,
      "first_name": "Humfrid",
      "last_name": "Flowers",
      "position": "Senior Developer",
      "company_name": "Brown, Romaguera and Bradtke",
      "address": "2 Lindbergh Street",
      "work_tel": "6902714868",
      "mobile": "6177433158",
      "email": "hflowersp@whitehouse.gov",
      "namecard_img": "http://dummyimage.com/186x100.png/5fa2dd/ffffff",
      "external_id": "p7G5m3Q"
    }, {
      "user_id": 6,
      "first_name": "Egon",
      "last_name": "Roskell",
      "position": "Senior Sales Associate",
      "company_name": "Muller LLC",
      "address": "55 Badeau Court",
      "work_tel": "4677840815",
      "mobile": "6557387694",
      "email": "eroskellq@networkadvertising.org",
      "namecard_img": "http://dummyimage.com/113x100.png/ff4444/ffffff",
      "external_id": "iH1U4C22"
    }, {
      "user_id": 7,
      "first_name": "Berna",
      "last_name": "Halson",
      "position": "Community Outreach Specialist",
      "company_name": "Feil Inc",
      "address": "0183 Iowa Trail",
      "work_tel": "8246486073",
      "mobile": "5145909006",
      "email": "bhalsonr@ask.com",
      "namecard_img": "http://dummyimage.com/201x100.png/dddddd/000000",
      "external_id": "dPSAg55GmcOV"
    }, {
      "user_id": 15,
      "first_name": "Arline",
      "last_name": "Tollemache",
      "position": "VP Accounting",
      "company_name": "Abbott, Simonis and Funk",
      "address": "1071 Shopko Road",
      "work_tel": "7703789899",
      "mobile": "8221628592",
      "email": "atollemaches@skype.com",
      "namecard_img": "http://dummyimage.com/127x100.png/cc0000/ffffff",
      "external_id": "dx2sIHQF4s"
    }, {
      "user_id": 3,
      "first_name": "Atlante",
      "last_name": "Tout",
      "position": "Account Coordinator",
      "company_name": "Murphy-Johnston",
      "address": "62 Troy Road",
      "work_tel": "3237650129",
      "mobile": "3523200032",
      "email": "atoutt@mysql.com",
      "namecard_img": "http://dummyimage.com/121x100.png/ff4444/ffffff",
      "external_id": "iNGsoa7JZgf"
    }, {
      "user_id": 15,
      "first_name": "Shepard",
      "last_name": "Shalcras",
      "position": "Sales Representative",
      "company_name": "Schoen Group",
      "address": "42131 Spohn Center",
      "work_tel": "2422396362",
      "mobile": "1947530556",
      "email": "sshalcrasu@imageshack.us",
      "namecard_img": "http://dummyimage.com/212x100.png/dddddd/000000",
      "external_id": "5UAhRJzt"
    }, {
      "user_id": 2,
      "first_name": "Melloney",
      "last_name": "Tandy",
      "position": "Structural Analysis Engineer",
      "company_name": "Oberbrunner-Lueilwitz",
      "address": "8730 Prentice Lane",
      "work_tel": "2629805272",
      "mobile": "3943273681",
      "email": "mtandyv@godaddy.com",
      "namecard_img": "http://dummyimage.com/181x100.png/cc0000/ffffff",
      "external_id": "QNLcbc4"
    }, {
      "user_id": 1,
      "first_name": "Vicky",
      "last_name": "Noriega",
      "position": "Chief Design Engineer",
      "company_name": "Schuppe Group",
      "address": "64979 Esker Park",
      "work_tel": "8827421340",
      "mobile": "9724845669",
      "email": "vnoriegaw@pbs.org",
      "namecard_img": "http://dummyimage.com/111x100.png/cc0000/ffffff",
      "external_id": "7FkMPud"
    }, {
      "user_id": 1,
      "first_name": "Cirilo",
      "last_name": "Deering",
      "position": "VP Quality Control",
      "company_name": "Grady, Stroman and Dicki",
      "address": "37 Green Way",
      "work_tel": "7362517497",
      "mobile": "6849690286",
      "email": "cdeeringx@delicious.com",
      "namecard_img": "http://dummyimage.com/147x100.png/5fa2dd/ffffff",
      "external_id": "wDdBO7"
    }, {
      "user_id": 2,
      "first_name": "Inge",
      "last_name": "Sugars",
      "position": "Compensation Analyst",
      "company_name": "Spinka-Franecki",
      "address": "74598 Towne Center",
      "work_tel": "3165926083",
      "mobile": "3899431801",
      "email": "isugarsy@addtoany.com",
      "namecard_img": "http://dummyimage.com/216x100.png/ff4444/ffffff",
      "external_id": "7qqvYb"
    }, {
      "user_id": 10,
      "first_name": "Lucienne",
      "last_name": "Cuschieri",
      "position": "Analog Circuit Design manager",
      "company_name": "Schumm, King and Olson",
      "address": "53 Clyde Gallagher Place",
      "work_tel": "4963751277",
      "mobile": "8968190314",
      "email": "lcuschieriz@jiathis.com",
      "namecard_img": "http://dummyimage.com/109x100.png/cc0000/ffffff",
      "external_id": "JM4yJk8X"
    }, {
      "user_id": 6,
      "first_name": "Odey",
      "last_name": "Megson",
      "position": "Dental Hygienist",
      "company_name": "VonRueden-Lehner",
      "address": "11 Transport Hill",
      "work_tel": "5554445199",
      "mobile": "1265421430",
      "email": "omegson10@hao123.com",
      "namecard_img": "http://dummyimage.com/227x100.png/dddddd/000000",
      "external_id": "4vTCD8QdF2"
    }, {
      "user_id": 11,
      "first_name": "Davis",
      "last_name": "Morman",
      "position": "Project Manager",
      "company_name": "Schowalter-Wuckert",
      "address": "34481 Eagle Crest Court",
      "work_tel": "7082540501",
      "mobile": "6824036060",
      "email": "dmorman11@php.net",
      "namecard_img": "http://dummyimage.com/102x100.png/ff4444/ffffff",
      "external_id": "tozCiyPgU"
    }, {
      "user_id": 10,
      "first_name": "Eugen",
      "last_name": "Lowing",
      "position": "Tax Accountant",
      "company_name": "Kuhlman LLC",
      "address": "58749 Cottonwood Point",
      "work_tel": "7211653948",
      "mobile": "3273318591",
      "email": "elowing12@blogtalkradio.com",
      "namecard_img": "http://dummyimage.com/159x100.png/dddddd/000000",
      "external_id": "PE5vZJyl4"
    }, {
      "user_id": 10,
      "first_name": "Shelton",
      "last_name": "Earngy",
      "position": "Software Consultant",
      "company_name": "Flatley LLC",
      "address": "420 Corry Alley",
      "work_tel": "9451301801",
      "mobile": "2986186368",
      "email": "searngy13@gravatar.com",
      "namecard_img": "http://dummyimage.com/155x100.png/cc0000/ffffff",
      "external_id": "Cv2BihVb"
    }, {
      "user_id": 4,
      "first_name": "Cara",
      "last_name": "Atty",
      "position": "Systems Administrator III",
      "company_name": "Kunde Group",
      "address": "0 Harper Alley",
      "work_tel": "7746559741",
      "mobile": "5857979050",
      "email": "catty14@dagondesign.com",
      "namecard_img": "http://dummyimage.com/154x100.png/5fa2dd/ffffff",
      "external_id": "UuVji0FBq"
    }, {
      "user_id": 1,
      "first_name": "Sioux",
      "last_name": "Poller",
      "position": "Executive Secretary",
      "company_name": "Considine Inc",
      "address": "19469 Corscot Trail",
      "work_tel": "2456208258",
      "mobile": "1161051246",
      "email": "spoller15@vinaora.com",
      "namecard_img": "http://dummyimage.com/235x100.png/cc0000/ffffff",
      "external_id": "Xi1sfZz2a4Ts"
    }, {
      "user_id": 4,
      "first_name": "Bendicty",
      "last_name": "Simounet",
      "position": "Financial Advisor",
      "company_name": "Bartoletti and Sons",
      "address": "28610 Boyd Avenue",
      "work_tel": "8423971753",
      "mobile": "2881184982",
      "email": "bsimounet16@abc.net.au",
      "namecard_img": "http://dummyimage.com/144x100.png/5fa2dd/ffffff",
      "external_id": "fp1sVW"
    }, {
      "user_id": 1,
      "first_name": "Emilio",
      "last_name": "Grishanin",
      "position": "Social Worker",
      "company_name": "Fadel-Maggio",
      "address": "2 Clemons Point",
      "work_tel": "6128538389",
      "mobile": "4693294908",
      "email": "egrishanin17@cafepress.com",
      "namecard_img": "http://dummyimage.com/125x100.png/dddddd/000000",
      "external_id": "TnR4mTkxpV"
    }, {
      "user_id": 15,
      "first_name": "Walliw",
      "last_name": "Lomath",
      "position": "Account Coordinator",
      "company_name": "Wilkinson, Blick and Bins",
      "address": "71 Alpine Pass",
      "work_tel": "5934783153",
      "mobile": "9513139930",
      "email": "wlomath18@people.com.cn",
      "namecard_img": "http://dummyimage.com/227x100.png/cc0000/ffffff",
      "external_id": "myG7xMv7a2I"
    }]);
    
    await knex("category").insert([{
        "user_id": 11,
        "category_name": "Major Pharmaceuticals"
      }, {
        "user_id": 6,
        "category_name": "Life Insurance"
      }, {
        "user_id": 5,
        "category_name": "Telecommunications Equipment"
      }, {
        "user_id": 10,
        "category_name": "n/a"
      }, {
        "user_id": 1,
        "category_name": "Medical Specialities"
      }, {
        "user_id": 6,
        "category_name": "n/a"
      }, {
        "user_id": 3,
        "category_name": "Precious Metals"
      }, {
        "user_id": 11,
        "category_name": "Electric Utilities: Central"
      }, {
        "user_id": 11,
        "category_name": "Major Pharmaceuticals"
      }, {
        "user_id": 9,
        "category_name": "n/a"
      }, {
        "user_id": 9,
        "category_name": "n/a"
      }, {
        "user_id": 1,
        "category_name": "Precious Metals"
      }, {
        "user_id": 4,
        "category_name": "Medical Specialities"
      }, {
        "user_id": 7,
        "category_name": "n/a"
      }, {
        "user_id": 15,
        "category_name": "Radio And Television Broadcasting And Communications Equipment"
      }, {
        "user_id": 10,
        "category_name": "Precious Metals"
      }, {
        "user_id": 14,
        "category_name": "Building Products"
      }, {
        "user_id": 10,
        "category_name": "Oil & Gas Production"
      }, {
        "user_id": 8,
        "category_name": "Major Pharmaceuticals"
      }, {
        "user_id": 7,
        "category_name": "RETAIL: Building Materials"
      }, {
        "user_id": 3,
        "category_name": "Business Services"
      }, {
        "user_id": 7,
        "category_name": "Biotechnology: Electromedical & Electrotherapeutic Apparatus"
      }, {
        "user_id": 5,
        "category_name": "Steel/Iron Ore"
      }, {
        "user_id": 7,
        "category_name": "Major Chemicals"
      }, {
        "user_id": 5,
        "category_name": "Major Banks"
      }, {
        "user_id": 1,
        "category_name": "Food Distributors"
      }, {
        "user_id": 9,
        "category_name": "n/a"
      }, {
        "user_id": 11,
        "category_name": "Biotechnology: Electromedical & Electrotherapeutic Apparatus"
      }, {
        "user_id": 1,
        "category_name": "Major Chemicals"
      }, {
        "user_id": 12,
        "category_name": "Major Pharmaceuticals"
      }]);

    await knex("member_relationship").insert([{
        "user_id": 6,
        "namecard_id": 29,
        "category_id": 26
      }, {
        "user_id": 15,
        "namecard_id": 44,
        "category_id": 8
      }, {
        "user_id": 12,
        "namecard_id": 39,
        "category_id": 27
      }, {
        "user_id": 14,
        "namecard_id": 33,
        "category_id": 27
      }, {
        "user_id": 3,
        "namecard_id": 43,
        "category_id": 26
      }, {
        "user_id": 10,
        "namecard_id": 26,
        "category_id": 6
      }, {
        "user_id": 7,
        "namecard_id": 1,
        "category_id": 1
      }, {
        "user_id": 5,
        "namecard_id": 29,
        "category_id": 8
      }, {
        "user_id": 4,
        "namecard_id": 2,
        "category_id": 19
      }, {
        "user_id": 2,
        "namecard_id": 42,
        "category_id": 7
      }, {
        "user_id": 10,
        "namecard_id": 34,
        "category_id": 22
      }, {
        "user_id": 7,
        "namecard_id": 5,
        "category_id": 7
      }, {
        "user_id": 8,
        "namecard_id": 44,
        "category_id": 15
      }, {
        "user_id": 12,
        "namecard_id": 1,
        "category_id": 18
      }, {
        "user_id": 2,
        "namecard_id": 12,
        "category_id": 22
      }, {
        "user_id": 12,
        "namecard_id": 15,
        "category_id": 15
      }, {
        "user_id": 15,
        "namecard_id": 7,
        "category_id": 19
      }, {
        "user_id": 6,
        "namecard_id": 43,
        "category_id": 14
      }, {
        "user_id": 14,
        "namecard_id": 20,
        "category_id": 24
      }, {
        "user_id": 7,
        "namecard_id": 7,
        "category_id": 2
      }, {
        "user_id": 5,
        "namecard_id": 25,
        "category_id": 19
      }, {
        "user_id": 9,
        "namecard_id": 29,
        "category_id": 10
      }, {
        "user_id": 4,
        "namecard_id": 31,
        "category_id": 4
      }, {
        "user_id": 7,
        "namecard_id": 26,
        "category_id": 29
      }, {
        "user_id": 2,
        "namecard_id": 34,
        "category_id": 24
      }, {
        "user_id": 14,
        "namecard_id": 30,
        "category_id": 10
      }, {
        "user_id": 2,
        "namecard_id": 19,
        "category_id": 17
      }, {
        "user_id": 4,
        "namecard_id": 21,
        "category_id": 8
      }, {
        "user_id": 1,
        "namecard_id": 39,
        "category_id": 15
      }, {
        "user_id": 4,
        "namecard_id": 6,
        "category_id": 4
      }]);

    await knex("non_member_card").insert([{
      "user_id": 5,
      "first_name": "Willis",
      "last_name": "Colman",
      "position": "Chief Design Engineer",
      "company_name": "Gerlach-Kshlerin",
      "address": "389 Kingsford Plaza",
      "work_tel": "8222922799",
      "mobile": "5093244684",
      "email": "wcolman0@dedecms.com",
      "namecard_img": "http://dummyimage.com/245x100.png/ff4444/ffffff",
      "category_id": 5,
      "external_id": "iCrfVYn4Qc"
    }, {
      "user_id": 4,
      "first_name": "Datha",
      "last_name": "Strond",
      "position": "Recruiting Manager",
      "company_name": "Thompson, Gutmann and Morissette",
      "address": "28834 Morrow Place",
      "work_tel": "6204984867",
      "mobile": "4629267194",
      "email": "dstrond1@pinterest.com",
      "namecard_img": "http://dummyimage.com/180x100.png/5fa2dd/ffffff",
      "category_id": 8,
      "external_id": "2iWsiP3"
    }, {
      "user_id": 10,
      "first_name": "Joelly",
      "last_name": "Cope",
      "position": "General Manager",
      "company_name": "Daugherty LLC",
      "address": "5 Lighthouse Bay Court",
      "work_tel": "6586282267",
      "mobile": "1019695027",
      "email": "jcope2@uiuc.edu",
      "namecard_img": "http://dummyimage.com/158x100.png/5fa2dd/ffffff",
      "category_id": 10,
      "external_id": "F17QjFPdUuv"
    }, {
      "user_id": 13,
      "first_name": "Fonz",
      "last_name": "Dils",
      "position": "Actuary",
      "company_name": "Haag-Okuneva",
      "address": "5 Stang Court",
      "work_tel": "8331488923",
      "mobile": "6991741588",
      "email": "fdils3@jigsy.com",
      "namecard_img": "http://dummyimage.com/128x100.png/5fa2dd/ffffff",
      "category_id": 4,
      "external_id": "4TnabTNN1w"
    }, {
      "user_id": 12,
      "first_name": "Lou",
      "last_name": "O' Driscoll",
      "position": "GIS Technical Architect",
      "company_name": "Lowe Inc",
      "address": "861 Ilene Way",
      "work_tel": "8476024915",
      "mobile": "7708937983",
      "email": "lodriscoll4@wp.com",
      "namecard_img": "http://dummyimage.com/175x100.png/dddddd/000000",
      "category_id": 8,
      "external_id": "63H935GqC"
    }, {
      "user_id": 11,
      "first_name": "Davide",
      "last_name": "Baldam",
      "position": "VP Sales",
      "company_name": "Anderson-Heidenreich",
      "address": "08 Valley Edge Junction",
      "work_tel": "3435236948",
      "mobile": "3149365196",
      "email": "dbaldam5@sphinn.com",
      "namecard_img": "http://dummyimage.com/197x100.png/cc0000/ffffff",
      "category_id": 10,
      "external_id": "IAIRyguZSu"
    }, {
      "user_id": 6,
      "first_name": "Lelah",
      "last_name": "Pescott",
      "position": "Environmental Tech",
      "company_name": "Gusikowski, Kuvalis and Marks",
      "address": "505 Sugar Terrace",
      "work_tel": "2424913959",
      "mobile": "9772953390",
      "email": "lpescott6@ustream.tv",
      "namecard_img": "http://dummyimage.com/136x100.png/5fa2dd/ffffff",
      "category_id": 10,
      "external_id": "Gbu4BdJlCvTq"
    }, {
      "user_id": 6,
      "first_name": "Agustin",
      "last_name": "Jerosch",
      "position": "Graphic Designer",
      "company_name": "O'Keefe Inc",
      "address": "570 Atwood Avenue",
      "work_tel": "4416508417",
      "mobile": "1071201484",
      "email": "ajerosch7@google.ca",
      "namecard_img": "http://dummyimage.com/195x100.png/dddddd/000000",
      "category_id": 2,
      "external_id": "dw9NhPgw"
    }, {
      "user_id": 11,
      "first_name": "Grete",
      "last_name": "Izkovitz",
      "position": "VP Quality Control",
      "company_name": "Beer-Jones",
      "address": "93360 Glacier Hill Drive",
      "work_tel": "2474509607",
      "mobile": "3463382124",
      "email": "gizkovitz8@bloglines.com",
      "namecard_img": "http://dummyimage.com/164x100.png/dddddd/000000",
      "category_id": 1,
      "external_id": "RbQQsj"
    }, {
      "user_id": 11,
      "first_name": "Carl",
      "last_name": "Edinboro",
      "position": "Tax Accountant",
      "company_name": "Crooks, Thompson and Kunde",
      "address": "80969 Russell Junction",
      "work_tel": "9971327408",
      "mobile": "6396882066",
      "email": "cedinboro9@elpais.com",
      "namecard_img": "http://dummyimage.com/109x100.png/ff4444/ffffff",
      "category_id": 10,
      "external_id": "ydYajK5Bk83"
    }, {
      "user_id": 13,
      "first_name": "Flossy",
      "last_name": "Strodder",
      "position": "Recruiter",
      "company_name": "Hyatt-Berge",
      "address": "3 Monica Circle",
      "work_tel": "6905533069",
      "mobile": "3607659412",
      "email": "fstroddera@usa.gov",
      "namecard_img": "http://dummyimage.com/243x100.png/5fa2dd/ffffff",
      "category_id": 2,
      "external_id": "hc0GVlPLN2L"
    }, {
      "user_id": 5,
      "first_name": "Dana",
      "last_name": "Funcheon",
      "position": "Analog Circuit Design manager",
      "company_name": "Stiedemann, Krajcik and Kling",
      "address": "17646 Rieder Street",
      "work_tel": "3297940331",
      "mobile": "2578084557",
      "email": "dfuncheonb@123-reg.co.uk",
      "namecard_img": "http://dummyimage.com/130x100.png/5fa2dd/ffffff",
      "category_id": 9,
      "external_id": "NP75FZqCY"
    }, {
      "user_id": 3,
      "first_name": "Donaugh",
      "last_name": "Lodevick",
      "position": "Paralegal",
      "company_name": "Waters-Vandervort",
      "address": "873 Clemons Place",
      "work_tel": "9936088083",
      "mobile": "4127166193",
      "email": "dlodevickc@ucla.edu",
      "namecard_img": "http://dummyimage.com/139x100.png/ff4444/ffffff",
      "category_id": 2,
      "external_id": "5shr9iP9gRvI"
    }, {
      "user_id": 7,
      "first_name": "Lind",
      "last_name": "Matteuzzi",
      "position": "Librarian",
      "company_name": "O'Keefe-Weimann",
      "address": "44770 Montana Junction",
      "work_tel": "3077576673",
      "mobile": "7284857278",
      "email": "lmatteuzzid@arstechnica.com",
      "namecard_img": "http://dummyimage.com/102x100.png/dddddd/000000",
      "category_id": 7,
      "external_id": "3oD9kL"
    }, {
      "user_id": 9,
      "first_name": "Nicolina",
      "last_name": "Dinneen",
      "position": "Administrative Officer",
      "company_name": "Bernhard and Sons",
      "address": "4830 Lotheville Plaza",
      "work_tel": "7627427021",
      "mobile": "6433118684",
      "email": "ndinneene@illinois.edu",
      "namecard_img": "http://dummyimage.com/124x100.png/cc0000/ffffff",
      "category_id": 1,
      "external_id": "bKEAdUD"
    }, {
      "user_id": 3,
      "first_name": "Mychal",
      "last_name": "Mulcaster",
      "position": "Librarian",
      "company_name": "Block, Denesik and Stanton",
      "address": "46551 Browning Hill",
      "work_tel": "4077485087",
      "mobile": "9507863963",
      "email": "mmulcasterf@deviantart.com",
      "namecard_img": "http://dummyimage.com/213x100.png/dddddd/000000",
      "category_id": 9,
      "external_id": "oG0W62EZ7"
    }, {
      "user_id": 5,
      "first_name": "Kelly",
      "last_name": "Upchurch",
      "position": "VP Marketing",
      "company_name": "Waelchi, Bechtelar and Lindgren",
      "address": "298 Springview Circle",
      "work_tel": "7665373099",
      "mobile": "2885733834",
      "email": "kupchurchg@oaic.gov.au",
      "namecard_img": "http://dummyimage.com/160x100.png/ff4444/ffffff",
      "category_id": 7,
      "external_id": "rb9oltMfB"
    }, {
      "user_id": 10,
      "first_name": "Brigg",
      "last_name": "Adamek",
      "position": "Web Designer IV",
      "company_name": "Boyle Group",
      "address": "56469 Waxwing Way",
      "work_tel": "1955169425",
      "mobile": "5727145726",
      "email": "badamekh@etsy.com",
      "namecard_img": "http://dummyimage.com/168x100.png/cc0000/ffffff",
      "category_id": 3,
      "external_id": "T3GBXuqmLd"
    }, {
      "user_id": 5,
      "first_name": "Deirdre",
      "last_name": "Reuble",
      "position": "Programmer Analyst I",
      "company_name": "Kautzer-Leffler",
      "address": "75 Becker Drive",
      "work_tel": "6751925111",
      "mobile": "8461532700",
      "email": "dreublei@amazon.co.uk",
      "namecard_img": "http://dummyimage.com/157x100.png/5fa2dd/ffffff",
      "category_id": 2,
      "external_id": "DyYHqX"
    }, {
      "user_id": 7,
      "first_name": "Jen",
      "last_name": "Sheepy",
      "position": "Chemical Engineer",
      "company_name": "Kub Group",
      "address": "55 Stang Terrace",
      "work_tel": "2891608112",
      "mobile": "7668394138",
      "email": "jsheepyj@bbc.co.uk",
      "namecard_img": "http://dummyimage.com/126x100.png/5fa2dd/ffffff",
      "category_id": 5,
      "external_id": "nA3jiKaV"
    }, {
      "user_id": 2,
      "first_name": "Ainslee",
      "last_name": "Wolffers",
      "position": "GIS Technical Architect",
      "company_name": "Donnelly-Pfeffer",
      "address": "6446 Golf Course Trail",
      "work_tel": "9195520940",
      "mobile": "6332934345",
      "email": "awolffersk@newsvine.com",
      "namecard_img": "http://dummyimage.com/248x100.png/ff4444/ffffff",
      "category_id": 4,
      "external_id": "Ip3osCA"
    }, {
      "user_id": 9,
      "first_name": "Gery",
      "last_name": "Corington",
      "position": "Computer Systems Analyst III",
      "company_name": "Kihn Inc",
      "address": "17 Delladonna Drive",
      "work_tel": "8657226440",
      "mobile": "9378811566",
      "email": "gcoringtonl@bizjournals.com",
      "namecard_img": "http://dummyimage.com/141x100.png/cc0000/ffffff",
      "category_id": 8,
      "external_id": "ymYR3BYER"
    }, {
      "user_id": 1,
      "first_name": "Konstantine",
      "last_name": "Banker",
      "position": "Senior Financial Analyst",
      "company_name": "Keebler Inc",
      "address": "1 Nelson Plaza",
      "work_tel": "3187744800",
      "mobile": "6148309373",
      "email": "kbankerm@engadget.com",
      "namecard_img": "http://dummyimage.com/247x100.png/cc0000/ffffff",
      "category_id": 4,
      "external_id": "BLNS9zE"
    }, {
      "user_id": 11,
      "first_name": "Joellyn",
      "last_name": "Schuler",
      "position": "VP Marketing",
      "company_name": "Sauer, Nader and Rippin",
      "address": "9998 Sunfield Junction",
      "work_tel": "6529160705",
      "mobile": "1457653405",
      "email": "jschulern@blogspot.com",
      "namecard_img": "http://dummyimage.com/131x100.png/dddddd/000000",
      "category_id": 1,
      "external_id": "92QspzV"
    }, {
      "user_id": 14,
      "first_name": "Rosalie",
      "last_name": "Durkin",
      "position": "Information Systems Manager",
      "company_name": "Bechtelar Inc",
      "address": "672 Bonner Alley",
      "work_tel": "4871164739",
      "mobile": "8296781081",
      "email": "rdurkino@bloglovin.com",
      "namecard_img": "http://dummyimage.com/229x100.png/5fa2dd/ffffff",
      "category_id": 1,
      "external_id": "oLdfiTdOiTzs"
    }, {
      "user_id": 15,
      "first_name": "Clementia",
      "last_name": "Engley",
      "position": "Software Test Engineer IV",
      "company_name": "Witting Inc",
      "address": "5609 Monterey Road",
      "work_tel": "4568894935",
      "mobile": "8439198120",
      "email": "cengleyp@uol.com.br",
      "namecard_img": "http://dummyimage.com/169x100.png/dddddd/000000",
      "category_id": 10,
      "external_id": "NzONk1G"
    }, {
      "user_id": 13,
      "first_name": "Kamilah",
      "last_name": "Harbert",
      "position": "Tax Accountant",
      "company_name": "Hickle Group",
      "address": "97 Truax Road",
      "work_tel": "4778715570",
      "mobile": "1716128413",
      "email": "kharbertq@odnoklassniki.ru",
      "namecard_img": "http://dummyimage.com/179x100.png/ff4444/ffffff",
      "category_id": 10,
      "external_id": "nJVLUKQBC1"
    }, {
      "user_id": 7,
      "first_name": "Bondon",
      "last_name": "Manie",
      "position": "Statistician II",
      "company_name": "Hyatt-Hagenes",
      "address": "9997 Green Ridge Pass",
      "work_tel": "3594713810",
      "mobile": "2662298520",
      "email": "bmanier@mozilla.org",
      "namecard_img": "http://dummyimage.com/249x100.png/cc0000/ffffff",
      "category_id": 2,
      "external_id": "C3snuI"
    }, {
      "user_id": 3,
      "first_name": "Hermine",
      "last_name": "Schiesterl",
      "position": "Senior Quality Engineer",
      "company_name": "Streich Group",
      "address": "51675 Blue Bill Park Hill",
      "work_tel": "9147271995",
      "mobile": "3054255505",
      "email": "hschiesterls@japanpost.jp",
      "namecard_img": "http://dummyimage.com/219x100.png/ff4444/ffffff",
      "category_id": 6,
      "external_id": "MuPxQjk"
    }, {
      "user_id": 11,
      "first_name": "Rani",
      "last_name": "Henstridge",
      "position": "Statistician IV",
      "company_name": "Bechtelar, Anderson and Swift",
      "address": "09945 Acker Crossing",
      "work_tel": "1315585773",
      "mobile": "2184674482",
      "email": "rhenstridget@google.de",
      "namecard_img": "http://dummyimage.com/133x100.png/ff4444/ffffff",
      "category_id": 4,
      "external_id": "rRFiIem"
    }, {
      "user_id": 10,
      "first_name": "Tallou",
      "last_name": "Meekings",
      "position": "Administrative Officer",
      "company_name": "Beahan LLC",
      "address": "540 Maryland Way",
      "work_tel": "8713546122",
      "mobile": "3203807138",
      "email": "tmeekingsu@gravatar.com",
      "namecard_img": "http://dummyimage.com/241x100.png/dddddd/000000",
      "category_id": 1,
      "external_id": "fL5Lq6T0kjy"
    }, {
      "user_id": 8,
      "first_name": "Malena",
      "last_name": "McCunn",
      "position": "Geologist II",
      "company_name": "Cummings, Reilly and Simonis",
      "address": "19 Pawling Center",
      "work_tel": "3506455650",
      "mobile": "7552204953",
      "email": "mmccunnv@businessinsider.com",
      "namecard_img": "http://dummyimage.com/151x100.png/5fa2dd/ffffff",
      "category_id": 9,
      "external_id": "hyjRefew0vM4"
    }, {
      "user_id": 15,
      "first_name": "Lola",
      "last_name": "Hallum",
      "position": "Quality Engineer",
      "company_name": "Murphy, Boehm and Gleason",
      "address": "9446 Harbort Trail",
      "work_tel": "4369951807",
      "mobile": "1272664753",
      "email": "lhallumw@pen.io",
      "namecard_img": "http://dummyimage.com/214x100.png/cc0000/ffffff",
      "category_id": 2,
      "external_id": "mRIEmbd8rEc3"
    }, {
      "user_id": 12,
      "first_name": "Shanan",
      "last_name": "Elsop",
      "position": "Structural Engineer",
      "company_name": "Reichert-Abernathy",
      "address": "88 Badeau Alley",
      "work_tel": "3343019687",
      "mobile": "5846148969",
      "email": "selsopx@cbsnews.com",
      "namecard_img": "http://dummyimage.com/135x100.png/ff4444/ffffff",
      "category_id": 10,
      "external_id": "BndbqR0"
    }, {
      "user_id": 1,
      "first_name": "Belicia",
      "last_name": "Strewther",
      "position": "Human Resources Assistant I",
      "company_name": "Hoppe, Botsford and Harris",
      "address": "6261 Sachs Circle",
      "work_tel": "1187645478",
      "mobile": "2207820267",
      "email": "bstrewthery@mlb.com",
      "namecard_img": "http://dummyimage.com/134x100.png/ff4444/ffffff",
      "category_id": 1,
      "external_id": "8UPiQZVWXhe"
    }, {
      "user_id": 3,
      "first_name": "Marys",
      "last_name": "Culpan",
      "position": "Engineer I",
      "company_name": "Roberts-Jacobi",
      "address": "8475 Meadow Ridge Hill",
      "work_tel": "9983875442",
      "mobile": "5539518939",
      "email": "mculpanz@delicious.com",
      "namecard_img": "http://dummyimage.com/179x100.png/cc0000/ffffff",
      "category_id": 1,
      "external_id": "hoUPTDczA"
    }, {
      "user_id": 13,
      "first_name": "Arda",
      "last_name": "Longley",
      "position": "VP Marketing",
      "company_name": "Stracke, Cole and Turcotte",
      "address": "417 Macpherson Hill",
      "work_tel": "1272108058",
      "mobile": "7638605919",
      "email": "alongley10@cnn.com",
      "namecard_img": "http://dummyimage.com/196x100.png/cc0000/ffffff",
      "category_id": 7,
      "external_id": "DW2R9C6bJSgQ"
    }, {
      "user_id": 1,
      "first_name": "Sammy",
      "last_name": "Kobsch",
      "position": "Assistant Media Planner",
      "company_name": "Haley-Weimann",
      "address": "6 Grayhawk Circle",
      "work_tel": "9795328438",
      "mobile": "7577783591",
      "email": "skobsch11@businesswire.com",
      "namecard_img": "http://dummyimage.com/167x100.png/5fa2dd/ffffff",
      "category_id": 7,
      "external_id": "AK8In9pjH"
    }, {
      "user_id": 13,
      "first_name": "Celestyna",
      "last_name": "Edmonds",
      "position": "Paralegal",
      "company_name": "Simonis, Wiza and Abbott",
      "address": "20 Annamark Circle",
      "work_tel": "7376601423",
      "mobile": "6888634471",
      "email": "cedmonds12@barnesandnoble.com",
      "namecard_img": "http://dummyimage.com/196x100.png/5fa2dd/ffffff",
      "category_id": 2,
      "external_id": "gEIfKb"
    }, {
      "user_id": 1,
      "first_name": "Michelina",
      "last_name": "Minor",
      "position": "Structural Analysis Engineer",
      "company_name": "Ortiz-Kessler",
      "address": "862 Montana Plaza",
      "work_tel": "9101344252",
      "mobile": "6015850732",
      "email": "mminor13@google.com.br",
      "namecard_img": "http://dummyimage.com/100x100.png/dddddd/000000",
      "category_id": 2,
      "external_id": "tLazMdqf"
    }, {
      "user_id": 9,
      "first_name": "Amber",
      "last_name": "Mylchreest",
      "position": "Account Representative IV",
      "company_name": "Upton, McLaughlin and Rolfson",
      "address": "310 Nova Junction",
      "work_tel": "5555290831",
      "mobile": "7293175205",
      "email": "amylchreest14@slate.com",
      "namecard_img": "http://dummyimage.com/165x100.png/ff4444/ffffff",
      "category_id": 10,
      "external_id": "uUQ4VdjuR"
    }, {
      "user_id": 13,
      "first_name": "Marlowe",
      "last_name": "O'Hogertie",
      "position": "Sales Representative",
      "company_name": "Mertz, Orn and Kuhn",
      "address": "4892 Milwaukee Pass",
      "work_tel": "2308477281",
      "mobile": "5821658908",
      "email": "mohogertie15@yelp.com",
      "namecard_img": "http://dummyimage.com/171x100.png/5fa2dd/ffffff",
      "category_id": 10,
      "external_id": "CWzhP915mutg"
    }, {
      "user_id": 8,
      "first_name": "Francklyn",
      "last_name": "Dight",
      "position": "Senior Financial Analyst",
      "company_name": "Kulas, Schuppe and King",
      "address": "031 Ludington Avenue",
      "work_tel": "5106224860",
      "mobile": "9443908958",
      "email": "fdight16@wordpress.org",
      "namecard_img": "http://dummyimage.com/197x100.png/ff4444/ffffff",
      "category_id": 1,
      "external_id": "TMhYld69"
    }, {
      "user_id": 5,
      "first_name": "Eveline",
      "last_name": "Stedmond",
      "position": "Product Engineer",
      "company_name": "Lowe, Sawayn and Hartmann",
      "address": "24 Lakewood Point",
      "work_tel": "1543104847",
      "mobile": "2776799627",
      "email": "estedmond17@wired.com",
      "namecard_img": "http://dummyimage.com/248x100.png/cc0000/ffffff",
      "category_id": 6,
      "external_id": "KpFGTge8qWqI"
    }, {
      "user_id": 15,
      "first_name": "Sergeant",
      "last_name": "Frandsen",
      "position": "Internal Auditor",
      "company_name": "Champlin, Sporer and Labadie",
      "address": "621 Atwood Park",
      "work_tel": "3123911826",
      "mobile": "9583544578",
      "email": "sfrandsen18@bandcamp.com",
      "namecard_img": "http://dummyimage.com/191x100.png/cc0000/ffffff",
      "category_id": 8,
      "external_id": "N1pDsD4UXwX"
    }])
    
    await knex("message").insert([{
        "user_id": 6,
        "namecard_id": 27,
        "content": "vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus"
      }, {
        "user_id": 1,
        "namecard_id": 19,
        "content": "ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis"
      }, {
        "user_id": 13,
        "namecard_id": 37,
        "content": "quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas ut massa quis augue luctus"
      }, {
        "user_id": 2,
        "namecard_id": 13,
        "content": "ac diam cras pellentesque volutpat dui maecenas tristique est et"
      }, {
        "user_id": 9,
        "namecard_id": 15,
        "content": "aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam"
      }, {
        "user_id": 9,
        "namecard_id": 45,
        "content": "purus eu magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient"
      }, {
        "user_id": 1,
        "namecard_id": 1,
        "content": "amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin"
      }, {
        "user_id": 7,
        "namecard_id": 23,
        "content": "faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin"
      }, {
        "user_id": 14,
        "namecard_id": 29,
        "content": "justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla"
      }, {
        "user_id": 9,
        "namecard_id": 33,
        "content": "sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet"
      }, {
        "user_id": 7,
        "namecard_id": 14,
        "content": "turpis eget elit sodales scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed"
      }, {
        "user_id": 15,
        "namecard_id": 39,
        "content": "sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus"
      }, {
        "user_id": 7,
        "namecard_id": 35,
        "content": "ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet"
      }, {
        "user_id": 12,
        "namecard_id": 24,
        "content": "justo morbi ut odio cras mi pede malesuada in imperdiet et"
      }, {
        "user_id": 7,
        "namecard_id": 27,
        "content": "posuere cubilia curae nulla dapibus dolor vel est donec odio justo"
      }, {
        "user_id": 3,
        "namecard_id": 22,
        "content": "quisque porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla"
      }, {
        "user_id": 13,
        "namecard_id": 20,
        "content": "platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel"
      }, {
        "user_id": 1,
        "namecard_id": 40,
        "content": "augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit"
      }, {
        "user_id": 14,
        "namecard_id": 29,
        "content": "et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient"
      }, {
        "user_id": 15,
        "namecard_id": 37,
        "content": "facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at"
      }, {
        "user_id": 3,
        "namecard_id": 45,
        "content": "sagittis dui vel nisl duis ac nibh fusce lacus purus aliquet at feugiat non pretium"
      }, {
        "user_id": 2,
        "namecard_id": 43,
        "content": "orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu"
      }, {
        "user_id": 11,
        "namecard_id": 17,
        "content": "libero non mattis pulvinar nulla pede ullamcorper augue a suscipit"
      }, {
        "user_id": 9,
        "namecard_id": 44,
        "content": "aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam"
      }, {
        "user_id": 1,
        "namecard_id": 33,
        "content": "ut erat id mauris vulputate elementum nullam varius nulla facilisi"
      }, {
        "user_id": 3,
        "namecard_id": 22,
        "content": "in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet"
      }, {
        "user_id": 7,
        "namecard_id": 10,
        "content": "bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu"
      }, {
        "user_id": 3,
        "namecard_id": 39,
        "content": "imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere"
      }, {
        "user_id": 3,
        "namecard_id": 18,
        "content": "ac leo pellentesque ultrices mattis odio donec vitae nisi nam"
      }, {
        "user_id": 13,
        "namecard_id": 6,
        "content": "nulla elit ac nulla sed vel enim sit amet nunc viverra"
      }])
};
