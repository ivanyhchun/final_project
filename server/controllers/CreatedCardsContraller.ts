import express from 'express';
import { CreatedCardsService } from "../services/CreatedCardsServices";

export class CreatedCardsController {

    constructor(private createdCardsService: CreatedCardsService) {

    }

    loadCardObj = async (req: express.Request, res: express.Response) => {
        try {
            const queryId = req.query.id + "";
            const result = await this.createdCardsService.loadCardObj(queryId)
            if (result.length > 0) {
                res.status(200).json(result);
            } else {
                res.status(400).json("no such record")
            }
        } catch (error) {
            console.log(error);
            res.status(500).json({ success: false });
        }
    }
    putBgImage = async (req: express.Request, res: express.Response) => {
        try {
            const queryId = parseInt(String(req.query.id));
            if (req.file) {
                const bgImg = req.file.filename;
                await this.createdCardsService.putBgImage(queryId, bgImg);
                res.status(200).json(bgImg);
            }
        } catch (error) {
            res.status(401).json({ message: "Upload image failed" });
            console.log(error);
        }
    }

    putLogoImage = async (req: express.Request, res: express.Response) => {
        try {
            const queryId = parseInt(String(req.query.id));
            let image64 = req.body.imagedata64
            if (image64) {
                await this.createdCardsService.putLogoImage(queryId, image64);
            }
            res.status(200).json({ success: true });
        } catch (error) {
            res.status(401).json({ message: "Upload image failed" });
            console.log(error);
        }
    }

    delLogoImage = async (req: express.Request, res: express.Response) => {
        try {
            const queryId = parseInt(String(req.query.id));
            await this.createdCardsService.delLogoImage(queryId);
            res.status(200).json({ success: true });
        } catch (error) {
            res.status(401).json({ message: "Upload image failed" });
            console.log(error);
        }
    }
}