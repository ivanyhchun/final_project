import express from 'express';
import { CategoryService } from '../services/CategoryServices';
import { Category } from '../services/models';

export class CategoryController{
    constructor(private categoryService:CategoryService){

    }

    getCategory = async (req: express.Request, res: express.Response) =>{
        try{
            const memberId = req['user'].id 
            const categories:Category[] = await this.categoryService.getCategory(memberId)
            res.status(200).json(categories)
        } catch (error){
            console.log(error);
            res.status(401).json({ success: false }); 
        }
    }

    postCategory = async (req: express.Request, res: express.Response) =>{
        try{
            const categoryName = req.query.categoryName + ''
            const memberId = req['user'].id 
            const newCategoryId = await this.categoryService.postCategory(memberId,categoryName)
            if (newCategoryId){
                const newCategories:Category[] = await this.categoryService.getCategory(memberId)
                res.status(200).json(newCategories)
            } else {
                res.status(500).json({success:false,message:"Unable to add new category"})
            }
        } catch (error){
            console.log(error);
            res.status(401).json({ success: false }); 
        }
    }

    putCategory = async (req: express.Request, res: express.Response) =>{
        try{
            const newCategoryName = req.query.newCategoryName + ''
            const oldCategoryId = req.query.oldCategoryId + ""
            const memberId = req['user'].id 
            const renamedCategory = await this.categoryService.putCategory(memberId,oldCategoryId,newCategoryName)
            if (renamedCategory){
                const renamedCategories:Category[] = await this.categoryService.getCategory(memberId)
                res.status(200).json(renamedCategories)
            } else {
                res.status(500).json({success:false,message:"Unable to rename existing category"})
            }
        } catch (error){
            console.log(error);
            res.status(401).json({ success: false }); 
        }
    }

    deleteCategory = async (req: express.Request, res: express.Response) =>{
        try{
            const oldCategoryId = req.query.oldCategoryId + ""
            const memberId = req['user'].id 
            const deleteCategory = await this.categoryService.deleteCategory(memberId,oldCategoryId)
            if (deleteCategory){
                const updatedCategories:Category[] = await this.categoryService.getCategory(memberId)
                res.status(200).json(updatedCategories)
            } else {
                res.status(500).json({success:false,message:"Unable to delete existing category"})
            }
        } catch (error){
            console.log(error);
            res.status(401).json({ success: false }); 
        }
    }

}