import express from 'express';
import path from 'path';
import { CategoryService } from '../services/CategoryServices';
import { NamecardsService } from '../services/NamecardsServices';
import { recognizeCards } from '../services/recognizeNamecard';

export class NamecardsController {
    constructor(private namecardsService: NamecardsService, private categoryService: CategoryService) {
    }

    loadNamecardsList = async (req: express.Request, res: express.Response) => {
        try {
            const id = req['user'].id
            const result = await this.namecardsService.loadNamecardsList(id);          
            res.status(200).json(result);
        } catch (error) {
            console.log(error);
            res.status(401).json({ success: false });
        }
    }

    loadOneNamecard = async (req:express.Request, res: express.Response) =>{
        try{
            const id = req['user'].id
            const queryId = req.query.externalId
            if (queryId){
                const result = await this.namecardsService.loadOneNamecard(id,queryId);          
                res.status(200).json(result);  
            }
        }catch(error){
            console.log(error);
            res.status(401).json({ success: false });
        }
    }

    getNonMemberCardImage = async (req:express.Request, res: express.Response) =>{
        try{
            const id = req['user'].id
            if (id){
                let filename = req.params.path
                let filepath = path.join(__dirname, '../protected/non-mem-namecards/') + filename
                res.sendFile(filepath)
            }
        } catch (error) {
            res.status(401).json({message:"Get image failed"});
            console.log(error); 
        }
    }
    

    postNonMemberCard = async (req: express.Request, res: express.Response)=>{
        try{
            const userId = req['user'].id; 
            const {
                firstName,
                lastName, 
                position,
                companyName,
                address,
                workContact,
                mobile,
                email,
                categoryIDorName
            } = req.body; 
            const method = req.query.method
            let namecardImg;
            if (method == 'camera'){
                namecardImg = req.body.namecardImg
            } else if (method == 'manual'){
                namecardImg = ""
            }

            let categoryId = null;
            if (categoryIDorName){
                if (Number.isNaN(parseInt(categoryIDorName))){
                    categoryId = (await this.categoryService.postCategory(userId,categoryIDorName))[0]
                } else {
                    categoryId = categoryIDorName
                }
            }
            let newNonMemCardId = (await this.namecardsService.insertNonMemberCard(
                userId, 
                firstName,
                lastName, 
                position,
                companyName,
                address,
                workContact,
                mobile,
                email,
                namecardImg,
                categoryId
            ))[0]; 
            res.status(200).json({newNonMemCardId:newNonMemCardId})
        } catch (error){
            console.log(error);
            res.status(401).json({ success: false })
        }
    }

    editNonMemberCard = async (req: express.Request, res: express.Response) => {
        try {
            const userId = req['user'].id; 
            const cardId = String(req.query.externalId); 
            const {
                firstName,
                lastName, 
                position,
                companyName,
                address,
                workContact,
                mobile,
                email,
                namecardImg,
                categoryIDorName
            } = req.body;
            let categoryId = null;
            if (categoryIDorName){
                if (Number.isNaN(parseInt(categoryIDorName))){
                    categoryId = (await this.categoryService.postCategory(userId,categoryIDorName))[0]
                } else {
                    categoryId = categoryIDorName
                }
            } 
            await this.namecardsService.editNonMemberCard(
                cardId, 
                userId, 
                firstName,
                lastName, 
                position,
                companyName,
                address,
                workContact,
                mobile,
                email,
                namecardImg,
                categoryId
            ); 
            res.status(200).json({ success: true }); 
        } catch (error) {
            console.log(error); 
            res.status(401).json({ success: false }); 
        }
    }

    deleteNamecard = async (req: express.Request, res: express.Response) => {
        try {
            const cardId = parseInt(String(req.query.id)); 
            const isMember = (req.query.isMember === "true"); 
            const userId = req['user'].id; 
            await this.namecardsService.deleteNamecard(userId, isMember, cardId); 
            res.status(200).json({ success: true }); 
        } catch (error) {
            console.log(error); 
            res.status(401).json({ success: false }); 
        }
    }

    uploadNonMemberCardImage = async (req: express.Request, res: express.Response) => {
        try {
            if (req.file){
                const namecardImg = req.file.filename;
                const fullPath = path.join(__dirname,'../protected/non-mem-namecards/') + namecardImg
                const businessCard = await recognizeCards(fullPath);
                const namecard = {
                    ...businessCard,
                    namecardImg: namecardImg
                }
                res.status(200).json(namecard)
            } else {
                res.status(400).json("Cannot receive file. Please upload again")
            }  
        } catch (error) {
            console.log(error);
            res.status(401).json({ success: false })
        }
    }

    addFavorite = async (req: express.Request, res: express.Response) => {
        try {
            const cardId = req.query.externalId; 
            const isFavorite = req.body.favorite;  
            await this.namecardsService.addFavorite(cardId, isFavorite); 
            res.status(200).json({ success: true }); 
        } catch (error) {
            console.log(error); 
            res.status(401).json({ success: false }); 
        }
    }

    updateNamecardCategory = async (req: express.Request, res: express.Response) => {
        try{
            let categoryId;
            const categoryName = req.body.categoryName;
            const namecardId = parseInt(req.body.namecardId+"");
            const namecardOwner = req.body.namecardOwner
            const userId = req['user'].id; 
            if (req.body.categoryId == 'null'){
                categoryId = null
            } else {
                categoryId = parseInt(req.body.categoryId+'')
                if (Number.isNaN(categoryId)){
                    categoryId = (await this.categoryService.postCategory(userId,categoryName+""))[0]
                }
            }
            if (namecardOwner){
                const memberRelationshipId = (await this.namecardsService.updateNamecardCategoryMember(userId,categoryId,namecardId))[0]
                res.status(200).json(memberRelationshipId)
            } else {
                const nonMemCardId = (await this.namecardsService.updateNamecardCategoryNonMember(userId,categoryId,namecardId))[0]
                res.status(200).json(nonMemCardId)
            }
        } catch (error){
            console.log(error); 
            res.status(401).json({ success: false }); 
        }
    }
}



