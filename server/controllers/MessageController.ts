import express from 'express'; 
import { MessageService } from '../services/MessageServices'; 

export class MessageController {
    constructor(private messageService: MessageService) {
    }

    loadMessage = async (req: express.Request, res: express.Response) => {
        try {
            const userId = req['user'].id; 
            const result = await this.messageService.loadMessage(userId); 
            res.status(200).json(result); 
        } catch (error) {
            console.log(error); 
            res.status(401).json({ success: false }); 
        }
    }

    messageIsRead = async (req: express.Request, res: express.Response) => {
        try {
            const userId = req['user'].id; 
            const msgId = parseInt(String(req.query.msgId)); 
            await this.messageService.messageIsRead(userId, msgId); 
            res.status(200).json({ success: true }); 
        } catch (error) {
            console.log(error); 
            res.status(401).json({ success: false }); 
        }
    }

    deleteMessage = async (req: express.Request, res: express.Response) => {
        try {
            const msgIds = req.body.msgIds; 
            await this.messageService.deleteMessage(msgIds); 
            res.status(200).json({ success: true }); 
        } catch (error) {
            console.log(error); 
            res.status(401).json({ success: false }); 
        }
    }

}