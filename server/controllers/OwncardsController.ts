import express from 'express';
import path from 'path';
import { OwnCard } from '../services/models';
import { OwncardsService } from '../services/OwncardsServices';
import { MessageService } from '../services/MessageServices';
import fs from 'fs';

export class OwncardsController {
    constructor(private owncardsService: OwncardsService, private messageService: MessageService) {
    }

    loadOwncardsList = async (req: express.Request, res: express.Response) => {
        try {
            const userId = req['user'].id;
            const result: OwnCard[] = await this.owncardsService.loadOwncardsList(userId);
            res.status(200).json(result);
        } catch (error) {
            console.log(error);
            res.status(500).json({ success: false });
        }
    }

    loadOneOwncard = async (req: express.Request, res: express.Response) => {
        try {
            const queryId = parseInt(String(req.query.id));
            if (queryId) {
                const result = await this.owncardsService.loadOneOwncard(queryId);
                res.status(200).json(result);
            }
        } catch (error) {
            console.log(error);
            res.status(401).json({ success: false });
        }
    }

    createOwncard = async (req: express.Request, res: express.Response) => {
        try {
            const userId = req['user'].id;
            const {
                firstName,
                lastName,
                position,
                companyName,
                address,
                workContact,
                mobile,
                email
            } = req.body.data;
            const cardId = await this.owncardsService.createOwncard(
                userId,
                firstName,
                lastName,
                position,
                companyName,
                address,
                workContact,
                mobile,
                email
            );
            res.status(200).json({ cardId: cardId });
        } catch (error) {
            console.log(error);
            res.status(401).json({ success: false });
        }
    }

    cardElements = async (req: express.Request, res: express.Response) => {
        try {
            const userId = req['user'].id;
            if (req.query.nameCardId) {
                const namecard_id = req.query.nameCardId + "";
                let info = req.body
                const result = await this.owncardsService.cardElements(namecard_id, userId, info);
                res.status(200).json({ cardInfo: result });
            }
        } catch (error) {
            console.log(error);
            res.status(401).json({ success: false });
        }
    }

    editOwncard = async (req: express.Request, res: express.Response) => {
        try {
            const userId = req['user'].id;
            const cardId = req.body.id;
            const cardName = req.body.firstName;
            const {
                id,
                firstName,
                lastName,
                position,
                companyName,
                address,
                workContact,
                mobile,
                email,
                namecardImg
            } = req.body;
            await this.owncardsService.editOwncard(
                id,
                userId,
                firstName,
                lastName,
                position,
                companyName,
                address,
                workContact,
                mobile,
                email,
                namecardImg);
            await this.messageService.generateMessage(cardId, cardName);
            res.status(200).json({ success: true });
        } catch (error) {
            console.log(error);
            res.status(401).json({ success: false });
        }
    }

    deleteOwncard = async (req: express.Request, res: express.Response) => {
        try {
            const cardId = parseInt(String(req.query.id));
            await this.owncardsService.deleteOwncard(cardId);
            res.status(200).json({ success: true });
        } catch (error) {
            console.log(error);
            res.status(401).json({ success: false });
        }
    }

    drawingToPNG = async (req: express.Request, res: express.Response) => {
        try {
            const userId = req['user'].id;
            const namecard_id = req.query.nameCardId + "";
            if (namecard_id) {
                const base64URL = req.body.base64URL;
                let base64Image = base64URL.split(';base64,').pop();
                let nameCardImg = Date.now() + '.png';
                await fs.promises.writeFile(path.resolve('./protected/owncard-image/' + nameCardImg), base64Image, { encoding: 'base64' });
                const result = await this.owncardsService.drawingToPNG(userId, namecard_id, nameCardImg);
                if (result.length > 0) {
                    res.status(200).json({ success: true });
                } else {
                    res.status(503).json({ success: false });
                    console.log('upload failed');
                    alert('Upload failed. Please try again')
                }
            }
        } catch (error) {
            res.status(401).json({ success: false });
            console.log(error);
        }
    }

    getNameCardImg = async (req: express.Request, res: express.Response) => {
        try {
            const id = req['user'].id
            if (id) {
                let filename = req.params.path
                let filepath = path.join(__dirname, '../protected/owncard-image/') + filename
                res.sendFile(filepath)
            }
        } catch (error) {
            res.status(401).json({ message: "Get image failed" });
            console.log(error);
        }
    }

    QRCodeAddCard = async (req: express.Request, res: express.Response) => {
        try {
            const userId = req['user'].id
            const externalId = req.params.externalId
            const result = await this.owncardsService.checkQRCodeExternalID(externalId)

            if (result.length > 0) {
                let cardId = parseInt(result[0].id)
                let newRelationshipId = await this.owncardsService.addMemberRelationship(userId, cardId)
                if (newRelationshipId == false) {
                    res.status(202).json("This card has been added previously")
                } else {
                    res.status(200).json(newRelationshipId)
                }
            } else {
                res.status(404).json("Requested card not found")
            }
        } catch (error) {
            res.status(500).json({ message: "Server error" });
            console.log(error);
        }
    }

    CardBGImage = async (req: express.Request, res: express.Response) => {
        try {
            let filename = req.params.path
            let filepath = path.join(__dirname, '../protected/owncard-image/') + filename
            res.sendFile(filepath)
        } catch (error) {
            res.status(500).json({ message: "Server error" });
            console.log(error);
        }
    }
}