import express from 'express';
import { checkPassword, hashPassword } from '../hash';
import { UserService } from "../services/UserServices"
import jwtSimple from 'jwt-simple';
import path from 'path';



export class UserController {

    constructor(private userService: UserService) {

    }

    login = async (req: express.Request, res: express.Response) => {
        const inputEmail = (req.body.email).toLowerCase(); 
        console.log('check input email', inputEmail); 
        const users = await this.userService.login(inputEmail);
        const userFound = users;
        if (!userFound) {
            res.status(401).json({ success: false, message: "Incorrect Username/Password" });
            return;
        }
        const match = await checkPassword(req.body.password, userFound.password);
        if (match) {
            const token = jwtSimple.encode({
                id: parseInt(userFound.id),
                email:userFound.email
            },process.env.JWT_SECRET!) 
            res.status(200).json({token: token,userId:userFound.id});
        } else {
            res.status(401).json({ 'success': 'false' }); 
        }
    }

    getProfileImage = async (req: express.Request, res: express.Response)=>{
        try{
            const id = req['user'].id
            if (id){
                let filename = req.params.path
                let filepath = path.join(__dirname, '../protected/profileImg/') + filename
                res.sendFile(filepath)
            }
        } catch (error){
            res.status(401).json({message:"Get image failed"});
            console.log(error); 
        }
    }

    putProfileImage = async (req: express.Request, res: express.Response) =>{
        try{
            const id = req['user'].id
            if (req.file){
                const profileImg = req.file.filename;
                await this.userService.putProfileImage(id,profileImg);
                res.status(200).json(profileImg); 
            }
        } catch (error){
            res.status(401).json({message:"Upload image failed"});
            console.log(error); 
        }
    }

    editProfileName = async (req: express.Request, res: express.Response) => {
        try {
            const userId = req['user'].id;
            const firstName = req.body.firstName;
            const lastName = req.body.lastName;
            await this.userService.editProfileName(userId, firstName, lastName);
            res.status(200).json({ success: true });
        } catch (error) {
            console.log(error);
            res.status(500).json({ success: false });
        }
    }

    signup = async (req: express.Request, res: express.Response) => {
        try {
            const inputFirstName = req.body.firstName;
            const inputLastName = req.body.lastName;
            const inputEmail = (req.body.email).toLowerCase();
            const inputPassword = req.body.password;
            const hashedPassword = await hashPassword(inputPassword);
            const newUserID = await this.userService.signUp(inputFirstName,inputLastName, inputEmail, hashedPassword);
            if (newUserID) {
                const token = jwtSimple.encode({
                    id: newUserID[0],
                    email:inputEmail
                },process.env.JWT_SECRET!)
                res.status(200).json({token: token,userId:newUserID}); 
            }
        } catch (error) {
            if (error.constraint == 'users_email_unique') {
                console.log(error)
                res.status(406).json({ success: false, message: "failed to create user. Try with another email" }); 
            } else {
                res.status(501).json({ success: false, message: "signup error" }); 
            }
        }
    }

    loadUserProfile = async (req: express.Request, res: express.Response) => {
        try {
            const userId = req['user'].id;
            const result = await this.userService.loadUserProfile(userId);
            res.status(200).json(result);
        } catch (error) {
            console.log(error);
            res.status(500).json({ success: false });
        }
    }

    chgPassword = async (req: express.Request, res: express.Response) => {
        try {
            const userId = req['user'].id;
            const user = await this.userService.getPassword(userId);
            if (!user) {
                res.status(401).json({ success: false, message: "Incorrect Username/Password" });
                return;
            }
            const userPassword = user.password;
            const oldPassword = req.body.oldPassword;
            const match = await checkPassword(oldPassword, userPassword);
            if (match) {
                const newPassword = await hashPassword(req.body.newPassword);
                await this.userService.changePassword(userId, newPassword);
                res.status(201).json({ success: true, message: "successful" });
            } else {
                res.status(401).json({ success: false, message: "Password does not match record. Please try again." });
            }

        } catch (error) {
            console.log(error);
            res.status(500).json({ success: false,message:"Server error. Please try again later." });
        }
    }
}


